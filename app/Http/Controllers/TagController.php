<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Traits\ArticleTrait;
use App\Traits\FrontendConfig;
use Artesaos\SEOTools\Traits\SEOTools;
use App\Models\Tag;

class TagController extends Controller
{
    use FrontendConfig;
    use SEOTools;
    use ArticleTrait;

    public function detail($slug)
    {
        $tag = Tag::with('seo')->where('slug', $slug)->firstOrFail();

        $this->seo()->setTitle($tag->seo->title);
        $this->seo()->setDescription($tag->seo->description);
        $this->seo()->setCanonical($tag->seo->canonical);

        $this->data['popularPosts'] = $this->popularPosts();
        $this->data['tag'] = $tag;

        $this->data['articles'] = Article::with('permalink')
            ->whereHas('tags', function ($q) use ($tag) {
                $q->whereIn('id', [$tag->id]);
            })
            ->orderBy('created_at', 'desc')
            ->paginate(20);

        $this->data['tags'] = Tag::where('type', 'article')
            ->whereHas('articles')
            ->take(10)->get();

        $this->data['menu'] = $this->getMenu();
        return view('article-tag', $this->data);
    }
}
