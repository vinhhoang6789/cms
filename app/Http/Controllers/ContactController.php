<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Block;
use App\Models\Contact;
use App\Traits\ArticleTrait;
use App\Traits\FrontendConfig;
use Artesaos\SEOTools\Traits\SEOTools;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class ContactController extends Controller
{
    use FrontendConfig;
    use ArticleTrait;
    use SEOTools;

    public function index()
    {
        $this->seo()->setTitle("Liên hệ");
        $this->data['menu'] = $this->getMenu();

        return view('contact',$this->data);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'content' => 'required',
            'email' => 'required|email',
        ];

        if($request->input('type') == 'smart'){
            $rules['phone'] = 'required';
        }

        if($phone = $request->input('phone')){
            $rules['phone'] = 'phone:VN';
        }

        $request->validate($rules);

        try{
            DB::beginTransaction();

            $contact = new Contact();
            $contact->name = $request->input('name');
            if($phone){
                $contact->phone = $request->input('phone');
            }
            $contact->email = $request->input('email');
            $contact->content = $request->input('content');
            $contact->save();

            if($files = $request->input('file')){
                $files = array_map(function($file){
                    return [
                        'url' => $file
                    ];
                }, $files);

                $contact->attachments()->createMany($files);
            }

            DB::commit();

        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }

    }

}
