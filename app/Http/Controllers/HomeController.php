<?php

namespace App\Http\Controllers;

use App\Models\Block;
use App\Models\Slider;
use App\Traits\ArticleTrait;
use App\Traits\FrontendConfig;
use Artesaos\SEOTools\Traits\SEOTools;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    use FrontendConfig;
    use ArticleTrait;
    use SEOTools;

    public function index()
    {
        $blocks = Block::where('group', 'home')->get()->keyBy('slug');

        $menu = $this->getMenu();

        $sliders = Slider::where('group', 'home')->orderBy('position')->get();

        $pageClass = "home";

        return view('home', compact("menu", "blocks", "sliders", "pageClass"));
    }

    public function subscribe(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
        ]);
    }
}
