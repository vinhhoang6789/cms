<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Traits\FrontendConfig;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    use FrontendConfig;

    public function detail($slug)
    {
        $menu = $this->getMenu();
        return view('home', compact("menu"));
    }
}
