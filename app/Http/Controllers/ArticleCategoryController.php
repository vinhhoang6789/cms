<?php

namespace App\Http\Controllers;
use App\Models\Article;
use App\Traits\FrontendConfig;
use App\Traits\ArticleTrait;
use Artesaos\SEOTools\Traits\SEOTools;

class ArticleCategoryController extends Controller
{
    use FrontendConfig;
    use ArticleTrait;
    use SEOTools;

    public function detail($category)
    {
        $category = $category->load(['permalink', 'seo'])->first();

        $this->seo()->setTitle($category->seo->title);
        $this->seo()->setDescription($category->seo->description);
        $this->seo()->setCanonical($category->seo->canonical);

        $this->data['menu'] = $this->getMenu();
        $this->data['popularPosts'] = $this->popularPosts();

        $this->data['category'] = $category;

        $this->data['articles'] = Article::with('permalink')
            ->whereIn('article_category_id', [$category->id])
            ->orderBy('created_at', 'desc')
            ->paginate(20);

        $this->data['breadcrumbCategories'] = $this->articleCategory($category->id);

        return view('article-category',$this->data);
    }
}
