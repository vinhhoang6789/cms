<?php

namespace App\Http\Controllers;
use App\Models\Article;
use App\Traits\ArticleTrait;
use App\Traits\FrontendConfig;
use Artesaos\SEOTools\Traits\SEOTools;

class ArticleController extends Controller
{
    use FrontendConfig;
    use ArticleTrait;
    use SEOTools;

    public function index()
    {
        $this->seo()->setTitle('Tin công nghệ, tài liệu lập trình, chia sẻ kiến thức.');
        $this->seo()->setDescription('Tin công nghệ, tài liệu lập trình, chia sẻ kiến thức.');
        $this->data['menu'] = $this->getMenu();

        $this->data['articles'] = Article::with('permalink')->orderBy('created_at', 'desc')->paginate(20);

        return view('news', $this->data);
    }

    public function detail($article)
    {
        $article = $article->with(['seo','category', 'tags'])->first();

        $this->seo()->setTitle($article->seo->title);
        $this->seo()->setDescription($article->seo->description);
        $this->seo()->setCanonical($article->seo->canonical);

        $this->data['article'] = $article;
        $this->data['menu'] = $this->getMenu();

        $this->data['share'] = \Share::currentPage()
            ->facebook()->twitter();

        $this->data['popularPosts'] =  $this->popularPosts([$article->id]);
        $this->data['breadcrumbCategories'] = $this->articleCategory($article->article_category_id);
        $this->data['relatedPosts'] = $this->relatedPosts([$article->id]);

        return view('detail-article', $this->data);
    }

}
