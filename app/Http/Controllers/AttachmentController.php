<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AttachmentController extends Controller
{
    public function upload(Request $request)
    {
        $request->validate([
            'file' => 'max:3000|mimes:jpeg,bmp,png,svg,gif,jpg,docx,pdf'
        ]);

        try{
            $path = $request->file('file')->store('attachments', 'public');

        }catch (\Exception $e){
            return response()->json(['message' => $e->getMessage()], 422);
        }

        return response()->json(['path' => $path]);
    }
}
