<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\ContactDataTable;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use Illuminate\Http\Response;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(ContactDataTable $datatable)
    {
        $this->authorize("edit-setting");

        $title = "Danh sách liên hệ";
        return $datatable->render('backend.datatable',compact("title"));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Contact $contact
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show($id)
    {
        $this->authorize("edit-setting");
        $contact = Contact::with('attachments')->findOrFail($id);
        return view('backend.contact.detail',compact("contact"));
    }
}
