<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Comment;
use App\Models\Product;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $articles = Article::with('permalink')->orderBy('created_at','desc')->take(10)->get();
        $products = Product::with('permalink')->orderBy('created_at','desc')->take(10)->get();

        $comments = Comment::with('commentable')->orderBy('created_at','desc')->take(10)->get();

        return view('backend.dashboard',compact("articles","products",'comments'));
    }
}
