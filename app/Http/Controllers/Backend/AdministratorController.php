<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\AdministratorDataTable;
use App\Http\Controllers\Controller;
use App\Models\Administrator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class AdministratorController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Administrator::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(AdministratorDataTable $dataTable)
    {
        $title = "Danh sách quản trị viên";
        return $dataTable->render('backend.datatable',compact("title"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $roles = Role::orderBy('name')->get();
        return view('backend.administrator.add',compact("roles"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'username' => 'required|unique:administrators',
            'email' => 'required|unique:administrators|email',
            'password' => 'required|min:6',
            'role_id' => 'required',
        ]);

        try{
            DB::beginTransaction();

            $administrator = new Administrator();
            $administrator->name = $request->input('name');
            $administrator->username = $request->input('username');
            $administrator->email = $request->input('email');
            $administrator->password = bcrypt($request->input('password'));
            $administrator->role_id = $request->input('role_id');
            $administrator->avatar = $request->input('avatar');
            $administrator->save();

            $role = Role::findOrFail($request->input('role_id'));
            $administrator->assignRole($role);

            DB::commit();
            return redirect()->route('administrators.index')->with(['success' => 'Thêm quản trị viên thành công.']);
        }catch (\Exception $e){
            DB::rollback();
            return back()->withInput($request->all())->withErrors([$e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Administrator  $administrator
     * @return \Illuminate\Http\Response
     */
    public function show(Administrator $administrator)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Administrator  $administrator
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Administrator $administrator)
    {
        $roles = Role::orderBy('name')->get();
        return view('backend.administrator.edit',compact("roles", "administrator"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Administrator  $administrator
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Administrator $administrator)
    {
        $request->validate([
            'name' => 'required',
            'username' => 'required|unique:administrators,username,'.$administrator->id,
            'email' => 'required|email|unique:administrators,email,'.$administrator->id,
            'role_id' => 'required',
        ]);

        try{
            DB::beginTransaction();
            $administrator->name = $request->input('name');
            $administrator->username = $request->input('username');
            $administrator->email = $request->input('email');

            if($request->input('password')){
                $administrator->password = bcrypt($request->input('password'));
            }

            $administrator->role_id = $request->input('role_id');
            $administrator->avatar = $request->input('avatar');
            $administrator->save();

            $role = Role::findOrFail($request->input('role_id'));
            $administrator->assignRole($role);

            DB::commit();
            return back()->with(['success' => 'Chỉnh sửa quản trị viên thành công.']);
        }catch (\Exception $e){
            DB::rollback();
            return back()->withInput($request->all())->withErrors([$e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Administrator  $administrator
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Administrator $administrator)
    {
        if($administrator->id == Auth::guard('administrator')->user()->id){
            return response()->json(['message' => "Không thể xóa bản ghi này."], 422);
        }

        try {
            DB::beginTransaction();
            $administrator->delete();

            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }
}
