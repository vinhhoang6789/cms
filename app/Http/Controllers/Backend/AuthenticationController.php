<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class AuthenticationController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = 'admin/dashboard';

    public function username()
    {
        return 'username';
    }

    protected function guard()
    {
        return Auth::guard('administrator');
    }

    public function showLoginForm(){
        return view('backend.login');
    }

    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
    }

    protected function loggedOut(Request $request)
    {
        $this->guard()->logout();
        return redirect()->route('backend.login');
    }
}
