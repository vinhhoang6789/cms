<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\BlockDataTable;
use App\Http\Controllers\Controller;
use App\Models\Block;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BlockController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(BlockDataTable $dataTable)
    {
        $this->authorize('edit-settings');

        $title = "Danh sách blocks";
        return $dataTable->render('backend.datatable',compact("title"));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('edit-settings');

        return view('backend.block.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Request $request)
    {
        $this->authorize('edit-settings');

        $request->validate([
            'title' => 'required|max:255',
        ]);

        try{
            DB::beginTransaction();

            $block = new Block();
            $block->title = $request->input('title');
            $block->url = $request->input('url');
            $block->group = $request->input('group');
            $block->content = $request->input('content');

            $slug = $request->input('slug');

            if($slug){
                $slug = SlugService::createSlug(Block::class, 'slug', $slug);
                $block->slug = $slug;
            }

            $block->save();

            DB::commit();

            return redirect()->route('blocks.index')->with(['success' => 'Thêm block thành công.']);

        }catch (\Exception $e){
            DB::rollBack();
            return back()->withErrors([$e->getMessage()])->withInput($request->all());
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Block $block
     * @return \Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Block $block)
    {
        $this->authorize('edit-settings');

        return view('backend.block.edit', compact("block"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Block $block
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, Block $block)
    {
        $this->authorize('edit-settings');

        $request->validate([
            'title' => 'required|max:255',
        ]);

        try{
            DB::beginTransaction();

            $block->title = $request->input('title');
            $block->url = $request->input('url');
            $block->group = $request->input('group');
            $block->content = $request->input('content');

            $slug = $request->input('slug');

            if($slug && $slug != $block->slug){
                $slug = SlugService::createSlug(Block::class, 'slug', $slug);
                $block->slug = $slug;
            }

            $block->save();

            DB::commit();

            return back()->with(['success' => 'Cập nhật block thành công.']);

        }catch (\Exception $e){
            DB::rollBack();
            return back()->withErrors([$e->getMessage()])->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Block $block
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Block $block)
    {
        $this->authorize('edit-settings');

        try {
            DB::beginTransaction();
            $block->delete();
            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }

    public function duplicate($id)
    {
        $this->authorize('edit-settings');

        try{
            DB::beginTransaction();
            $block = Block::find($id);
            $new = $block->replicate();
            $new->push();

            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }

    public function multipleDestroy(Request $request)
    {
        $this->authorize('edit-settings');

        $ids = $request->input('ids');

        try{
            DB::beginTransaction();

            if($ids){
                Block::whereIn('id',$ids)->delete();
            }
            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }

}
