<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\ArticleCategoryDataTable;
use App\Http\Controllers\Controller;
use App\Models\ArticleCategory;
use App\Models\Permalink;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArticleCategoryController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(ArticleCategory::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ArticleCategoryDataTable $articleCategoryDataTable)
    {
        $title = "Danh mục bài viết";
        return $articleCategoryDataTable->render('backend.datatable',compact("title"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $categories = ArticleCategory::orderBy('title')->get()->groupBy('parent');
        return view('backend.article-category.add', compact("categories"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|max:255',
        ]);

        try{
            DB::beginTransaction();

            $category = new ArticleCategory();
            $category->title = $request->input('title');

            if($parent = $request->input('parent')){
                $category->parent = $parent;
            }

            $category->excerpt = $request->input('excerpt');
            $category->active = $request->input('active');
            $category->featured_image = $request->input('featured_image');

            $category->save();

            $linkData = [
                'type' => ArticleCategory::PERMALINK_TYPE,
                'action' => ArticleCategory::PERMALINK_ACTION,
            ];

            $slug = $request->input('slug');

            if($slug){
                $slug = SlugService::createSlug(Permalink::class, 'slug', $slug);
                $linkData['slug'] = $slug;
            }
            $category->permalink()->create($linkData);

            $seo = $request->input('seo');

            if(!$seo['title']){
                $seo['title'] = $category->title;
            }

            if(!$seo['description']){
                $seo['description'] = $category->title;
            }

            $category->seo()->create($seo);

            DB::commit();

            return redirect()->route('article-categories.index')->with(['success' => 'Thêm danh mục thành công.']);

        }catch (\Exception $e){
            DB::rollBack();
            return back()->withErrors([$e->getMessage()])->withInput($request->all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ArticleCategory  $articleCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ArticleCategory $articleCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ArticleCategory  $articleCategory
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(ArticleCategory $articleCategory)
    {
        $articleCategory->load('seo', 'permalink');
        $categories = ArticleCategory::orderBy('title')->get()->groupBy('parent');
        return view('backend.article-category.edit', compact("categories","articleCategory"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ArticleCategory  $articleCategory
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, ArticleCategory $articleCategory)
    {
        $request->validate([
            'title' => 'required|max:255',
        ]);

        try{
            DB::beginTransaction();

            $articleCategory->title = $request->input('title');
            if($parent = $request->input('parent')){
                $articleCategory->parent = $parent;
            }

            $articleCategory->excerpt = $request->input('excerpt');
            $articleCategory->featured_image = $request->input('featured_image');
            $articleCategory->active = $request->input('active');

            $articleCategory->save();

            $slug = $request->input('slug');

            if($slug && $slug != $articleCategory->permalink->slug){
                $slug = SlugService::createSlug(Permalink::class, 'slug', $slug);
                $articleCategory->permalink()->update(['slug' => $slug]);
            }

            $seo = $request->input('seo');

            if(!$seo['title']){
                $seo['title'] = $articleCategory->title;
            }

            if(!$seo['description']){
                $seo['description'] = $articleCategory->title;
            }

            if(empty($seo['index'])){
                $seo['index'] = 0;
            }

            if(empty($seo['follow'])){
                $seo['follow'] = 0;
            }

            $articleCategory->seo()->update($seo);

            DB::commit();

            return back()->with(['success' => 'Chỉnh sửa danh mục thành công.']);

        }catch (\Exception $e){
            DB::rollBack();
            return back()->withErrors([$e->getMessage()])->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ArticleCategory  $articleCategory
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(ArticleCategory $articleCategory)
    {
        try {
            DB::beginTransaction();

            $articleCategory->delete();
            $articleCategory->seo()->delete();
            $articleCategory->permalink()->delete();

            DB::commit();

            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }

    public function multipleDestroy(Request $request)
    {
        $ids = $request->input('ids');

        try{
            $this->authorize('delete', ArticleCategory::find($ids[0]));

            DB::beginTransaction();

            foreach ($ids as $id){
                $category = ArticleCategory::find($id);
                $category->delete();
                $category->permalink()->delete();
                $category->seo()->delete();
            }

            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }
}
