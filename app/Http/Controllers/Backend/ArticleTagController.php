<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\ArticleTagDataTable;
use App\Http\Controllers\Controller;
use App\Models\ArticleCategory;
use App\Models\Permalink;
use App\Models\Tag;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArticleTagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(ArticleTagDataTable $dataTable)
    {
        $this->authorize('viewAny', Tag::class);

        $title = "Tags bài viết";
        return $dataTable->render('backend.datatable',compact("title"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Tag::class);
        return view('backend.article-tag.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Request $request)
    {
        $this->authorize('create', Tag::class);

        $request->validate([
            'title' => 'required|max:255',
        ]);

        try{
            DB::beginTransaction();

            $category = new Tag();
            $category->title = $request->input('title');
            $category->type = 'article';

            $category->save();

            $slug = $request->input('slug');

            if($slug){
                $category->slug = SlugService::createSlug(Tag::class, 'slug', $slug);
            }

            $seo = $request->input('seo');

            if(!$seo['title']){
                $seo['title'] = $category->title;
            }

            if(!$seo['description']){
                $seo['description'] = $category->title;
            }

            $category->seo()->create($seo);

            DB::commit();

            return redirect()->route('article-tags.index')->with(['success' => 'Thêm tag thành công.']);

        }catch (\Exception $e){
            DB::rollBack();
            return back()->withErrors([$e->getMessage()])->withInput($request->all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Tag $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit($id)
    {
        $tag = Tag::findOrFail($id);
        $this->authorize('update', $tag);

        return view('backend.article-tag.edit', compact("tag"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Tag $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, $id)
    {
        $tag = Tag::findOrFail($id);

        $this->authorize('update', $tag);

        $request->validate([
            'title' => 'required|max:255',
        ]);

        try{
            DB::beginTransaction();

            $tag->title = $request->input('title');
            $tag->save();

            $slug = $request->input('slug');

            if($slug && $slug != $tag->slug){
                $tag->slug = SlugService::createSlug(Permalink::class, 'slug', $slug);
            }

            $seo = $request->input('seo');

            if(!$seo['title']){
                $seo['title'] = $tag->title;
            }

            if(!$seo['description']){
                $seo['description'] = $tag->title;
            }

            if(empty($seo['index'])){
                $seo['index'] = 0;
            }

            if(empty($seo['follow'])){
                $seo['follow'] = 0;
            }

            $tag->seo()->update($seo);

            DB::commit();

            return back()->with(['success' => 'Chỉnh sửa tag thành công.']);

        }catch (\Exception $e){
            DB::rollBack();
            return back()->withErrors([$e->getMessage()])->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Tag $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy($id)
    {
        $tag = Tag::find($id);
        $this->authorize('delete', $tag);

        try {
            DB::beginTransaction();
            $tag->delete();
            $tag->seo()->delete();
            $tag->permalink()->delete();

            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }

    public function multipleDestroy(Request $request)
    {
        $ids = $request->input('ids');
        $this->authorize('delete', Tag::find($ids[0]));

        try{
            DB::beginTransaction();

            foreach ($ids as $id){
                $category = Tag::find($id);
                $category->delete();
                $category->seo()->delete();
                $category->permalink()->delete();
            }

            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }
}
