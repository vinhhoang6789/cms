<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\ProductCategoryDataTable;
use App\Http\Controllers\Controller;
use App\Models\Permalink;
use App\Models\ProductCategory;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductCategoryController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(ProductCategory::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ProductCategoryDataTable $dataTable)
    {
        $title = "Danh mục sản phẩm";
        return $dataTable->render('backend.datatable',compact("title"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $categories = ProductCategory::orderBy('title')->get()->groupBy('parent');
        return view('backend.product-category.add',compact("categories"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|max:255',
        ]);

        try{
            DB::beginTransaction();

            $category = new ProductCategory();
            $category->title = $request->input('title');

            if($parent = $request->input('parent')){
                $category->parent = $parent;
            }

            $category->excerpt = $request->input('excerpt');
            $category->active = $request->input('active');
            $category->featured_image = $request->input('featured_image');

            $category->save();

            $linkData = [
                'type' => ProductCategory::PERMALINK_TYPE,
                'action' => ProductCategory::PERMALINK_ACTION,
            ];

            $slug = $request->input('slug');

            if($slug){
                $slug = SlugService::createSlug(Permalink::class, 'slug', $slug);
                $linkData['slug'] = $slug;
            }
            $category->permalink()->create($linkData);

            $seo = $request->input('seo');
            if(!$seo['title']){
                $seo['title'] = $category->title;
            }

            if(!$seo['description']){
                $seo['description'] = $category->title;
            }

            $category->seo()->create($seo);

            DB::commit();

            return redirect()->route('product-categories.index')->with(['success' => 'Thêm danh mục thành công.']);

        }catch (\Exception $e){
            DB::rollBack();
            return back()->withErrors([$e->getMessage()])->withInput($request->all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ProductCategory $productCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductCategory  $productCategory
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(ProductCategory $productCategory)
    {
        $categories = ProductCategory::orderBy('title')->get()->groupBy('parent');
        return view('backend.product-category.edit',compact("categories", "productCategory"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductCategory  $productCategory
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, ProductCategory $productCategory)
    {
        $request->validate([
            'title' => 'required|max:255',
        ]);

        try{
            DB::beginTransaction();

            $productCategory->title = $request->input('title');

            if($parent = $request->input('parent')){
                $productCategory->parent = $parent;
            }

            $productCategory->excerpt = $request->input('excerpt');
            $productCategory->featured_image = $request->input('featured_image');
            $productCategory->active = $request->input('active');

            $productCategory->save();

            $slug = $request->input('slug');

            if($slug && $slug != $productCategory->permalink->slug){
                $slug = SlugService::createSlug(Permalink::class, 'slug', $slug);
                $productCategory->permalink()->update(['slug' => $slug]);
            }

            $seo = $request->input('seo');

            if(!$seo['title']){
                $seo['title'] = $productCategory->title;
            }

            if(!$seo['description']){
                $seo['description'] = $productCategory->title;
            }

            if(empty($seo['index'])){
                $seo['index'] = 0;
            }

            if(empty($seo['follow'])){
                $seo['follow'] = 0;
            }

            $productCategory->seo()->update($seo);

            DB::commit();

            return back()->with(['success' => 'Chỉnh sửa danh mục thành công.']);

        }catch (\Exception $e){
            DB::rollBack();
            return back()->withErrors([$e->getMessage()])->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductCategory  $productCategory
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(ProductCategory $productCategory)
    {
        try {
            DB::beginTransaction();
            $productCategory->delete();
            $productCategory->seo()->delete();
            $productCategory->permalink()->delete();

            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }

    public function multipleDestroy(Request $request)
    {
        $ids = $request->input('ids');
        $this->authorize('delete', ProductCategory::find($ids[0]));

        try{
            DB::beginTransaction();


            if($ids){
                foreach ($ids as $id){
                    $category = ProductCategory::find($id);
                    $category->delete();
                    $category->seo()->delete();
                    $category->permalink()->delete();
                }
            }

            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }
}
