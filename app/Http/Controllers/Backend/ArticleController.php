<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\ArticleDataTable;
use App\DataTables\TrashArticleDataTable;
use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\ArticleCategory;
use App\Models\Permalink;
use App\Models\Tag;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Article::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ArticleDataTable $dataTable)
    {
        $title = "Danh sách bài viết";
        return $dataTable->render('backend.datatable',compact("title"));
    }

    public function trash(TrashArticleDataTable $dataTable)
    {
        $this->authorize('viewAny', Article::class);

        $title = "Bài viết đã xóa";
        return $dataTable->render('backend.datatable',compact("title"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $categories = ArticleCategory::orderBy('title')->get()->groupBy('parent');
        $tags = Tag::where('type','article')->orderBy('title')->get();

        return view('backend.article.add', compact("categories", "tags"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|max:255',
            'content' => 'required'
        ]);

        try{
            DB::beginTransaction();

            $userId = Auth::guard('administrator')->user()->id;

            $article = new Article();
            $article->title = $request->input('title');
            $article->administrator_id = $userId;
            $article->is_featured = $request->input('is_featured');
            $article->article_category_id = $request->input('article_category_id');
            $article->content = $request->input('content');
            $article->excerpt = $request->input('excerpt');
            $article->featured_image = $request->input('featured_image');
            $article->status = $request->input('status');
            $article->save();

            $linkData = [
                'type' => Article::PERMALINK_TYPE,
                'action' => Article::PERMALINK_ACTION
            ];

            $slug = $request->input('slug');

            if($slug){
                $slug = SlugService::createSlug(Permalink::class, 'slug', $slug);
                $linkData['slug'] = $slug;
            }

            $article->permalink()->create($linkData);

            if($tags = $request->input('tag')){
                $article->tags()->attach($tags);
            }

            $seo = $request->input('seo');

            if(!$seo['title']){
                $seo['title'] = $article->title;
            }

            if(!$seo['description']){
                $seo['description'] = $article->title;
            }

            $article->seo()->create($seo);

            DB::commit();

            return redirect()->route('articles.index')->with(['success' => 'Thêm bài viết thành công.']);

        }catch (\Exception $e){
            DB::rollBack();
            return back()->withErrors([$e->getMessage()])->withInput($request->all());
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Article $article)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Article $article
     * @return \Illuminate\View\View
     */
    public function edit(Article $article)
    {
        $article->load(['tags','permalink']);
        $categories = ArticleCategory::orderBy('title')->get()->groupBy('parent');
        $tags = Tag::where('type','article')->orderBy('title')->get();

        return view('backend.article.edit', compact("article", "tags", "categories"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Article $article
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Article $article)
    {
        $request->validate([
            'title' => 'required|max:255',
            'content' => 'required'
        ]);

        try{
            DB::beginTransaction();

            $article->title = $request->input('title');
            $article->is_featured = $request->input('is_featured');
            $article->article_category_id = $request->input('article_category_id');
            $article->content = $request->input('content');
            $article->excerpt = $request->input('excerpt');
            $article->featured_image = $request->input('featured_image');
            $article->status = $request->input('status');
            $article->save();

            $slug = $request->input('slug');

            if($slug && $slug != $article->permalink->slug){
                $slug = SlugService::createSlug(Permalink::class, 'slug', $slug);
                $article->permalink()->update(['slug' => $slug]);
            }

            if($tags = $request->input('tag')){
                $article->tags()->sync($tags);
            }

            $seo = $request->input('seo');

            if(!$seo['title']){
                $seo['title'] = $article->title;
            }

            if(!$seo['description']){
                $seo['description'] = $article->title;
            }

            if(empty($seo['index'])){
                $seo['index'] = 0;
            }

            if(empty($seo['follow'])){
                $seo['follow'] = 0;
            }

            $article->seo()->update($seo);

            DB::commit();

            return back()->with(['success' => 'Cập nhật bài viết thành công.']);

        }catch (\Exception $e){
            DB::rollBack();
            return back()->withErrors([$e->getMessage()])->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Article $article
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Article $article)
    {
        try {
            DB::beginTransaction();
            $article->delete();
            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }

    public function duplicate($id)
    {
        $this->authorize('create', Article::class);

        try{
            DB::beginTransaction();
            $article = Article::find($id);

            $new = $article->replicate();

            $new->status = 'draft';
            $new->push();

            $article->relations = [];
            $article->load(['seo', 'tags', 'permalink']);

            foreach ($article->getRelations() as $name => $values){
                if($name == 'tags'){
                    $new->{$name}()->sync($values);

                }else{
                    $data = $values->toArray();

                    if($name == 'permalink'){
                        $slug = SlugService::createSlug(Permalink::class, 'slug', $data['slug']);
                        $data['slug'] = $slug;
                    }

                    $new->{$name}()->create($data);
                }
            }

            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }

    public function multipleDestroy(Request $request)
    {
        $ids = $request->input('ids');
        $this->authorize('delete', Article::find($ids[0]));

        try{
            DB::beginTransaction();

            if($ids){
                Article::whereIn('id',$ids)->delete();
            }
            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }

    public function updateField($id, Request $request)
    {
        try{
            $article = Article::find($id);
            $this->authorize('edit', $article);

            DB::beginTransaction();

            if($request->all()){

                foreach($request->all() as $name=>$value){
                    $article->$name = $value;
                }
                $article->save();
            }

            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }

    public function destroyPermanently($id)
    {
        $article = Article::withTrashed()->find($id);
        $this->authorize('delete', $article);

        try {
            DB::beginTransaction();

            $article->forceDelete();
            $article->seo()->forceDelete();
            $article->tags()->forceDelete();
            $article->permalink()->forceDelete();

            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }

    public function multipleDestroyTrash(Request $request)
    {
        $ids = $request->input('ids');
        $articles = Article::withTrashed()->whereIn('id',$ids)->get();

        $this->authorize('delete', $articles[0]);

        try{
            DB::beginTransaction();

            foreach ($articles as $article){
                $article->forceDelete();
                $article->seo()->forceDelete();
                $article->tags()->forceDelete();
                $article->permalink()->forceDelete();
            }

            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }

    public function restore($id)
    {
        $this->authorize('create', Article::class);

        try{
            DB::beginTransaction();
            $article = Article::withTrashed()->find($id);
            $article->restore();

            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }

    public function multipleRestoreTrash(Request $request)
    {
        $this->authorize('create', Article::class);

        try{
            DB::beginTransaction();
            $ids = $request->input('ids');

            if($ids){
                Article::withTrashed()->whereIn('id',$ids)->restore();
            }

            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }


}
