<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\PageDataTable;
use App\DataTables\TrashPageDataTable;
use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\Permalink;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PageController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Page::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PageDataTable $dataTable)
    {
        $title = "Danh sách trang tĩnh";
        return $dataTable->render('backend.datatable',compact("title"));
    }

    public function trash(TrashPageDataTable $dataTable)
    {
        $this->authorize('viewAny', Page::class);

        $title = "Trang tĩnh đã xóa";
        return $dataTable->render('backend.datatable',compact("title"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.page.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|max:255',
            'content' => 'required'
        ]);

        try{
            DB::beginTransaction();

            $page = new Page();
            $page->title = $request->input('title');
            $page->content = $request->input('content');
            $page->status = $request->input('status');
            $page->save();

            $linkData = [
                'type' => Page::PERMALINK_TYPE,
                'action' => Page::PERMALINK_ACTION
            ];

            $slug = $request->input('slug');

            if($slug){
                $slug = SlugService::createSlug(Permalink::class, 'slug', $slug);
                $linkData['slug'] = $slug;
            }

            $page->permalink()->create($linkData);

            $seo = $request->input('seo');

            if(!$seo['title']){
                $seo['title'] = $page->title;
            }

            if(!$seo['description']){
                $seo['description'] = $page->title;
            }

            $page->seo()->create($seo);

            DB::commit();

            return redirect()->route('pages.index')->with(['success' => 'Thêm trang tĩnh thành công.']);

        }catch (\Exception $e){
            DB::rollBack();
            return back()->withErrors([$e->getMessage()])->withInput($request->all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        $page->load('permalink');
        return view('backend.page.edit',compact("page"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Page $page)
    {
        $request->validate([
            'title' => 'required|max:255',
            'content' => 'required'
        ]);

        try{
            DB::beginTransaction();

            $page->title = $request->input('title');
            $page->status = $request->input('status');
            $page->content = $request->input('content');
            $page->save();

            $slug = $request->input('slug');

            if($slug && $slug != $page->permalink->slug){
                $slug = SlugService::createSlug(Permalink::class, 'slug', $slug);
                $page->permalink()->update(['slug' => $slug]);
            }

            $seo = $request->input('seo');

            if(!$seo['title']){
                $seo['title'] = $page->title;
            }

            if(!$seo['description']){
                $seo['description'] = $page->title;
            }

            if(empty($seo['index'])){
                $seo['index'] = 0;
            }

            if(empty($seo['follow'])){
                $seo['follow'] = 0;
            }

            $page->seo()->update($seo);

            DB::commit();

            return back()->with(['success' => 'Cập nhật trang tĩnh thành công.']);

        }catch (\Exception $e){
            DB::rollBack();
            return back()->withErrors([$e->getMessage()])->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Page $page)
    {
        try {
            DB::beginTransaction();
            $page->delete();
            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }

    public function duplicate($id)
    {
        $page = Page::find($id);
        $this->authorize('create', Page::class);

        try{
            DB::beginTransaction();

            $new = $page->replicate();

            $new->status = 'draft';
            $new->push();

            $page->relations = [];
            $page->load(['seo', 'permalink']);

            foreach ($page->getRelations() as $name => $values){
                $data = $values->toArray();

                if($name == 'permalink'){
                    $slug = SlugService::createSlug(Permalink::class, 'slug', $data['slug']);
                    $data['slug'] = $slug;
                }

                $new->{$name}()->create($data);
            }

            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }

    public function multipleDestroy(Request $request)
    {
        $ids = $request->input('ids');
        $this->authorize('delete', Page::find($ids[0]));

        try{
            DB::beginTransaction();

            Page::whereIn('id',$ids)->delete();

            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }

    public function destroyPermanently($id)
    {
        $page = Page::withTrashed()->find($id);
        $this->authorize('delete',$page);

        try {
            DB::beginTransaction();

            $page->forceDelete();
            $page->seo()->forceDelete();
            $page->permalink()->forceDelete();

            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }

    public function multipleDestroyTrash(Request $request)
    {
        $ids = $request->input('ids');
        $pages = Page::withTrashed()->whereIn('id',$ids)->get();

        $this->authorize('delete',$pages[0]);

        try{
            DB::beginTransaction();

            foreach ($pages as $page){
                $page->forceDelete();
                $page->seo()->forceDelete();
                $page->permalink()->forceDelete();
            }

            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }

    public function restore($id)
    {
        $page = Page::withTrashed()->find($id);
        $this->authorize('create', Page::class);

        try{
            DB::beginTransaction();
            $page->restore();

            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }

    public function multipleRestoreTrash(Request $request)
    {
        $this->authorize('create', Page::class);

        try{
            DB::beginTransaction();
            $ids = $request->input('ids');

            if($ids){
                Page::withTrashed()->whereIn('id',$ids)->restore();
            }

            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }
}
