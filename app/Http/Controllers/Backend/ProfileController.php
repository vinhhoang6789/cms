<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Administrator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    public function index()
    {
        $administrator = Auth::user();
        return view('backend.profile.edit',compact("administrator"));
    }

    public function edit(Request $request)
    {
        $administrator = Administrator::findOrFail(Auth::user()->id);

        $request->validate([
            'name' => 'required',
            'username' => 'required|unique:administrators,username,'.$administrator->id,
            'email' => 'required|email|unique:administrators,email,'.$administrator->id,
        ]);

        try{
            DB::beginTransaction();
            $administrator->name = $request->input('name');
            $administrator->username = $request->input('username');
            $administrator->email = $request->input('email');

            if($request->input('password')){
                $administrator->password = bcrypt($request->input('password'));
            }

            $administrator->avatar = $request->input('avatar');
            $administrator->save();

            DB::commit();
            return back()->with(['success' => 'Cập nhật thông tin thành công.']);
        }catch (\Exception $e){
            DB::rollback();
            return back()->withInput($request->all())->withErrors([$e->getMessage()]);
        }
    }
}
