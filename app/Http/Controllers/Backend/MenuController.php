<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\MenuDataTable;
use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Models\MenuItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MenuController extends Controller
{
    public function index(MenuDataTable $dataTable)
    {
        $this->authorize('edit-settings');

        $title = "Danh sách menu";
        return $dataTable->render('backend.datatable',compact("title"));
    }

    public function detail($menuId)
    {
        $this->authorize('edit-settings');

        $menu = Menu::with(['menuItems' => function($q){
            $q->orderBy('position', 'asc');
        }])->findOrFail($menuId);

        return view('backend.menu-detail', compact("menu", "menuId"));
    }

    public function detailItem($id)
    {
        $this->authorize('edit-settings');

        $item = MenuItem::findOrFail($id);
        return view('backend.menu-item-detail', compact("item"));
    }

    public function addMenuItem(Request $request)
    {
        $this->authorize('edit-settings');

        $request->validate([
            'title' => 'required|max:255',
            'url' => 'required|max:255',
            'menu_id' => 'required',
        ]);

        $maxPosition = MenuItem::selectRaw('MAX(position) as max_postion')
            ->where('menu_id', $request->input('menu_id'))
            ->where('parent', 0)
            ->first()->max_postion;

        try{
            DB::beginTransaction();

            $menuItem = new MenuItem();
            $menuItem->menu_id = $request->input('menu_id');
            $menuItem->title = $request->input('title');
            $menuItem->url = $request->input('url');
            $menuItem->position = $maxPosition + 1;
            $menuItem->target = $request->input('target');
            $menuItem->save();

            DB::commit();

            return response()->json($menuItem);

        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }

    public function updateMenuItem(Request $request)
    {
        $this->authorize('edit-settings');

        $request->validate([
            'title' => 'required|max:255',
            'url' => 'required|max:255',
        ]);

        try{
            DB::beginTransaction();

            $menuItem = MenuItem::find($request->input('id'));
            $menuItem->title = $request->input('title');
            $menuItem->url = $request->input('url');
            $menuItem->target = $request->input('target');
            $menuItem->save();

            DB::commit();

            return redirect()->route('backend.menuItem.detail', $menuItem->id)->with('success',"Cập nhật dữ liệu thành công!");

        }catch (\Exception $e){
            DB::rollBack();
            return response()->json($e->getMessage(), 422);
        }
    }

    public function updateMenuItemPosition(Request $request)
    {
        $this->authorize('edit-settings');

        $post = $request->input('menu_data');

        try{
            DB::beginTransaction();

            $i = 1;
            foreach ($post[0] as $ro) {
                $pid = $ro['id'];

                if (isset($ro['children']) && $ro['children'][0]) {
                    $this->recursiveUpdateMenuItem($pid, $ro['children'][0]);
                }
                MenuItem::where('id', $pid)->update(['position' => $i, 'parent' => 0]);
                $i++;
            }

            DB::commit();

            return response()->json(['success' => true]);

        }catch (\Exception $e){
            DB::rollBack();
            return response()->json($e->getMessage(), 422);
        }
    }

    protected function recursiveUpdateMenuItem($pid, $children)
    {
        $this->authorize('edit-settings');

        $ci = 1;
        foreach ($children as $c) {
            $id = $c['id'];
            MenuItem::where('id', $id)->update(['position' => $ci, 'parent' => $pid]);
            $ci++;
            if (isset($c['children']) && $c['children'][0]) {

                $this->recursiveUpdateMenuItem($id, $c['children'][0]);
            }
        }
    }

    public function deleteMenuItem($id)
    {
        $this->authorize('edit-settings');

        try{
            DB::beginTransaction();
            MenuItem::find($id)->delete();
            DB::commit();

            return response()->json(['success']);
        }catch (\Exception $e){
            DB::rollBack();

            return response()->json(['message' => $e->getMessage()], 422);
        }
    }
}
