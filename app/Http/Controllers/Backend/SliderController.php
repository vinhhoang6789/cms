<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\SliderDataTable;
use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SliderController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Slider::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SliderDataTable $dataTable)
    {
        $title = "Danh sách slides";
        return $dataTable->render('backend.datatable',compact("title"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create()
    {
        return view('backend.slider.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'image' => 'required|max:255'
        ]);

        try{
            DB::beginTransaction();

            $slider = new Slider();
            $slider->name = $request->input('name');
            $slider->group = $request->input('group');
            $slider->image = $request->input('image');
            $slider->url = $request->input('url');
            $slider->description = $request->input('description');
            $slider->active = $request->input('active');
            $slider->save();

            DB::commit();
            return redirect()->route('sliders.index')->with(['success' => 'Thêm slider thành công.']);
        }catch (\Exception $e){
            DB::rollBack();

            return back()->withErrors([$e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Slider $slider)
    {
        return view('backend.slider.edit', compact("slider"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Slider $slider)
    {
        $request->validate([
            'name' => 'required|max:255',
            'image' => 'required|max:255',
        ]);

        try{
            DB::beginTransaction();

            $slider->name = $request->input('name');
            $slider->image = $request->input('image');
            $slider->group = $request->input('group');
            $slider->url = $request->input('url');
            $slider->description = $request->input('description');
            $slider->active = $request->input('active');
            $slider->save();

            DB::commit();

            return back()->with(['success' => 'Cập nhật slider thành công.']);

        }catch (\Exception $e){
            DB::rollBack();
            return back()->withErrors([$e->getMessage()])->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Slider $slider)
    {
        try {
            DB::beginTransaction();
            $slider->delete();
            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }

    public function updateField($id, Request $request)
    {
        try{
            $slider = Slider::find($id);
            $this->authorize('edit', $slider);

            DB::beginTransaction();

            if($request->all()){
                foreach($request->all() as $name=>$value){
                    $slider->$name = $value;
                }

                $slider->save();
            }

            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }
}
