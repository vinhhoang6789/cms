<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Spatie\ResponseCache\Facades\ResponseCache;

class SettingController extends Controller
{
    public function index(){
        $this->authorize('edit-settings');

        $settings = Setting::where('group','general')->get();
        $icon = 'settings';
        $title = "Cấu hình chung";
        $type = 'general';

        return view('backend.setting',compact("settings","icon", "title","type"));
    }

    public function cache(){
        $this->authorize('edit-settings');

        $settings = Setting::where('group','cache')->get();
        $icon = 'settings';
        $title = "Cấu hình cache";
        $type = 'cache';

        return view('backend.setting',compact("settings","icon", "title","type"));
    }

    public function save(Request $request)
    {
        $this->authorize('edit-settings');

        try{
            DB::beginTransaction();

            $setting = $request->input('setting');
            $type = $request->input('type');

            if($type == 'cache'){
                $settings = Setting::where('group','cache')->get()->keyBy('id')->toArray();
                $ids = array_keys($settings);

                foreach ($ids as $id){
                    if(empty($setting[$id])){
                        $setting[$id] = 0;
                    }
                }
            }

            $value = array_map(function($value, $key){
                return [
                    'id' => $key,
                    'value' => $value,
                ];
            }, $setting, array_keys($setting));

            \Batch::update(new Setting, $value, 'id');

            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            return back()->withErrors(['message'=>[$e->getMessage()]]);
        }

        return back()->with('success',"Cập nhật dữ liệu thành công!");
    }

    public function clearCache()
    {
        ResponseCache::clear();

        return back()->with('success',"Đã xóa toàn bộ cache hệ thống.");
    }
}
