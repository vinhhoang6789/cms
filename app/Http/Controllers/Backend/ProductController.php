<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\ProductDataTable;
use App\DataTables\TrashProductDataTable;
use App\Http\Controllers\Controller;
use App\Models\Permalink;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductImage;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Product::class,'products');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ProductDataTable $dataTable)
    {
        $title = "Danh sách sản phẩm";
        return $dataTable->render('backend.datatable',compact("title"));
    }

    public function trash(TrashProductDataTable $dataTable)
    {
        $title = "Sản phẩm đã xóa";
        return $dataTable->render('backend.datatable',compact("title"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $categories = ProductCategory::orderBy('title')->get()->groupBy('parent');
        return view('backend.product.add',compact("categories"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|max:255',
            'price' => 'required',
        ]);

        try{
            DB::beginTransaction();

            $product = new Product();
            $product->title = $request->input('title');
            $product->product_category_id = $request->input('product_category_id');
            $product->price = $request->input('price');
            $product->status = $request->input('status');
            $product->save();

            $linkData = [
                'type' => Product::PERMALINK_TYPE,
                'action' => Product::PERMALINK_ACTION,
            ];

            $slug = $request->input('slug');

            if($slug){
                $slug = SlugService::createSlug(Permalink::class, 'slug', $slug);
                $linkData['slug'] = $slug;
            }
            $product->permalink()->create($linkData);

            if($images = $request->input('images')){
                $images = array_map(function($img){
                    return ['url' => $img];
                }, $images);

                $product->images()->createMany($images);
            }

            $seo = $request->input('seo');

            if(!$seo['title']){
                $seo['title'] = $product->title;
            }

            if(!$seo['description']){
                $seo['description'] = $product->title;
            }

            $product->seo()->create($seo);

            DB::commit();

            return redirect()->route('products.index')->with(['success' => 'Thêm sản phẩm thành công.']);

        }catch (\Exception $e){
            DB::rollBack();
            return back()->withErrors([$e->getMessage()])->withInput($request->all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Product $product)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Product $product)
    {
        $product->load(['permalink', 'seo', 'images']);

        $categories = ProductCategory::orderBy('title')->get()->groupBy('parent');
        return view('backend.product.edit',compact("categories","product"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Product $product)
    {
        $request->validate([
            'title' => 'required|max:255',
            'price' => 'required',
        ]);

        try{
            DB::beginTransaction();

            $product->title = $request->input('title');
            $product->product_category_id = $request->input('product_category_id');
            $product->price = $request->input('price');
            $product->status = $request->input('status');
            $product->save();

            $slug = $request->input('slug');

            if($slug && $slug != $product->permalink->slug){
                $slug = SlugService::createSlug(Permalink::class, 'slug', $slug);
                $product->permalink()->update(['slug' => $slug]);
            }

            $product->images()->delete();

            if($images = $request->input('images')){
                $images = array_map(function($img){
                    return [
                        'url' => $img
                    ];
                }, $images);

                $product->images()->createMany($images);
            }

            $seo = $request->input('seo');

            if(!$seo['title']){
                $seo['title'] = $product->title;
            }

            if(!$seo['description']){
                $seo['description'] = $product->title;
            }

            if(empty($seo['index'])){
                $seo['index'] = 0;
            }

            if(empty($seo['follow'])){
                $seo['follow'] = 0;
            }

            $product->seo()->update($seo);

            DB::commit();

            return back()->with(['success' => 'Cập nhật sản phẩm thành công.']);

        }catch (\Exception $e){
            DB::rollBack();
            return back()->withErrors([$e->getMessage()])->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Product $product)
    {
        try {
            DB::beginTransaction();
            $product->delete();
            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }

    public function updateField($id, Request $request)
    {
        $product = Product::find($id);
        $this->authorize('edit', $product);

        try{
            DB::beginTransaction();

            if($request->all()){
                foreach($request->all() as $name=>$value){
                    $product->$name = $value;
                }
                $product->save();
            }

            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }

    public function duplicate($id)
    {
        $this->authorize('create', Product::class);

        try{
            DB::beginTransaction();

            $product = Product::find($id);
            $new = $product->replicate();

            $new->status = 'draft';
            $new->push();

            $product->relations = [];
            $product->load(['seo', 'permalink', 'images']);

            foreach ($product->getRelations() as $name => $values){
                $data = $values->toArray();

                if($name == 'permalink'){
                    $slug = SlugService::createSlug(Permalink::class, 'slug', $data['slug']);
                    $data['slug'] = $slug;

                    $new->{$name}()->create($data);

                }else if($name == 'images'){
                    $new->{$name}()->createMany($data);

                }else{
                    $new->{$name}()->create($data);
                }
            }

            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }

    public function multipleDestroy(Request $request)
    {
        $ids = $request->input('ids');
        $this->authorize('delete', Product::find($ids[0]));

        try{
            DB::beginTransaction();

            Product::whereIn('id',$ids)->delete();

            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }

    public function destroyPermanently($id)
    {
        try {
            DB::beginTransaction();
            $product = Product::withTrashed()->find($id);

            $product->forceDelete();
            $product->seo()->forceDelete();
            $product->permalink()->forceDelete();
            $product->images()->forceDelete();

            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }

    public function multipleDestroyTrash(Request $request)
    {
        $ids = $request->input('ids');
        $this->authorize('delete', Product::find($ids[0]));

        try{
            DB::beginTransaction();

            $products = Product::withTrashed()->whereIn('id',$ids)->get();

            foreach ($products as $product){
                $product->forceDelete();
                $product->seo()->forceDelete();
                $product->permalink()->forceDelete();
                $product->images()->forceDelete();
            }

            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }

    public function restore($id)
    {
        $this->authorize('create',Product::class);

        try{
            DB::beginTransaction();
            $product = Product::withTrashed()->find($id);
            $product->restore();

            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }

    public function multipleRestoreTrash(Request $request)
    {
        $this->authorize('create',Product::class);

        try{
            DB::beginTransaction();
            $ids = $request->input('ids');

            if($ids){
                Product::withTrashed()->whereIn('id',$ids)->restore();
            }

            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }
}
