<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\RoleDatatable;
use App\Http\Controllers\Controller;
use App\Models\Administrator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Role::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(RoleDatatable $dataTable)
    {

        $title = "Phân quyền hệ thống";
        return $dataTable->render('backend.datatable',compact("title"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $manageableList = Administrator::manageableList();
        return view('backend.role.add', compact('manageableList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:roles|max:255'
        ]);

        try{
            DB::beginTransaction();

            $isSuperAdmin = $request->input('is_super_admin');
            $permissions = $request->input('permission');

            $role = Role::create([
                'name' => $request->input('name'),
                'guard_name' => 'administrator',
                'is_super_admin' => $isSuperAdmin
            ]);

            if(!$isSuperAdmin && $permissions){
                $data = [];

                foreach ($permissions as $table => $permission){

                    foreach (array_keys($permission) as $action){
                        $data[] = $action.' '.$table;
                    }
                }

                $permissionModel = Permission::whereIn('name', $data)->get();

                $role->syncPermissions($permissionModel);
            }

            DB::commit();

            return redirect()->route('roles.index')->with(['success' => 'Thêm quyền thành công.']);
        }catch (\Exception $e){
            DB::rollBack();
            return back()->withErrors([$e->getMessage()])->withInput($request->all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Role  $Role
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Role $role)
    {
        $role->load('permissions');
        $permissions = $role->permissions->pluck('name')->toArray();

        $manageableList = Administrator::manageableList();
        return view('backend.role.edit', compact('manageableList', "role", "permissions"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Role  $role
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Role $role)
    {
        $request->validate([
            'name' => 'required|unique:roles,name,'.$role->id.'|max:255'
        ]);

        try{
            DB::beginTransaction();

            $isSuperAdmin = $request->input('is_super_admin');
            $permissions = $request->input('permission');

            $role->update([
                'name' => $request->input('name'),
                'is_super_admin' => $isSuperAdmin
            ]);

            if(!$isSuperAdmin && $permissions){
                $data = [];

                foreach ($permissions as $table => $permission){

                    foreach (array_keys($permission) as $action){
                        $data[] = $action.' '.$table;
                    }
                }

                $permissionModel = Permission::whereIn('name', $data)->get();
                $role->syncPermissions($permissionModel);

            }else{
                $permissions = $role->getAllPermissions();

                foreach($permissions as $permission){
                    $role->revokePermissionTo($permission);
                }

            }

            DB::commit();

            return back()->with(['success' => 'Chỉnh sửa quyền thành công.']);
        }catch (\Exception $e){
            DB::rollBack();
            return back()->withErrors([$e->getMessage()])->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Role  $role
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Role $role)
    {
        try {
            DB::beginTransaction();
            $role->delete();

            DB::commit();
            return response()->json(['success' => true]);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }
}
