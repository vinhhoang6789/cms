<?php

namespace App\Policies;

use App\Models\Administrator;
use App\Models\ProductCategory;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductCategoryPolicy
{
    use HandlesAuthorization;


    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @return mixed
     */
    public function viewAny(Administrator $adminAuth)
    {
        return $adminAuth->can('visible product_category');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\ProductCategory $productCategory
     * @return mixed
     */
    public function view(Administrator $adminAuth,ProductCategory $productCategory)
    {
        return $adminAuth->can('visible product_category');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @return mixed
     */
    public function create(Administrator $adminAuth)
    {
        return $adminAuth->can('create product_category');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\ProductCategory $productCategory
     * @return mixed
     */
    public function update(Administrator $adminAuth,ProductCategory $productCategory)
    {
        return $adminAuth->can('edit product_category');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\ProductCategory $productCategory
     * @return mixed
     */
    public function delete(Administrator $adminAuth,ProductCategory $productCategory)
    {
        return $adminAuth->can('delete product_category');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\ProductCategory $productCategory
     * @return mixed
     */
    public function restore(Administrator $adminAuth,ProductCategory $productCategory)
    {
        return $adminAuth->can('create product_category');
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\ProductCategory $productCategory
     * @return mixed
     */
    public function forceDelete(Administrator $adminAuth,ProductCategory $productCategory)
    {
        return $adminAuth->can('delete product_category');
    }
}
