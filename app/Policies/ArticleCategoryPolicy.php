<?php

namespace App\Policies;

use App\Models\Administrator;
use App\Models\ArticleCategory;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArticleCategoryPolicy
{
    use HandlesAuthorization;


    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @return mixed
     */
    public function viewAny(Administrator $adminAuth)
    {

        return $adminAuth->can('visible article_category');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\ArticleCategory  $articleCategory
     * @return mixed
     */
    public function view(Administrator $adminAuth, ArticleCategory $articleCategory)
    {
        return $adminAuth->can('visible article_category');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @return mixed
     */
    public function create(Administrator $adminAuth)
    {
        return $adminAuth->can('create article_category');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\ArticleCategory  $articleCategory
     * @return mixed
     */
    public function update(Administrator $adminAuth, ArticleCategory $articleCategory)
    {
        return $adminAuth->can('edit article_category');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\ArticleCategory  $articleCategory
     * @return mixed
     */
    public function delete(Administrator $adminAuth, ArticleCategory $articleCategory)
    {
        return $adminAuth->can('delete article_category');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\ArticleCategory  $articleCategory
     * @return mixed
     */
    public function restore(Administrator $adminAuth, ArticleCategory $articleCategory)
    {
        return $adminAuth->can('create article_category');
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\ArticleCategory  $articleCategory
     * @return mixed
     */
    public function forceDelete(Administrator $adminAuth, ArticleCategory $articleCategory)
    {
        return $adminAuth->can('delete article_category');
    }
}
