<?php

namespace App\Policies;

use App\Models\Administrator;
use App\Models\Page;
use Illuminate\Auth\Access\HandlesAuthorization;

class PagePolicy
{
    use HandlesAuthorization;


    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @return mixed
     */
    public function viewAny(Administrator $adminAuth)
    {
        return $adminAuth->can('visible page');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\Page $page
     * @return mixed
     */
    public function view(Administrator $adminAuth,Page $page)
    {
        return $adminAuth->can('visible page');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @return mixed
     */
    public function create(Administrator $adminAuth)
    {
        return $adminAuth->can('create page');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\Page $page
     * @return mixed
     */
    public function update(Administrator $adminAuth,Page $page)
    {
        return $adminAuth->can('edit page');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\Page $page
     * @return mixed
     */
    public function delete(Administrator $adminAuth,Page $page)
    {
        return $adminAuth->can('delete page');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\Page $page
     * @return mixed
     */
    public function restore(Administrator $adminAuth,Page $page)
    {
        return $adminAuth->can('create page');
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\Page $page
     * @return mixed
     */
    public function forceDelete(Administrator $adminAuth,Page $page)
    {
        return $adminAuth->can('delete page');
    }
}
