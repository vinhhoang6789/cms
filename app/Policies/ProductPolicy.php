<?php

namespace App\Policies;

use App\Models\Administrator;
use App\Models\Product;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;


    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @return mixed
     */
    public function viewAny(Administrator $adminAuth)
    {
        return $adminAuth->can('visible product');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\Product $product
     * @return mixed
     */
    public function view(Administrator $adminAuth, Product $product)
    {
        return $adminAuth->can('visible product');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @return mixed
     */
    public function create(Administrator $adminAuth)
    {
        return $adminAuth->can('create product');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\Product $product
     * @return mixed
     */
    public function update(Administrator $adminAuth, Product $product)
    {
        return $adminAuth->can('edit product');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\Product $product
     * @return mixed
     */
    public function delete(Administrator $adminAuth, Product $product)
    {
        return $adminAuth->can('delete product');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\Product $product
     * @return mixed
     */
    public function restore(Administrator $adminAuth, Product $product)
    {
        return $adminAuth->can('create product');
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\Product $product
     * @return mixed
     */
    public function forceDelete(Administrator $adminAuth, Product $product)
    {
        return $adminAuth->can('delete product');
    }
}
