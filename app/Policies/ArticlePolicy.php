<?php

namespace App\Policies;

use App\Models\Administrator;
use App\Models\Article;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArticlePolicy
{
    use HandlesAuthorization;


    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @return mixed
     */
    public function viewAny(Administrator $adminAuth)
    {
        return $adminAuth->can('visible article');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\Article $article
     * @return mixed
     */
    public function view(Administrator $adminAuth, Article $article)
    {
        return $adminAuth->can('visible article');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @return mixed
     */
    public function create(Administrator $adminAuth)
    {
        return $adminAuth->can('create article');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\Article $article
     * @return mixed
     */
    public function update(Administrator $adminAuth, Article $article)
    {
        return $adminAuth->can('edit article');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\Article $article
     * @return mixed
     */
    public function delete(Administrator $adminAuth, Article $article)
    {
        return $adminAuth->can('delete article');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\Article $article
     * @return mixed
     */
    public function restore(Administrator $adminAuth, Article $article)
    {
        return $adminAuth->can('create article');
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\Article $article
     * @return mixed
     */
    public function forceDelete(Administrator $adminAuth, Article $article)
    {
        return $adminAuth->can('delete article');
    }
}
