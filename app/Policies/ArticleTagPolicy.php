<?php

namespace App\Policies;

use App\Models\Administrator;
use App\Models\Tag;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArticleTagPolicy
{
    use HandlesAuthorization;


    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @return mixed
     */
    public function viewAny(Administrator $adminAuth)
    {
        return $adminAuth->can('visible article_tag');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\Tag $tag
     * @return mixed
     */
    public function view(Administrator $adminAuth, Tag $tag)
    {
        return $adminAuth->can('visible article_tag');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @return mixed
     */
    public function create(Administrator $adminAuth)
    {
        return $adminAuth->can('create article_tag');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\Tag $tag
     * @return mixed
     */
    public function update(Administrator $adminAuth, Tag $tag)
    {
        return $adminAuth->can('edit article_tag');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\Tag $tag
     * @return mixed
     */
    public function delete(Administrator $adminAuth, Tag $tag)
    {
        return $adminAuth->can('delete article_tag');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\Tag $tag
     * @return mixed
     */
    public function restore(Administrator $adminAuth, Tag $tag)
    {
        return $adminAuth->can('create article_tag');
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\Tag $tag
     * @return mixed
     */
    public function forceDelete(Administrator $adminAuth, Tag $tag)
    {
        return $adminAuth->can('delete article_tag');
    }
}
