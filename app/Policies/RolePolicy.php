<?php

namespace App\Policies;

use App\Models\Administrator;
use Spatie\Permission\Models\Role;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;


    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @return mixed
     */
    public function viewAny(Administrator $adminAuth)
    {
        return $adminAuth->can('visible role');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  Role $role
     * @return mixed
     */
    public function view(Administrator $adminAuth, Role $role)
    {
        return $adminAuth->can('visible role');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @return mixed
     */
    public function create(Administrator $adminAuth)
    {
        return $adminAuth->can('create role');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  Role $role
     * @return mixed
     */
    public function update(Administrator $adminAuth, Role $role)
    {
        return $adminAuth->can('edit role');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  Role $role
     * @return mixed
     */
    public function delete(Administrator $adminAuth, Role $role)
    {
        return $adminAuth->can('delete role');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  Role $role
     * @return mixed
     */
    public function restore(Administrator $adminAuth, Role $role)
    {
        return $adminAuth->can('create role');
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  Role $role
     * @return mixed
     */
    public function forceDelete(Administrator $adminAuth, Role $role)
    {
        return $adminAuth->can('delete role');
    }
}
