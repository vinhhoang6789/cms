<?php

namespace App\Policies;

use App\Models\Administrator;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdministratorPolicy
{
    use HandlesAuthorization;


    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @return mixed
     */
    public function viewAny(Administrator $adminAuth)
    {
        return $adminAuth->can('visible administrator');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\Administrator  $administrator
     * @return mixed
     */
    public function view(Administrator $adminAuth, Administrator $administrator)
    {
        return $adminAuth->can('visible administrator');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @return mixed
     */
    public function create(Administrator $adminAuth)
    {
        return $adminAuth->can('create administrator');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\Administrator  $administrator
     * @return mixed
     */
    public function update(Administrator $adminAuth, Administrator $administrator)
    {
        return $adminAuth->can('edit administrator');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\Administrator  $administrator
     * @return mixed
     */
    public function delete(Administrator $adminAuth, Administrator $administrator)
    {
        return $adminAuth->can('delete administrator');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\Administrator  $administrator
     * @return mixed
     */
    public function restore(Administrator $adminAuth, Administrator $administrator)
    {
        return $adminAuth->can('create administrator');
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\Administrator  $administrator
     * @return mixed
     */
    public function forceDelete(Administrator $adminAuth, Administrator $administrator)
    {
        return $adminAuth->can('delete administrator');
    }
}
