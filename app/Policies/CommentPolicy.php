<?php

namespace App\Policies;

use App\Models\Administrator;
use App\Models\Article;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommentPolicy
{
    use HandlesAuthorization;


    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @return mixed
     */
    public function viewAny(Administrator $adminAuth)
    {
        return $adminAuth->can('visible comment');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\Comment $comment
     * @return mixed
     */
    public function view(Administrator $adminAuth, Comment $comment)
    {
        return $adminAuth->can('visible comment');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @return mixed
     */
    public function create(Administrator $adminAuth)
    {
        return $adminAuth->can('create comment');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\Comment $comment
     * @return mixed
     */
    public function update(Administrator $adminAuth, Comment $comment)
    {
        return $adminAuth->can('edit comment');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\Comment $comment
     * @return mixed
     */
    public function delete(Administrator $adminAuth, Comment $comment)
    {
        return $adminAuth->can('delete comment');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\Comment $comment
     * @return mixed
     */
    public function restore(Administrator $adminAuth, Comment $comment)
    {
        return $adminAuth->can('create comment');
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  \App\Models\Comment $comment
     * @return mixed
     */
    public function forceDelete(Administrator $adminAuth, Comment $comment)
    {
        return $adminAuth->can('delete comment');
    }
}
