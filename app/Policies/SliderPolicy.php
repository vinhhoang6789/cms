<?php

namespace App\Policies;

use App\Models\Administrator;
use App\Models\Slider;
use Illuminate\Auth\Access\HandlesAuthorization;

class SliderPolicy
{
    use HandlesAuthorization;


    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @return mixed
     */
    public function viewAny(Administrator $adminAuth)
    {
        return $adminAuth->can('visible slide');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  Slider $slider
     * @return mixed
     */
    public function view(Administrator $adminAuth, Slider $slider)
    {
        return $adminAuth->can('visible slide');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @return mixed
     */
    public function create(Administrator $adminAuth)
    {
        return $adminAuth->can('create slide');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  Slider $slider
     * @return mixed
     */
    public function update(Administrator $adminAuth, Slider $slider)
    {
        return $adminAuth->can('edit slide');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  Slider $slider
     * @return mixed
     */
    public function delete(Administrator $adminAuth, Slider $slider)
    {
        return $adminAuth->can('delete slide');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  Slider $slider
     * @return mixed
     */
    public function restore(Administrator $adminAuth, Slider $slider)
    {
        return $adminAuth->can('create slide');
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  Slider $slider
     * @return mixed
     */
    public function forceDelete(Administrator $adminAuth, Slider $slider)
    {
        return $adminAuth->can('delete slide');
    }
}
