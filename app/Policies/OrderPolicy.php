<?php

namespace App\Policies;

use App\Models\Administrator;
use App\Models\Order;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderPolicy
{
    use HandlesAuthorization;


    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @return mixed
     */
    public function viewAny(Administrator $adminAuth)
    {
        return $adminAuth->can('visible order');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  Order $order
     * @return mixed
     */
    public function view(Administrator $adminAuth, Order $order)
    {
        return $adminAuth->can('visible order');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @return mixed
     */
    public function create(Administrator $adminAuth)
    {
        return $adminAuth->can('create order');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  Order $order
     * @return mixed
     */
    public function update(Administrator $adminAuth, Order $order)
    {
        return $adminAuth->can('edit order');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  Order $order
     * @return mixed
     */
    public function delete(Administrator $adminAuth, Order $order)
    {
        return $adminAuth->can('delete order');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  Order $order
     * @return mixed
     */
    public function restore(Administrator $adminAuth, Order $order)
    {
        return $adminAuth->can('create order');
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\Administrator  $adminAuth
     * @param  Order $order
     * @return mixed
     */
    public function forceDelete(Administrator $adminAuth, Order $order)
    {
        return $adminAuth->can('delete order');
    }
}
