<?php
namespace App\Traits;
use App\Models\Article;
use App\Models\ArticleCategory;

trait ArticleTrait{
    public function recentPosts($except = [])
    {
        return Article::with('permalink')
            ->whereNotIn('id', $except)
            ->orderBy('created_at','desc')
            ->take(3)->get();
    }

    public function relatedPosts($except = [])
    {
        return Article::with('permalink')
            ->whereNotIn('id', $except)
            ->orderBy('created_at','desc')
            ->take(8)->get();
    }

    public function popularPosts($except = [])
    {
        $popularPosts = Article::with('permalink')->where('is_featured', true)
            ->whereNotIn('id',$except)->take(5)->get();

        if($popularPosts->isEmpty()){
            $popularPosts = Article::with('permalink')->whereNotIn('id',$except)->take(5)->get();
        }
        return $popularPosts;
    }


    public function articleCategory($id, $data = [])
    {
        if($id){
            $cat = ArticleCategory::with('permalink')->find($id);
            array_unshift($data, $cat);

            if($cat->parent == 0){
                return $data;

            }else{
                return $this->articleCategory($cat->parent, $data);
            }
        }

    }

}
