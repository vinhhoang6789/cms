<?php
namespace App\Traits;

use App\Models\Menu;
use App\Models\Setting;

trait FrontendConfig{
    protected $data = [];

    public function __construct()
    {
        //Load default seo config
        $settings = Setting::all()->keyBy('title');
        config(['seotools.meta.defaults.title' => $settings['Tiêu đề']->value]);
        config(['seotools.opengraph.defaults.title' => $settings['Tiêu đề']->value]);
        config(['seotools.json-ld.defaults.title' => $settings['Tiêu đề']->value]);

        config(['seotools.meta.defaults.description' => $settings['Meta Description']->value]);
        config(['seotools.opengraph.defaults.description' => $settings['Meta Description']->value]);
        config(['seotools.json-ld.defaults.description' => $settings['Meta Description']->value]);

        //Set site config
        config(['app.name' => $settings['Site Name']->value]);
        config(['app.logo' => asset($settings['Logo']->value)]);

        //Cache Setting
        config(['responsecache.enabled' => $settings['Chế độ cache']->value ? true : false]);
    }

    public function getMenu($id = 1)
    {
        $menu = Menu::with(['menuItems' => function($q){
            $q->orderBy('position', 'asc');
        }])->findOrFail($id);

        $items = $menu->menuItems->groupBy('parent');

        return $this->generateMenu($items);
    }

    public function html($parent, $children, $lvl){
        $url = url($parent->url);
        $isActive = url()->current() == $url ? ' active' : '';
        $hasSubmenu = !empty($children[$parent->id]);

        $liClass = 'nav-item';
        $aClass = 'nav-link';

        if($lvl == 1 && $hasSubmenu){
            $liClass .= ' dropdown';
            $aClass .= ' dropdown-toggle';

        }elseif ($lvl >= 2){
            $liClass = '';
            $aClass = 'dropdown-item';

            if($hasSubmenu){
                $aClass .= ' dropdown-toggle';
                $liClass = 'dropdown-submenu';
            }
        }


        $liClass.= $isActive;

        $recursiveItem = '';

        if($hasSubmenu){
            $recursiveItem = '<ul class="dropdown-menu">'.$this->recursiveChildren($parent->id, $children, $lvl).'</ul>';
        }

        return '<li class="'.$liClass.'">
            <a class="'.$aClass.'" href="'.$url.'">'.$parent->title.'</a>
            '.$recursiveItem.'
        </li>';

    }

    public function generateMenu($data, $lvl = 1){
        $html = '';

        if(!empty($data[0])){
            $parents = $data[0];
            $children = $data;
            unset($children[0]);

            foreach ($parents as $parent){
                $html .= $this->html($parent, $children, $lvl);
            }
        }

        return $html;
    }

    public function recursiveChildren($parent, $children, $lvl){
        $html = '';

        $lvl += 1;

        foreach ($children[$parent] as $parent){
            $html .= $this->html($parent, $children, $lvl);
        }

        return $html;
    }

}
