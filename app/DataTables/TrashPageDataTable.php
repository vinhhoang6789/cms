<?php

namespace App\DataTables;

use App\Models\Page;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class TrashPageDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('created_at', function ($record) {
                return $record->created_at->format('d-m-Y H:i:s');
            })
            ->editColumn('status', function ($record) {
                return '<span class="badge badge-danger">Đã xóa</span>';
            })
            ->editColumn('check', function($record){
                return '<label class="pos-rel">
                            <input type="checkbox" class="ace check-item" value="'.$record->id.'"/>
                            <span class="lbl"></span>
                        </label>';
            })
            ->editColumn('title', function($record){
                return '<a href="'.url($record->permalink->slug).'" target="_blank">'.$record->title.'</a>';
            })
            ->addColumn('action', function ($record) {
                $toggle = 'data-toggle="tooltip" data-placement="top"';
                return '
                    <div class="action-buttons">
                        <a '.$toggle.' title="Xóa vĩnh viễn" class="delete-permanently-post red" href="'.route('pages.destroyPermanently', $record->id).'">
                            <i class="ace-icon fa fa-remove bigger-130"></i>
                        </a>
                        <a '.$toggle.' title="Khôi phục" class="restore-post green" href="'.route('pages.restore', $record->id).'">
                            <i class="ace-icon fa fa-recycle bigger-130"></i>
                        </a>
                    </div>

                ';
            })
            ->rawColumns(['check','action','status','title']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Page $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Page $model)
    {
        return $model::onlyTrashed()->newQuery()->with('permalink');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('pagedatatable-table')
            ->setTableAttribute('class', 'table table-striped table-bordered table-hover dataTable no-footer')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Blfrtip')
            ->lengthChange(true)
            ->orderBy(4,'desc')
            ->buttons(
                Button::make()->text('<i class="fa fa-remove" data-url="'.route('pages.multipleDestroyTrash').'"></i> Xóa vĩnh viễn')
                    ->className('btn btn-danger multiple-delete-trash'),
                Button::make()->text('<i class="fa fa-recycle" data-url="'.route('pages.multipleRestore').'"></i> Khôi phục')
                    ->className('btn btn-success multiple-restore'),

                Button::make()->text('Danh sách trang tĩnh')
                    ->className('btn btn-success')
                    ->action('goToPage("'.route('pages.index').'")'),
                Button::make()->text('Trang tĩnh đã xóa')
                    ->className('btn btn-warning')
                    ->action('goToPage("'.route('pages.trash').'")'),
                Button::make('print')->className('btn btn-info'),
                Button::make('reload')->text('<i class="fa fa-refresh"></i> Tải lại')->className('btn btn-default'),
            );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('check')
                ->exportable(false)
                ->printable(false)
                ->width(20)
                ->title('<label class="pos-rel">
                                        <input type="checkbox" class="ace check-all" />
                                        <span class="lbl"></span>
                                    </label>'),
            Column::make('id')->title('ID')->hidden(),
            Column::make('title')->title('Tiêu đề'),
            Column::make('status')->title('Trạng thái'),
            Column::make('created_at')->title("Ngày tạo"),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(100)
                ->addClass('text-center')->title('Thao tác'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Page_' . date('YmdHis');
    }
}
