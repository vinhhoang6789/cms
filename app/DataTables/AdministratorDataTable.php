<?php

namespace App\DataTables;

use App\Models\Administrator;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class AdministratorDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('created_at', function ($record) {
                return $record->created_at->format('d-m-Y');
            })
            ->addColumn('action', function ($record) {
                $editLink = route('administrators.edit',$record->id);
                $toggle = 'data-toggle="tooltip" data-placement="top"';
                return '
                     <div class="action-buttons">
                        <a '.$toggle.' title="Chỉnh sửa" href="'.$editLink.'" class="blue"><i class="fa fa-pencil ace-icon bigger-130"></i></a>
                        <a '.$toggle.' title="Xóa" class="delete-post red" href="'.route('administrators.destroy', $record->id).'">
                            <i class="fa fa-remove ace-icon bigger-130"></i>
                        </a>
                </div>
                ';
            })
            ->rawColumns(['action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Administrator $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Administrator $model)
    {
        return $model->newQuery()->with('role');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('articledatatable-table')
            ->setTableAttribute('class', 'table table-striped table-bordered table-hover dataTable no-footer')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Blfrtip')
            ->orderBy(5)
            ->buttons(
                Button::make('reload')->text('<i class="fa fa-refresh"></i> Tải lại')->className('btn btn-default'),
                Button::make('create')->text('<i class="fa fa-plus"></i> Thêm mới')->className('btn btn-success'),
            );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')->title("ID")->hidden(),
            Column::make('name')->title("Tên người dùng"),
            Column::make('username')->title("Tên đăng nhập"),
            Column::make('email')->title("Email"),
            Column::make('role.name')->title("Phân quyền"),
            Column::make('created_at')->title("Ngày tạo"),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center')
                ->title("Thao tác")
            ,
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Administrator_' . date('YmdHis');
    }
}
