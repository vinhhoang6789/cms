<?php

namespace App\DataTables;

use App\Models\Slider;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class SliderDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('created_at', function ($record) {
                return $record->created_at->format('d-m-Y');
            })
            ->editColumn('active', function ($record) {
                return $record->active ? '<span class="badge badge-success">Có</span>' : '<span class="badge badge-warning">Không</span>';
            })
            ->editColumn('position', function($record){
                $style = 'style="width: 60px;text-align:center"';
                $attr = 'data-url="'.route('sliders.updateField', $record->id).'"';

                return '<input '.$style.' type="number" name="position" '.$attr.' class="update-field" value="'.$record->position.'"/>';
            })
            ->editColumn('image', function ($record) {
                return '<img width="100" src="'.$record->image.'"/>';
            })
            ->addColumn('action', function ($record) {
                $editLink = route('sliders.edit',$record->id);
                $toggle = 'data-toggle="tooltip" data-placement="top"';

                return '
                <div class="action-buttons">
                        <a '.$toggle.' title="Chỉnh sửa" href="'.$editLink.'" class="blue"><i class="fa fa-pencil ace-icon bigger-130"></i></a>
                        <a '.$toggle.' title="Xóa" class="delete-post red" href="'.route('sliders.destroy', $record->id).'">
                            <i class="fa fa-remove ace-icon bigger-130"></i>
                        </a>
                </div>
                ';
            })
            ->rawColumns(['active','action','status', 'image', 'position']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Slider $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Slider $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('sliderdatatable-table')
                    ->setTableAttribute('class', 'table table-striped table-bordered table-hover dataTable no-footer')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Blfrtip')
                    ->orderBy(6,'desc')
                    ->buttons(
                        Button::make('reload')->text('<i class="fa fa-refresh"></i> Tải lại')->className('btn btn-default'),
                        Button::make('create')->text('<i class="fa fa-plus"></i> Thêm mới')->className('btn btn-success'),
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')->title("ID")->hidden(),
            Column::make('image')->title("Hình ảnh"),
            Column::make('group')->title("Nhóm"),
            Column::make('name')->title("Tiêu đề"),
            Column::make('description')->title("Mô tả"),
            Column::make('position')->title("Vị trí")->width(50),
            Column::make('active')->title("Hoạt động")->width(70),
            Column::make('created_at')->title("Ngày tạo")->width(70),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(70)
                ->addClass('text-center')
                ->title("Thao tác")
            ,
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Slider_' . date('YmdHis');
    }
}
