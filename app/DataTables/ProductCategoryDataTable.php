<?php

namespace App\DataTables;

use App\Models\ProductCategory;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class ProductCategoryDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('created_at', function ($record) {
                return $record->created_at->format('d-m-Y H:i:s');
            })
            ->editColumn('parent', function ($record) {
                if($record->parentCategory){
                    return $record->parentCategory->title;
                }
                return 'Root';
            })
            ->editColumn('active', function ($record) {
                return $record->active ? '<span class="badge badge-success">Hoạt động</span>' : '<span class="badge badge-warning">Ẩn</span>';
            })
            ->editColumn('check', function($record){
                return '<label class="pos-rel">
                            <input type="checkbox" class="ace check-item" value="'.$record->id.'"/>
                            <span class="lbl"></span>
                        </label>';
            })
            ->editColumn('title', function($record){
                return '<a href="'.url($record->permalink->slug).'" target="_blank">'.$record->title.'</a>';
            })
            ->addColumn('action', function ($record) {
                $editLink = route('product-categories.edit',$record->id);
                $toggle = 'data-toggle="tooltip" data-placement="top"';

                return '
                <div class="action-buttons">
                        <a '.$toggle.' title="Chỉnh sửa" href="'.$editLink.'" class="blue"><i class="fa fa-pencil ace-icon bigger-130"></i></a>
                        <a '.$toggle.' title="Xóa" class="delete-post red" href="'.route('product-categories.destroy', $record->id).'">
                            <i class="fa fa-remove ace-icon bigger-130"></i>
                        </a>
                </div>
                ';
            })
            ->rawColumns(['active','action','status','check','title']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ProductCategory $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ProductCategory $model)
    {
        return $model->newQuery()->with('parentCategory');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('productcategorydatatable-table')
            ->setTableAttribute('class', 'table table-striped table-bordered table-hover dataTable no-footer')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Blfrtip')
            ->orderBy(5,'desc')
            ->buttons(
                Button::make()->text('<i class="fa fa-remove" data-url="'.route('product-categories.multipleDestroy').'"></i> Xóa nhiều')
                    ->className('btn btn-danger multiple-delete'),
                Button::make('print')->className('btn btn-info'),
                Button::make('reload')->text('<i class="fa fa-refresh"></i> Tải lại')->className('btn btn-default'),
                Button::make('create')->text('<i class="fa fa-plus"></i> Thêm mới')->className('btn btn-success'),
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('check')
                ->exportable(false)
                ->printable(false)
                ->title('<label class="pos-rel">
                                        <input type="checkbox" class="ace check-all" />
                                        <span class="lbl"></span>
                                    </label>')->width(20),
            Column::make('id')->title("ID")->hidden(),
            Column::make('title')->title("Tiêu đề")->width(300),
            Column::make('parent')->title("Danh mục cha"),
            Column::make('active')->title("Trạng thái"),
            Column::make('created_at')->title("Ngày tạo"),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(70)
                ->addClass('text-center')
                ->title("Thao tác")
            ,
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'ArticleCategory_' . date('YmdHis');
    }
}
