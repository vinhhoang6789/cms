<?php

namespace App\DataTables;

use App\Models\Menu;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class MenuDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('created_at', function ($record) {
                return $record->created_at->format('d-m-Y');
            })
            ->addColumn('action', function ($record) {
                $editLink = route('backend.menu.detail',$record->id);
                $toggle = 'data-toggle="tooltip" data-placement="top"';

                return '
                <div class="action-buttons">
                    <a '.$toggle.' title="Xem chi tiết" href="'.$editLink.'" class="blue"><i class="fa fa-pencil ace-icon bigger-130"></i></a>
                </div>
                ';
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Menu $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Menu $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('menudatatable-table')
                    ->setTableAttribute('class', 'table table-striped table-bordered table-hover dataTable no-footer')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('reload')->text('<i class="fa fa-refresh"></i> Tải lại')->className('btn btn-default'),
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')->title("ID")->hidden(),
            Column::make('name')->title("Tiêu đề"),
            Column::make('created_at')->title("Ngày tạo"),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Menu_' . date('YmdHis');
    }
}
