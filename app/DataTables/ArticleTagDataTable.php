<?php

namespace App\DataTables;

use App\Models\Tag;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class ArticleTagDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('created_at', function ($record) {
                return $record->created_at->format('d-m-Y');
            })
            ->editColumn('check', function($record){
                return '<label class="pos-rel">
                            <input type="checkbox" class="ace check-item" value="'.$record->id.'"/>
                            <span class="lbl"></span>
                        </label>';
            })
            ->editColumn('title', function($record){
                return '<a href="'.route('frontend.tag.detail', $record->slug).'" target="_blank">'.$record->title.'</a>';
            })
            ->addColumn('action', function ($record) {
                $editLink = route('article-tags.edit',$record->id);
                $toggle = 'data-toggle="tooltip" data-placement="top"';

                return '
                <div class="action-buttons">
                        <a '.$toggle.' title="Chỉnh sửa" href="'.$editLink.'" class="blue"><i class="fa fa-pencil ace-icon bigger-130"></i></a>
                        <a '.$toggle.' title="Xóa" class="delete-post red" href="'.route('article-tags.destroy', $record->id).'">
                            <i class="fa fa-remove ace-icon bigger-130"></i>
                        </a>
                </div>
                ';
            })
            ->rawColumns(['action','check', 'title']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Tag $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Tag $model)
    {
        return $model->newQuery()->where('type','article');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableAttribute('class', 'table table-striped table-bordered table-hover dataTable no-footer')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Blfrtip')
                    ->orderBy(3)
                    ->buttons(
                        Button::make()->text('<i class="fa fa-remove" data-url="'.route('article-tags.multipleDestroy').'"></i> Xóa nhiều')
                            ->className('btn btn-danger multiple-delete'),
                        Button::make('print')->className('btn btn-info'),
                        Button::make('reload')->text('<i class="fa fa-refresh"></i> Tải lại')->className('btn btn-default'),
                        Button::make('create')->text('<i class="fa fa-plus"></i> Thêm mới')->className('btn btn-success'),
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('check')
                ->exportable(false)
                ->printable(false)
                ->title('<label class="pos-rel">
                                        <input type="checkbox" class="ace check-all" />
                                        <span class="lbl"></span>
                                    </label>')->width(20),
            Column::make('id')->title("ID")->hidden(),
            Column::make('title')->title("Tiêu đề")->width(300),
            Column::make('created_at')->title("Ngày tạo"),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(70)
                ->addClass('text-center')
                ->title("Thao tác")
            ,
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'ArticleTag_' . date('YmdHis');
    }
}
