<?php

namespace App\DataTables;

use App\Models\Order;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class OrderDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('created_at', function ($record) {
                return $record->created_at->format('d-m-Y H:i:s');
            })
            ->editColumn('status', function ($record) {
                $status = '';

                return '<span class="badge badge-success">'.ucfirst($status).'</span>';
            })
            ->addColumn('action', function ($record) {
                $toggle = 'data-toggle="tooltip" data-placement="top"';
                return '
                    <div class="action-buttons">
                         <a '.$toggle.' title="Chỉnh sửa" href="'.route('orders.edit',$record->id).'" class="blue">
                            <i class="ace-icon fa fa-pencil bigger-130"></i>
                        </a>
                       <a title="Xem hóa đơn" '.$toggle.' class="duplicate-post green" href="'.route('orders.edit', $record->id).'">
                            <i class="ace-icon fa fa-file-text bigger-130"></i>
                        </a>
                    </div>

                ';
            })
            ->rawColumns(['action','status']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\OrderDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Order $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('orderdatatable-table')
            ->setTableAttribute('class', 'table table-striped table-bordered table-hover dataTable no-footer')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Blfrtip')
            ->lengthChange(true)
            ->orderBy(7,'desc')
            ->buttons(
                Button::make('print')->className('btn btn-info'),
                Button::make('reload')->text('<i class="fa fa-refresh"></i> Tải lại')->className('btn btn-default'),
            );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')->title("ID")->hidden(),
            Column::make('invoice')->title("Mã đơn hàng"),
            Column::make('name')->title("Khách hàng"),
            Column::make('email')->title("Email"),
            Column::make('phone')->title("Số điện thoại"),
            Column::make('status')->title("Trạng thái"),
            Column::make('created_at')->title("Ngày đặt hàng"),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center')
                ->title("Thao tác")
            ,
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Order_' . date('YmdHis');
    }
}
