<?php

namespace App\DataTables;

use App\Models\Block;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class BlockDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('created_at', function ($record) {
                return $record->created_at->format('d-m-Y');
            })
            ->editColumn('check', function($record){
                return '<label class="pos-rel">
                            <input type="checkbox" class="ace check-item" value="'.$record->id.'"/>
                            <span class="lbl"></span>
                        </label>';
            })
            ->addColumn('action', function ($record) {
                $toggle = 'data-toggle="tooltip" data-placement="top"';
                return '
                    <div class="action-buttons">
                         <a '.$toggle.' title="Chỉnh sửa" href="'.route('blocks.edit',$record->id).'" class="blue">
                            <i class="ace-icon fa fa-pencil bigger-130"></i>
                        </a>
                       <a title="Nhân bản" '.$toggle.' class="duplicate-post green" href="'.route('blocks.duplicate', $record->id).'">
                            <i class="ace-icon fa fa-file-text bigger-130"></i>
                        </a>
                         <a '.$toggle.' title="Xóa" class="delete-post red" href="'.route('blocks.destroy', $record->id).'">
                            <i class="fa fa-remove ace-icon bigger-130"></i>
                        </a>
                    </div>


                ';
            })
            ->rawColumns(['check', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Block $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Block $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('blockdatatable-table')
            ->setTableAttribute('class', 'table table-striped table-bordered table-hover dataTable no-footer')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Blfrtip')
            ->lengthChange(true)
            ->orderBy(4,'desc')
            ->buttons(
                Button::make()->text('<i class="fa fa-remove" data-url="'.route('blocks.multipleDestroy').'"></i> Xóa nhiều')
                    ->className('btn btn-danger multiple-delete'),
                Button::make('reload')->text('<i class="fa fa-refresh"></i> Tải lại')->className('btn btn-default'),
                Button::make('create')->text('<i class="fa fa-plus"></i> Thêm mới')->className('btn btn-success')
            );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('check')
                ->exportable(false)
                ->printable(false)
                ->title('<label class="pos-rel">
                                        <input type="checkbox" class="ace check-all" />
                                        <span class="lbl"></span>
                                    </label>')->width(20),
            Column::make('id')->title("ID")->hidden(),
            Column::make('title')->title("Tiêu đề")->width(700),
            Column::make('group')->title("Nhóm"),
            Column::make('url')->title("Liên kết"),
            Column::make('created_at')->title("Ngày tạo")->width(100),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(100)
                ->addClass('text-center')
                ->title("Thao tác")
            ,
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Block_' . date('YmdHis');
    }
}
