<?php

namespace App\DataTables;

use Spatie\Permission\Models\Role;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class RoleDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('created_at', function ($record) {
                return $record->created_at->format('d-m-Y');
            })
            ->addColumn('action', function ($record) {
                $toggle = 'data-toggle="tooltip" data-placement="top"';
                return '
                    <div class="action-buttons">
                         <a '.$toggle.' title="Chỉnh sửa" href="'.route('roles.edit',$record->id).'" class="blue">
                            <i class="ace-icon fa fa-pencil bigger-130"></i>
                        </a>
                        <a '.$toggle.' title="Xóa" class="delete-post red" href="'.route('roles.destroy', $record->id).'">
                            <i class="ace-icon fa fa-remove bigger-130"></i>
                        </a>
                    </div>
                ';
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param Role $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Role $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('roledatatable-table')
                    ->setTableAttribute('class', 'table table-striped table-bordered table-hover dataTable no-footer')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Blfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('print')->className('btn btn-info'),
                        Button::make('reload')->text('<i class="fa fa-refresh"></i> Tải lại')->className('btn btn-default'),
                        Button::make('create')->text('<i class="fa fa-plus"></i> Thêm mới')->className('btn btn-success'),
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')->title("ID")->hidden(),
            Column::make('name')->title('Quyền'),
            Column::make('created_at')->title("Ngày tạo"),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center')
                ->title('Thao tác'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Role_' . date('YmdHis');
    }
}
