<?php

namespace App\Console\Commands;

use App\Models\Article;
use App\Models\ArticleCategory;
use App\Models\Comment;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductImage;
use App\Models\Tag;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class GenerateTestingData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:testing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate testing data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
            DB::beginTransaction();
            DB::table('tags')->truncate();
            DB::table('articles')->truncate();
            DB::table('article_categories')->truncate();
            DB::table('products')->truncate();
            DB::table('product_categories')->truncate();

            factory(ArticleCategory::class, 100)->create()->each(function($tag){
                $tag->permalink()->create([
                    'type' => ArticleCategory::PERMALINK_TYPE,
                    'action' => ArticleCategory::PERMALINK_ACTION,
                ]);
                $tag->seo()->create();
            });

            factory(ProductCategory::class, 100)->create()->each(function($tag){
                $tag->permalink()->create([
                    'type' => ProductCategory::PERMALINK_TYPE,
                    'action' => ProductCategory::PERMALINK_ACTION,
                ]);
                $tag->seo()->create();
            });

            factory(Tag::class, 100)->create();

            factory(Article::class, 1000)->create()->each(function($tag){
                $tag->permalink()->create([
                    'type' => Article::PERMALINK_TYPE,
                    'action' => Article::PERMALINK_ACTION,
                ]);
                $tag->seo()->create();

            });

            factory(Product::class, 1000)->create()->each(function($tag){
                $tag->permalink()->create([
                    'type' => Product::PERMALINK_TYPE,
                    'action' => Product::PERMALINK_ACTION,
                ]);
                $tag->seo()->create();
            });

            factory(Comment::class, 500)->create();

            DB::commit();
        }catch (\Exception $e){
            echo $e->getMessage();
            DB::rollBack();
        }
    }
}
