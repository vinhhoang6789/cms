<?php
//Placeholder src in ratio
// 1:1, 2:3, 16:9
function placeholderSrc($width, $height){
    return "data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 $width $height'%3E%3C/svg%3E";
}

function photoThumb($src){
    $photo = 'frontend/img/placeholder-image.jpg';

    if($src){
        $photo = str_replace('shares/', 'shares/thumbs/', $src);
    }

    return url($photo);
}

function fullSubstr($body, $length){
    $threeDot = '';

    if(strlen($body) > $length){
        $threeDot = '...';
    }

    return substr($body, 0, $length).$threeDot;
}

function htmlBackendMenu($parent, $children){
    $html = '<li class="draggable-menu-item"
        data-id="'.$parent->id.'">
        <div class="title-menu">
            '.$parent->title.'
            <span class="action pull-right">
                <a href="'.route('backend.menuItem.detail', $parent->id).'"><i class="fa fa-pencil green"></i></a>
                <i class="fa fa-remove delete-menu red" data-id="'.$parent->id.'"></i>
            </span>
        </div>
        <ol>'.recursiveChildren($parent->id, $children).'</ol>
    </li>';

    return $html;
}

function generateBackendMenu($data){
    $html = '';
    if(!empty($data[0])){
        $parents = $data[0];
        $children = $data;
        unset($children[0]);

        foreach ($parents as $parent){
            $html .= htmlBackendMenu($parent, $children);
        }
    }
    return $html;
}

function recursiveChildren($parent, $children){
    $html = '';

    if(!empty($children[$parent])){
        foreach ($children[$parent] as $parent){
            $html .= htmlBackendMenu($parent, $children);
        }
    }

    return $html;
}

function generateCategoryTree($data, $default){
    $html = '';
    if(!empty($data[0])){
        $parents = $data[0];
        $children = $data;
        unset($children[0]);

        foreach ($parents as $parent){
            $html .= htmlCategoryTree($parent, $children, $default);
        }
    }
    return $html;
}

function htmlCategoryTree($parent, $children, $default, $space = ''){
    $html = '<option '.($default == $parent->id ? "selected" : null).' value="'.$parent->id.'">'.$space.' '.$parent->title.'</option>';
    $html .= recursiveChildrenCategory($parent->id, $children, $default, $space);
    return $html;
}

function recursiveChildrenCategory($parent, $children, $default, $space){
    $html = '';

    if(!empty($children[$parent])){
        $space .= '|-------';

        foreach ($children[$parent] as $parent){
            $html .= htmlCategoryTree($parent, $children, $default, $space);
        }
    }

    return $html;
}
