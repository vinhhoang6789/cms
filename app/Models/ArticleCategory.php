<?php

namespace App\Models;

use App\Traits\ClearsResponseCache;
use Illuminate\Database\Eloquent\Model;

class ArticleCategory extends Model
{
    const PERMALINK_TYPE = 'article_category';
    const PERMALINK_ACTION = 'ArticleCategoryController@detail';

    use ClearsResponseCache;

    public function permalink()
    {
        return $this->morphOne(Permalink::class,'permalinkable')->withDefault();
    }

    public function articles()
    {
        return $this->belongsToMany(Article::class);
    }

    public function parentCategory()
    {
        return $this->belongsTo(ArticleCategory::class,'parent');
    }

    public function seo()
    {
        return $this->morphOne(SeoConfiguration::class,'seo_configurable')->withDefault();
    }

    public function url(){
        return asset($this->permalink->slug);
    }
}
