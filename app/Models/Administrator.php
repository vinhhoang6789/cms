<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;


class Administrator extends Authenticatable
{
    use HasRoles;

    protected $guarded = ['username'];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function manageableList()
    {
        return [
            'article|Bài viết',
            'article_category|Danh mục bài viết',
            'article_tag|Tag bài viết',
            'product|Sản phẩm',
            'product_category|Danh mục sản phẩm',
            'order|Đơn hàng',
            'comment|Bình luận',
            'page|Trang tĩnh',
            'slide|Slide',
            'image|Hình ảnh',
        ];
    }

    public function articles()
    {
        return $this->hasMany(Article::class);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

}
