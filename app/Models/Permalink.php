<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;

class Permalink extends Model
{
    protected $fillable = ['type', 'slug', 'action'];

    use Sluggable;

    public function permalinkable()
    {
        return $this->morphTo();
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'permalinkable.title'
            ]
        ];
    }
}
