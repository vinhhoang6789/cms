<?php

namespace App\Models;

use App\Traits\ClearsResponseCache;
use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model
{
    use ClearsResponseCache;

    public function menu()
    {
        $this->belongsTo(Menu::class);
    }
}
