<?php

namespace App\Models;

use App\Traits\ClearsResponseCache;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use Sluggable;
    use SluggableScopeHelpers;
    use ClearsResponseCache;

    public function articles()
    {
        return $this->morphedByMany(Article::class, 'taggable');
    }

    public function seo()
    {
        return $this->morphOne(SeoConfiguration::class,'seo_configurable')->withDefault();
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
