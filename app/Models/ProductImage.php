<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $fillable = ['url', 'is_featured'];

    public function product()
    {
        $this->belongsTo(Product::class);
    }
}
