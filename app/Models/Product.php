<?php

namespace App\Models;

use App\Traits\ClearsResponseCache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    const PERMALINK_TYPE = 'product';
    const PERMALINK_ACTION  = 'ProductController@detail';

    use SoftDeletes;
    use ClearsResponseCache;

    public function permalink()
    {
        return $this->morphOne(Permalink::class, 'permalinkable');
    }

    public function category()
    {
        return $this->belongsTo(ProductCategory::class, 'product_category_id');
    }

    public function seo()
    {
        return $this->morphOne(SeoConfiguration::class,'seo_configurable')->withDefault();
    }

    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }

    public function url(){
        return asset($this->permalink->slug);
    }
}
