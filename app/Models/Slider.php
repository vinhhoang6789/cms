<?php

namespace App\Models;

use App\Traits\ClearsResponseCache;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    use ClearsResponseCache;
}
