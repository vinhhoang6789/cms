<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    protected $fillable = ['url'];

    public function attachable()
    {
        return $this->morphTo();
    }
}
