<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SeoConfiguration extends Model
{
    protected $fillable = ['title', 'index','follow','canonical','description', 'scheme'];

    public function seoConfigurable()
    {
        return $this->morphTo();
    }
}
