<?php

namespace App\Models;

use App\Traits\ClearsResponseCache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    const PERMALINK_TYPE = 'article';

    const PERMALINK_ACTION = 'ArticleController@detail';

    use SoftDeletes;

    use ClearsResponseCache;

    public function category()
    {
        return $this->belongsTo(ArticleCategory::class,'article_category_id');
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class,'taggable');
    }

    public function seo()
    {
        return $this->morphOne(SeoConfiguration::class,'seo_configurable')->withDefault();
    }

    public function comments()
    {
        return $this->morphMany(Comment::class,'commentable');
    }

    public function author()
    {
        return $this->belongsTo(Administrator::class,'administrator_id');
    }

    public function permalink()
    {
        return $this->morphOne(Permalink::class, 'permalinkable')->withDefault();
    }

    public function url(){
        return asset($this->permalink->slug);
    }
}
