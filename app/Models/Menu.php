<?php

namespace App\Models;

use App\Traits\ClearsResponseCache;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use ClearsResponseCache;

    public function menuItems()
    {
        return $this->hasMany(MenuItem::class);
    }
}
