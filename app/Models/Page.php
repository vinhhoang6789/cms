<?php

namespace App\Models;

use App\Traits\ClearsResponseCache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model
{
    const PERMALINK_TYPE = 'page';
    const PERMALINK_ACTION  = 'PageController@detail';

    use SoftDeletes;
    use ClearsResponseCache;

    public function permalink()
    {
        return $this->morphOne(Permalink::class, 'permalinkable');
    }

    public function seo()
    {
        return $this->morphOne(SeoConfiguration::class,'seo_configurable')->withDefault();
    }

    public function url(){
        return asset($this->permalink->slug);
    }
}
