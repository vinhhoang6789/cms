<?php

namespace App\Models;

use App\Traits\ClearsResponseCache;
use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    const PERMALINK_TYPE = 'product_category';
    const PERMALINK_ACTION  = 'ProductCategoryController@detail';

    use ClearsResponseCache;

    public function permalink()
    {
        return $this->morphOne(Permalink::class, 'permalinkable');
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function parentCategory()
    {
        return $this->belongsTo(ProductCategory::class,'parent');
    }

    public function seo()
    {
        return $this->morphOne(SeoConfiguration::class,'seo_configurable')->withDefault();
    }

    public function url(){
        return asset($this->permalink->slug);
    }
}
