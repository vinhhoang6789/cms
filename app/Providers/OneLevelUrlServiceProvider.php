<?php

namespace App\Providers;

use App\Models\Permalink;
use Illuminate\Support\ServiceProvider;

class OneLevelUrlServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     */
    public function boot()
    {
        //Handle permalink
        $router = app()->make('router');

        $router->get('{slug}', function ($slug){
           $permalink = Permalink::with('permalinkable')
               ->where('slug', $slug)
               ->firstOrFail();

           return $this->app->call(
               'App\Http\Controllers\\'.$permalink->action,
               [$permalink->permalinkable()]
           );

        })->middleware(['web', 'cacheResponse']);
    }
}
