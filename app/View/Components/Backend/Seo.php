<?php

namespace App\View\Components\Backend;

use Illuminate\View\Component;

class Seo extends Component
{
    public $formData;

    public $isEdit;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($formData, $isEdit = false)
    {
        if (!$formData) {
            $formData = [
                "title" => '',
                "canonical" => '',
                "description" => '',
                "scheme" => '',
                "index" => '',
                "follow" => '',
            ];
        }
        if(!$isEdit){
            $formData = (object) $formData;
        }

        $this->formData = $formData;
        $this->isEdit = $isEdit;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.backend.seo');
    }
}
