<?php

namespace App\View\Components\Backend;

use Illuminate\View\Component;

class MultipleImage extends Component
{
    public $images;
    public $name;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($images = [], $name = 'images')
    {
        if(!empty($images[0]) && !is_object($images[0])){
            $images = array_map(function($img){
                return (object) [
                    'url' => $img
                ];
            }, $images);

        }

        $this->images = $images;
        $this->name = $name;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.backend.multiple-image');
    }
}
