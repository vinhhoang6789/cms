<?php

namespace App\View\Components\Backend;

use Illuminate\View\Component;

class SingleImage extends Component
{
    public $currentImage;

    public $name = 'featured_image';

    public $imageEl = 'holder_image';

    public $removeEl = 'remove_holder';

    public $fileEl = 'file_holder_image';

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($currentImage, $name = '', $imageEl = '', $removeEl = '', $fileEl = '')
    {
        $this->currentImage = $currentImage;

        if($name){
            $this->name = $name;
        }

        if($imageEl){
            $this->imageEl = $imageEl;
        }

        if($removeEl){
            $this->removeEl = $removeEl;
        }

        if($fileEl){
            $this->fileEl = $fileEl;
        }
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.backend.single-image');
    }
}
