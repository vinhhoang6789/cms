<?php

return [
    'facebook' => '<li><a href=":url" class="social-button :class" id=":id" title=":title"><ion-icon name="logo-facebook"></ion-icon></a></li>',
    'twitter' => '<li><a href=":url" class="social-button :class" id=":id" title=":title"><ion-icon name="logo-twitter"></ion-icon></a></li>',
    'linkedin' => '<li><a href=":url" class="social-button :class" id=":id" title=":title"><ion-icon name="logo-linkedin"></ion-icon></a></li>',
    'whatsapp' => '<li><a target="_blank" href=":url" class="social-button :class" id=":id" title=":title"><ion-icon name="logo-facebook"></ion-icon></a></li>',
    'pinterest' => '<li><a href=":url" class="social-button :class" id=":id" title=":title"><ion-icon name="logo-facebook"></ion-icon></a></li>',
    'reddit' => '<li><a target="_blank" href=":url" class="social-button :class" id=":id" title=":title"><ion-icon name="logo-facebook"></ion-icon></a></li>',
    'telegram' => '<li><a target="_blank" href=":url" class="social-button :class" id=":id" title=":title"><ion-icon name="logo-facebook"></ion-icon></a></li>',
];
