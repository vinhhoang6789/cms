<footer>
    <div class="bg-primary top">
        <div class="container">
            <div class="row">
                <div class="col-md-6 mb-3 mb-lg-0">
                    <div class="d-flex align-items-center justify-content-center">
                        <img alt="Tư vấn thiết kế" src="{{asset('frontend/img/customer-service.png')}}"/>
                        <div class="ml-2">
                            <p class="m-0">Tư vấn thết kế website</p>
                            <p class="m-0"><strong>0986229993</strong></p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 mb-3 mb-lg-0">
                    <div class="d-flex align-items-center justify-content-center">
                        <img alt="Gửi yêu cầu tới hadava" src="{{asset('frontend/img/mail.png')}}"/>
                        <div class="ml-2">
                            <p class="m-0">Gửi yêu cầu tới hadava</p>
                            <p class="m-0"><strong>hdvinhhoang@gmail.com</strong></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 mb-3 col-lg-4">
                <a class="logo" href="{{config('app.url')}}">HADAVA.</a>
                <h3>CÔNG TY CP GIẢI PHÁP & PHÁT TRIỂN
                    PHẦN MỀM HADAVA</h3>

                <p>136 Kim Ngưu, Hai Bà Trưng, Hà Nội<br>
                    Tel: 0986229993<br>
                    hdvinhhoang@gmail.com</p>

                <div class="d-flex social">
                    <a href="#" title="Facebook"><ion-icon name="logo-facebook"></ion-icon></a>
                    <a href="#" title="Twitter"><ion-icon name="logo-twitter"></ion-icon></a>
                </div>
            </div>
            <div class="col-lg-2 mb-3 col-md-6">
                <h4>Danh mục</h4>
                <ul>
                    <li><a href="/tin-tuc" title="Tin tức">Tin tức</a></li>
                    <li><a href="/tuyen-dung" title="Tuyển dụng">Tuyển dụng</a></li>
                    <li><a href="/ho-tro" title="Hỗ trợ">Hỗ trợ</a></li>
                </ul>
            </div>

            <div class="col-lg-2 mb-3 col-md-6">
                <h4>Dịch vụ</h4>
                <ul>
                    <li><a href="/thiet-ke-web" title="Thiết kế web">Thiết kế website</a></li>
                    <li><a href="/ung-dung-web" title="Ứng dụng web">Ứng dụng website</a></li>
                    <li><a href="/ung-dung-di-dong" title="Ứng dụng di động">Ứng dụng di động</a></li>
                </ul>
            </div>

            <div class="col-md-6 mb-3 col-lg-4">
                <h4>Đăng ký nhận bản tin</h4>
                <form class="form-inline" method="post" action="{{route('frontend.subscribe')}}" id="subscribe-form">
                    <input class="form-control" type="text" name="email" placeholder="Nhập email của bạn"/>
                    <button type="submit" class="btn btn-primary">Đăng ký</button>
                </form>
                <p class="note mt-2">Bạn sẽ nhận được những cập nhật mới nhất của chúng tôi.</p>
            </div>

        </div>
    </div>
    <div class="bottom">
        <div class="container">
            <span><strong>© 2019 - 2020 Hadava </strong> - Thiết kế website | Giải pháp ứng dụng web | Ứng dụng di động</span>
        </div>
    </div>
</footer>
<script src="{{ asset('frontend/js/jquery-3.4.1.min.js') }}" ></script>
<script src="{{ asset('frontend/js/bootstrap.min.js') }}" ></script>
<script src="{{ asset('frontend/js/sweetalert2.min.js') }}" ></script>
<script src="{{ asset('frontend/js/sticky.min.js') }}" ></script>
<script src="{{ asset('frontend/slick/slick.min.js') }}" ></script>
<script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>
<script>
    var loader = $('.loader-container');
</script>
<script src="{{ asset('frontend/js/default.js') }}" ></script>
@yield('script')
</body>
</html>
