<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset("frontend/img/favicon-32x32.png")}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset("frontend/img/favicon-96x96.png")}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset("frontend/img/favicon-16x16.png")}}">
    {!! SEO::generate() !!}
    <style>
        {!! file_get_contents(public_path('frontend/css/bootstrap.min.css')) !!}
    </style>
    <link href="{{asset('frontend/css/dropzone.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('frontend/css/sweetalert2.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('frontend/css/animate.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('frontend/slick/slick.css')}}" rel="stylesheet"/>
    <link href="{{ asset('frontend/css/reset.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/responsive.css') }}" rel="stylesheet">
    <!-- Styles -->
</head>
<body class="{{$pageClass ?? ''}}">

<div class="loader-container">
    <div class="loader-flex">
        <div class="loader">Loading...</div>
    </div>
</div>

<div class="header-menu-box">
    <nav class="navbar navbar-expand-lg container">
        <div class="justify-content-between align-items-center nav-head">
            <a class="navbar-brand logo" href="/">HADAVA.</a>
            <span class="company-name">Công ty thiết kế website HADAVA</span>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#primary-menu" aria-controls="primary-menu" aria-expanded="false" aria-label="Toggle navigation">
                <ion-icon name="menu-outline"></ion-icon>
            </button>
        </div>
        <x-frontend.menu :menu="$menu"/>
    </nav>
</div>

