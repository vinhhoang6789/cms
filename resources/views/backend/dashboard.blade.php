@extends('.backend.layout.main')
@section('title', 'Bảng điều khiển')
@section('content')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="#">Home</a>
            </li>
            <li class="active">Bảng điều khiển</li>
        </ul><!-- /.breadcrumb -->
    </div>

    <div class="page-content">

        <div class="page-header">
            <h1>
                Bảng điều khiển
                <small>
                    <i class="ace-icon fa fa-angle-double-right"></i>
                    Tổng quan, thống kê hệ thống.
                </small>
            </h1>
        </div><!-- /.page-header -->

        <div class="row">

            <div class="col-md-6">
                <div class="widget-box transparent" id="recent-box">
                    <div class="widget-header">
                        <h4 class="widget-title lighter smaller">
                            <i class="ace-icon fa fa-rss orange"></i>Gần đây
                        </h4>

                        <div class="widget-toolbar no-border">
                            <ul class="nav nav-tabs" id="recent-tab">
                                <li class="active">
                                    <a data-toggle="tab" href="#article-tab" aria-expanded="true">Bài viết</a>
                                </li>

                                <li class="">
                                    <a data-toggle="tab" href="#product-tab" aria-expanded="false">Sản phẩm</a>
                                </li>

                                <li class="">
                                    <a data-toggle="tab" href="#comment-tab" aria-expanded="false">Bình Luận</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="widget-body">
                        <div class="widget-main padding-4">
                            <div class="tab-content padding-8">
                                <div id="article-tab" class="tab-pane active">
                                    <h4 class="smaller lighter green">
                                        <i class="ace-icon fa fa-list"></i>
                                        Bài viết gần đây
                                    </h4>
                                    @if(!$articles->isEmpty())
                                        @php
                                            $colors = ['orange', 'red','default','blue','grey','green','pink'];
                                        @endphp
                                        <ul id="tasks" class="item-list">
                                            @foreach($articles as $article)
                                                @php
                                                    $randColor = $colors[rand(0, count($colors) - 1)];
                                                @endphp
                                                <li class="item-{{$randColor}} clearfix ui-sortable-handle">
                                                    <a target="_blank"
                                                       href="{{url($article->permalink->slug)}}">{{$article->title}}</a>
                                                </li>
                                            @endforeach
                                        </ul>

                                        <div class="center" style="margin-top:10px">
                                            <i class="ace-icon fa fa-book fa-2x green middle"></i>

                                            <a href="{{route('articles.index')}}" class="btn btn-sm btn-white btn-info">
                                                Xem tất cả bài viết &nbsp;
                                                <i class="ace-icon fa fa-arrow-right"></i>
                                            </a>
                                        </div>

                                        <div class="hr hr-double hr8"></div>
                                    @else
                                        <p class="center">Không có bài viết nào gần đây.</p>
                                    @endif
                                </div>

                                <div id="product-tab" class="tab-pane">
                                    <h4 class="smaller lighter green">
                                        <i class="ace-icon fa fa-list"></i>
                                        Sản phẩm gần đây
                                    </h4>
                                    @if(!$products->isEmpty())
                                        <ul id="products" class="item-list">
                                            @foreach($products as $product)
                                                @php
                                                    $randColor = $colors[rand(0, count($colors) - 1)];
                                                @endphp
                                                <li class="item-{{$randColor}} clearfix ui-sortable-handle">
                                                    <a target="_blank"
                                                       href="{{url($product->permalink->slug)}}">{{$product->title}}</a>
                                                </li>
                                            @endforeach
                                        </ul>

                                        <div class="center" style="margin-top:10px">
                                            <i class="ace-icon fa fa-glass fa-2x green middle"></i>

                                            &nbsp;
                                            <a href="{{route('products.index')}}" class="btn btn-sm btn-white btn-info">
                                                Xem tất cả sản phẩm &nbsp;
                                                <i class="ace-icon fa fa-arrow-right"></i>
                                            </a>
                                        </div>

                                        <div class="hr hr-double hr8"></div>
                                    @else
                                        <p class="center">Không có sản phẩm nào gần đây</p>
                                    @endif

                                </div><!-- /.#member-tab -->

                                <div id="comment-tab" class="tab-pane">
                                    <h4 class="smaller lighter green">
                                        <i class="ace-icon fa fa-list"></i>
                                        Bình luận mới nhất
                                    </h4>
                                    @if(!$comments->isEmpty())
                                        <div class="comments">
                                            @foreach($comments as $comment)
                                                <div class="itemdiv commentdiv">
                                                    <div class="user">
                                                        <img src="{{asset('backend/img/default-avatar.png')}}">
                                                    </div>

                                                    <div class="body">
                                                        <div class="name">
                                                            <a href="#">{{$comment->name}} ({{$comment->email}})</a>
                                                        </div>

                                                        <div class="time">
                                                            <i class="ace-icon fa fa-clock-o"></i>
                                                            <span class="green">{{$comment->created_at->diffForHumans()}}</span>
                                                        </div>

                                                        <div class="text">
                                                            <i class="ace-icon fa fa-quote-left"></i>
                                                            {{substr($comment->content,0,150)}} …
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>


                                        <div class="hr hr8"></div>

                                        <div class="center">
                                            <i class="ace-icon fa fa-comments-o fa-2x green middle"></i>

                                            <a href="{{route('comments.index')}}" class="btn btn-sm btn-white btn-info">
                                                Xem tất cả bình luận &nbsp;
                                                <i class="ace-icon fa fa-arrow-right"></i>
                                            </a>
                                        </div>

                                        <div class="hr hr-double hr8"></div>
                                    @else
                                        <p class="center">Không có bình luận nào gần đây.</p>
                                    @endif
                                </div>
                            </div>
                        </div><!-- /.widget-main -->
                    </div><!-- /.widget-body -->
                </div>
            </div>
            <div class="col-md-6">
                <div class="widget-box transparent">
                    <div class="widget-header">
                        <h4 class="widget-title lighter smaller">
                            <i class="ace-icon fa fa-book info"></i>Hướng dẫn quản trị hệ thống
                        </h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <p>Xem file hướng dẫn tại <a href="#">đây.</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.row -->
    </div><!-- /.page-content -->
@endsection
