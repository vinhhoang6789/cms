@include('.backend.layout.header')
<div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
        try{ace.settings.loadState('main-container')}catch(e){}
    </script>
    <div id="sidebar" class="sidebar responsive ace-save-state">
        <script type="text/javascript">
            try{ace.settings.loadState('sidebar')}catch(e){}
        </script>
        @php
            $currentRouteName = \Request::route()->getName();
            $articleRoutes = [
                'articles.index',
                'articles.trash',
                'articles.create',
                'articles.edit',
            ];

            $articleCatRoutes = [
                'article-categories.index',
                'article-categories.create',
                'article-categories.edit',
            ];

            $articleTagRoutes = [
                'article-tags.index',
                'article-tags.create',
                'article-tags.edit'
            ];

            $articleMenuGroup = array_merge($articleRoutes, $articleCatRoutes, $articleTagRoutes);

            $productRoutes = [
                'products.index',
                'products.create',
                'products.edit',
                'products.trash',
            ];

            $productCatRoutes = [
                'product-categories.index',
                'product-categories.create',
                'product-categories.edit',
            ];

            $adminRoutes = ['administrators.index', 'administrators.create','administrators.edit'];
            $roleRoutes = ['roles.index', 'roles.create','roles.edit'];
            $adminGroupMenu = array_merge($adminRoutes, $roleRoutes);

            $productMenuGroup = array_merge($productRoutes, $productCatRoutes);
        @endphp

        <ul class="nav nav-list">

            <li class="{{$currentRouteName == 'backend.dashboard' ? 'active' : null}}">
                <a href="{{route('backend.dashboard')}}">
                    <i class="menu-icon fa fa-tachometer"></i>
                    <span class="menu-text"> Bảng điều khiển </span>
                </a>
                <b class="arrow"></b>
            </li>

            @can('edit-setting')
            <li class="{{in_array($currentRouteName, $adminGroupMenu) ? 'active open' : null}}">
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-user"></i>
                    <span class="menu-text"> Quản trị viên</span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <ul class="submenu">
                    <li class="{{in_array($currentRouteName, $adminRoutes) ? 'active' : null}}">
                        <a href="{{route('administrators.index')}}">
                            <span class="sidebar-normal">Danh sách quản trị viên</span>
                        </a>
                        <b class="arrow"></b>
                    </li>
                    <li class="{{in_array($currentRouteName, $roleRoutes) ? 'active' : null}}">
                        <a href="{{route('roles.index')}}">
                            <span class="sidebar-normal">Phân quyền</span>
                        </a>
                        <b class="arrow"></b>
                    </li>
                </ul>
            </li>
            @endcan

            @if(auth()->user()->can('visible article') || auth()->user()->can('visible article_category') || auth()->user()->can('visible article_tag'))
            <li class="{{in_array($currentRouteName, $articleMenuGroup) ? 'active open' : null}} ">
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-list-alt"></i>
                    <span class="menu-text">Bài viết </span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <ul class="submenu">
                    @can('visible article')
                    <li class="{{in_array($currentRouteName, $articleRoutes) ? 'active' : null}}">
                        <a href="{{route('articles.index')}}">
                            <span class="sidebar-normal">Danh sách bài viết</span>
                        </a>
                        <b class="arrow"></b>
                    </li>
                    @endcan
                    @can('visible article_category')
                    <li class="{{in_array($currentRouteName, $articleCatRoutes) ? 'active' : null}}">
                        <a href="{{route('article-categories.index')}}">
                            <span class="sidebar-normal">Danh mục</span>
                        </a>
                        <b class="arrow"></b>
                    </li>
                    @endcan
                    @can('visible article_tag')
                    <li class="{{in_array($currentRouteName, $articleTagRoutes) ? 'active' : null}}">
                        <a href="{{route('article-tags.index')}}">
                            <span class="sidebar-normal">Tags</span>
                        </a>
                        <b class="arrow"></b>
                    </li>
                    @endcan
                </ul>
            </li>
            @endif

            @if(auth()->user()->can('visible product') || auth()->user()->can('visible product_category'))
            <li class="{{in_array($currentRouteName, $productMenuGroup) ? 'active open' : null}}">
                <a class="dropdown-toggle" href="#">
                    <i class="menu-icon fa fa-glass"></i>
                    <span class="menu-text">Sản phẩm </span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <ul class="submenu">
                    @can('visible product')
                    <li class="{{in_array($currentRouteName, $productRoutes) ? 'active' : null}}">
                        <a href="{{route('products.index')}}">
                            <span class="sidebar-normal">Danh sách sản phẩm</span>
                        </a>
                        <b class="arrow"></b>
                    </li>
                    @endcan
                    @can('visible product_category')
                    <li class="{{in_array($currentRouteName, $productCatRoutes) ? 'active' : null}}">
                        <a href="{{route('product-categories.index')}}">
                            <span class="sidebar-normal">Danh mục</span>
                        </a>
                        <b class="arrow"></b>
                    </li>
                    @endcan
                </ul>
            </li>
            @endif

            @can('visible order')
            <li class="{{in_array($currentRouteName, ['orders.index', 'orders.update'])  ? 'active' : null}}">
                <a href="{{route('orders.index')}}">
                    <i class="menu-icon fa fa-shopping-cart"></i>
                    <span class="menu-text"> Đơn hàng </span>
                </a>
                <b class="arrow"></b>
            </li>
            @endcan

            @can('visible comment')
                <li class="{{in_array($currentRouteName, ['comments.index', 'comments.show'])  ? 'active' : null}}">
                    <a href="{{route('comments.index')}}">
                        <i class="menu-icon fa fa-comments-o"></i>
                        <span class="menu-text"> Bình luận </span>
                    </a>
                    <b class="arrow"></b>
                </li>
            @endcan

            @can('edit-setting')
                <li class="{{in_array($currentRouteName, ['contacts.index', 'contacts.show'])  ? 'active' : null}}">
                    <a href="{{route('contacts.index')}}">
                        <i class="menu-icon fa fa-send-o"></i>
                        <span class="menu-text"> Liên hệ </span>
                    </a>
                    <b class="arrow"></b>
                </li>
            @endcan

            @can('visible page')
            <li class="{{in_array($currentRouteName, ['pages.index', 'pages.create', 'pages.edit', 'pages.trash']) ? 'active' : null}}">
                <a href="{{route('pages.index')}}">
                    <i class="menu-icon fa fa-copy"></i>
                    <span class="menu-text"> Trang tĩnh </span>
                </a>
                <b class="arrow"></b>
            </li>
            @endcan

            @can('edit-setting')
                <li class="{{in_array($currentRouteName, ['blocks.index', 'blocks.create', 'blocks.edit']) ? 'active' : null}}">
                    <a href="{{route('blocks.index')}}">
                        <i class="menu-icon fa fa-th"></i>
                        <span class="menu-text">Quản lý Blocks</span>
                    </a>
                    <b class="arrow"></b>
                </li>
            @endcan

            @can('visible slide')
            <li class="{{in_array($currentRouteName, ['sliders.index', 'sliders.create', 'sliders.edit']) ? 'active' : null}}">
                <a href="{{route('sliders.index')}}">
                    <i class="menu-icon fa fa-camera"></i>
                    <span class="menu-text"> Sliders </span>
                </a>
                <b class="arrow"></b>
            </li>
            @endcan

            @can('edit-setting')
            <li class="{{in_array($currentRouteName, ['backend.menu', 'backend.menu.detail', 'backend.menuItem.detail']) ? 'active' : null}}">
                <a href="{{route('backend.menu')}}">
                    <i class="menu-icon fa fa-bars"></i>
                    <span class="menu-text"> Menu</span>
                </a>
                <b class="arrow"></b>
            </li>
            @endcan

            @can('visible image')
            <li class="{{$currentRouteName == 'backend.media' ? 'active' : null}}">
                <a href="{{route('backend.media')}}">
                    <i class="menu-icon fa fa-photo"></i>
                    <span class="menu-text"> Hình ảnh </span>
                </a>
                <b class="arrow"></b>
            </li>
            @endcan

            @can('edit-setting')
            <li class="{{in_array($currentRouteName, ['backend.setting.general', 'backend.setting.cache']) ? 'active open' : null}}">
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-cogs"></i>
                    <span class="menu-text">Cài đặt </span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>

                <ul class="submenu">
                    <li class="{{$currentRouteName == 'backend.setting.general' ? 'active' : null}}">
                        <a href="{{route('backend.setting.general')}}">
                            <span class="sidebar-normal">Cấu hình chung</span>
                        </a>
                        <b class="arrow"></b>
                    </li>

                    <li class="{{$currentRouteName == 'backend.setting.cache' ? 'active' : null}}">
                        <a href="{{route('backend.setting.cache')}}">
                            <span class="sidebar-normal">Cache</span>
                        </a>
                        <b class="arrow"></b>
                    </li>
                </ul>

            </li>
            @endcan
        </ul><!-- /.nav-list -->

        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
            <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state"
               data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>
    </div>

    <div class="main-content">
        <div class="main-content-inner">
            @yield('content')
        </div>
    </div>

@include('.backend.layout.footer')
