<!doctype html>
<html lang="en">
<head>
    <title>Hệ thống quản trị nội dung website</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport"/>
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset("frontend/img/favicon-32x32.png")}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset("frontend/img/favicon-96x96.png")}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset("frontend/img/favicon-16x16.png")}}">

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="csrf-token" content="{{csrf_token()}}"/>

    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/bs4/dt-1.10.20/b-1.6.1/datatables.min.css"/>

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="{{url('backend/assets/css/bootstrap.min.css')}}"/>
    <link rel="stylesheet" href="{{url('backend/assets/font-awesome/4.5.0/css/font-awesome.min.css')}}"/>

    <!-- text fonts -->
    <link rel="stylesheet" href="{{url('backend/assets/css/fonts.googleapis.com.css')}}"/>

    <link rel="stylesheet" href="{{url('backend/assets/css/chosen.min.css')}}" />

    <!-- ace styles -->
    <link rel="stylesheet" href="{{url('backend/assets/css/ace.min.css')}}"/>

<!--[if lte IE 9]>
    <link rel="stylesheet" href="{{url('backend/assets/css/ace-part2.min.css')}}"/>
    <![endif]-->
    <link rel="stylesheet" href="{{url('backend/assets/css/ace-rtl.min.css')}}"/>

<!--[if lte IE 9]>
    <link rel="stylesheet" href="{{url('backend/assets/css/ace-ie.min.css')}}"/>
    <![endif]-->

    <!-- ace settings handler -->
    <script src="{{url('backend/assets/js/ace-extra.min.js')}}"></script>

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
    <link rel="stylesheet" href="{{url('backend/assets/css/custom.css')}}"/>
<!--[if lte IE 8]>
    <script src="{{url('backend/assets/js/html5shiv.min.js')}}"></script>
    <script src="{{url('backend/assets/js/respond.min.js')}}"></script>
<![endif]-->

</head>
<body class="no-skin">
<div class="loader-container">
    <div class="loader-flex">
        <div class="loader">Loading...</div>
    </div>
</div>
<div id="navbar" class="navbar navbar-default ace-save-state">
    <div class="navbar-container ace-save-state" id="navbar-container">
        <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
            <span class="sr-only">Toggle sidebar</span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>
        </button>

        <div class="navbar-header pull-left">
            <a href="{{route('backend.dashboard')}}" class="navbar-brand">
                <small>
                    Hệ thống quản trị nội dung
                </small>
            </a>
        </div>

        <div class="navbar-buttons navbar-header pull-right" role="navigation">
            <ul class="nav ace-nav">

                <li class="light-blue dropdown-modal">
                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                        <img class="nav-user-photo" src="{{\Auth::user()->avatar ? asset(\Auth::user()->avatar) : url('backend/img/default-avatar.png')}}" />
                        <span class="user-info">
                            <small>Xin chào,</small>
                            {{\Auth::user()->name}}
                        </span>

                        <i class="ace-icon fa fa-caret-down"></i>
                    </a>

                    <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                        <li>
                            <a href="{{route('backend.profile')}}">
                                <i class="ace-icon fa fa-user"></i>
                                Thông tin tài khoản
                            </a>
                        </li>

                        <li class="divider"></li>

                        <li>
                            <a href="{{route('backend.logout')}}">
                                <i class="ace-icon fa fa-power-off"></i>
                                Đăng xuất
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div><!-- /.navbar-container -->
</div>

