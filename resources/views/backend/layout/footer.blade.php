<div class="footer">
    <div class="footer-inner">
        <div class="footer-content">
            <span class="bigger-120">
                <p class="text-center">© 2019-{{date('Y')}} <a class="red bolder" href="https://hadava.com">Hadava</a> – Thiết kế website | Giải pháp ứng dụng web | Ứng dụng mobile</p>
            </span>
        </div>
    </div>
</div>
<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
</a>
</div><!-- /.main-container -->

<!--[if !IE]> -->
<script src="{{url('backend/assets/js/jquery-2.1.4.min.j')}}s"></script>
<!-- <![endif]-->
<!--[if IE]>
<script src="{{url('backend/assets/js/jquery-1.11.3.min.js')}}"></script>
<![endif]-->
<script type="text/javascript">
    if('ontouchstart' in document.documentElement) document.write("<script src='{{url('backend/assets/js/jquery.mobile.custom.min.js')}}'>"+"<"+"/script>");
</script>
<script src="{{url('backend/assets/js/bootstrap.min.j')}}s"></script>

<!-- page specific plugin scripts -->

<!--[if lte IE 8]>
<script src="{{url('backend/assets/js/excanvas.min.js')}}"></script>
<![endif]-->
<script src="{{url('backend/assets/js/jquery-ui.custom.min.j')}}s"></script>
<script src="{{url('backend/assets/js/jquery.ui.touch-punch.min.js')}}"></script>
<script src="{{url('backend/assets/js/jquery.easypiechart.min.j')}}s"></script>
<script src="{{url('backend/assets/js/jquery.sparkline.index.min.js')}}"></script>
<script src="{{url('backend/assets/js/jquery.flot.min.j')}}s"></script>
<script src="{{url('backend/assets/js/jquery.flot.pie.min.js')}}"></script>
<script src="{{url('backend/assets/js/jquery.flot.resize.min.j')}}s"></script>

<!-- ace scripts -->
<script src="{{url('backend/assets/js/ace-elements.min.js')}}"></script>
<script src="{{url('backend/assets/js/ace.min.j')}}s"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<!--  Notifications Plugin    -->
<script src="{{url("backend/js/plugins/bootstrap-notify.js")}}"></script>

<script src="{{url("backend/js/plugins/jquery-sortable.js")}}"></script>

<script src="{{url('backend/assets/js/chosen.jquery.min.js')}}"></script>

<script src="{{url('backend/js/ckeditor/ckeditor.js')}}" type="text/javascript"></script>

<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.20/b-1.6.1/datatables.min.js"></script>

<script src="/vendor/datatables/buttons.server-side.js"></script>

<script src="/vendor/laravel-filemanager/js/lfm.js"></script>

<script src="{{url("backend/js/plugins/lodash.min.js")}}" type="text/javascript"></script>

<script>
    var siteUrl = '{{url('')}}'
    var loader = $('.loader-container')
</script>



<script src="{{url("backend/js/common.js")}}" type="text/javascript"></script>
<script src="{{url("backend/js/default.js")}}" type="text/javascript"></script>
<script src="{{url("backend/js/common-ajax.js")}}" type="text/javascript"></script>
<script src="{{url("backend/js/multiple-image.js")}}" type="text/javascript"></script>

@yield('script')
</body>
</html>

