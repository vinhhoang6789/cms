@extends('.backend.layout.main')
@section('title', 'Thêm mới trang tĩnh')
@section('content')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="{{route('pages.index')}}">Danh sách trang tĩnh</a>
            </li>
            <li class="active">Thêm trang tĩnh</li>
        </ul><!-- /.breadcrumb -->
    </div>

    <div class="page-content">

        <div class="page-header">
            <h1>
                Thêm mới trang tĩnh
            </h1>
        </div><!-- /.page-header -->

        <div class="row">
            <div class="col-xs-12">

                <form method="POST" action="{{route('pages.store')}}">
                    @csrf
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="fa fa-close"></i>
                            </button>
                            <div class="error-message">
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </div>
                        </div>
                    @endif

                    @if(session('success'))
                        <div class="alert alert-success mb-5 alert-with-icon">
                            <i class="material-icons" data-notify="icon">done</i>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="material-icons">close</i>
                            </button>
                            {{session('success')}}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-md-8">
                            <div class="widget-box">
                                <div class="card-body">
                                    <div class="widget-main">
                                        <div class="form-group">
                                            <label>Tiêu đề</label>
                                            <input value="{{old('title')}}" type="text" class="form-control" name="title" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Slug</label>
                                            <input value="{{old('slug')}}" type="text" name="slug" class="form-control">
                                            <small class="form-text text-muted">Để trống nếu muốn sinh tự động.</small>
                                        </div>

                                        <div class="editor-group">
                                            <label>Nội dung</label>
                                            <textarea id="editor" name="content" >{{old('content')}}</textarea>
                                        </div>
                                    </div>

                                </div>
                                <!-- end content-->
                            </div>

                        </div>

                        <div class="col-md-4">
                            <div class="widget-box">
                                <div class="widget-main">
                                    <div class="form-group">
                                        <label>Trạng thái</label>
                                        <select name="status" class="form-control">
                                            <option {{old('status') == 'status' ? 'selected' : null}} value="publish">Xuất bản</option>
                                            <option {{old('status') == 'draft' ? 'selected' : null}} value="draft">Bản nháp</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <x-backend.seo :formData="old('seo')"/>

                        </div>
                    </div>
                    <!--  end card  -->

                    <div class="clearfix form-actions">
                        <div class="col-md-offset-4 col-md-8">
                            <button class="btn btn-info" type="submit">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                Thêm mới
                            </button>

                            &nbsp; &nbsp; &nbsp;
                            <button class="btn" type="reset">
                                <i class="ace-icon fa fa-undo bigger-110"></i>
                                Reset
                            </button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
@endsection
