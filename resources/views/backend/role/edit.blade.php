@extends('.backend.layout.main')
@section('title', 'Chỉnh sửa quyền hệ thống')
@section('content')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="{{route('roles.index')}}">Quyền hệ thống</a>
            </li>
            <li class="active">Chỉnh sửa quyền</li>
        </ul><!-- /.breadcrumb -->
    </div>

    <div class="page-content">

        <div class="page-header">
            <h1>
                Chỉnh sửa quyền
            </h1>
        </div><!-- /.page-header -->

        <div class="row">
            <div class="col-xs-12">
                <form method="POST" action="{{route('roles.update', $role->id)}}">
                    @method('PUT')
                    @csrf
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="fa fa-close"></i>
                            </button>
                            <div class="error-message">
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </div>
                        </div>
                    @endif

                    @if(session('success'))
                        <div class="alert alert-success mb-5 alert-with-icon">
                            <i class="fa fa-check" ></i>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="fa fa-close"></i>
                            </button>
                            {{session('success')}}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="widget-box">
                                <div class="widget-body">
                                    <div class="widget-main">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label>Tên quyền</label>
                                                <input type="text" class="form-control" name="name" required value="{{$role->name}}">
                                                <div class="text-danger"></div>
                                            </div>

                                            <div class="form-group">
                                                <label>Có phải là Super Admin không?</label>
                                                <div id="set_as_superadmin" class="radio">
                                                    <label><input {{$role->is_super_admin ? 'checked' : ''}} type="radio" name="is_super_admin" value="1"> Có!</label> &nbsp;&nbsp;
                                                    <label><input {{!$role->is_super_admin ? 'checked' : ''}} type="radio" name="is_super_admin" value="0"> Không</label>
                                                </div>
                                                <div class="text-danger"></div>
                                            </div>

                                            <div id="privileges_configuration" class="form-group" style="display: {{$role->is_super_admin ? 'none' : 'block'}};">
                                                <label>Cấu hình chức năng</label>
                                                <table class="table table-striped table-hover table-bordered">
                                                    <thead>
                                                    <tr class="active">
                                                        <th width="3%">No.</th><th width="60%">Danh sách chức năng</th><th>&nbsp;</th><th>Xem</th><th>Thêm</th><th>Sửa</th><th>Xóa</th>
                                                    </tr>
                                                    <tr class="info">
                                                        <th>&nbsp;</th>
                                                        <th>&nbsp;</th>
                                                        <th>&nbsp;</th>
                                                        <td align="center"><input title="Check all vertical" type="checkbox" id="is_visible"></td>
                                                        <td align="center"><input title="Check all vertical" type="checkbox" id="is_create"></td>
                                                        <td align="center"><input title="Check all vertical" type="checkbox" id="is_edit"></td>
                                                        <td align="center"><input title="Check all vertical" type="checkbox" id="is_delete"></td>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    @foreach($manageableList as $i=>$item)
                                                        @php
                                                            $item = explode('|', $item);
                                                        @endphp

                                                        <tr>
                                                            <td>{{$i+1}}</td>
                                                            <td>{{$item[1]}}</td>
                                                            <td class="info" align="center"><input type="checkbox" title="Check All Horizontal" class="select_horizontal"></td>
                                                            <td class="active" align="center">
                                                                <input {{in_array('visible '.$item[0], $permissions) ? 'checked' : ''}} type="checkbox" class="is_visible" name="permission[{{$item[0]}}][visible]" value="1">
                                                            </td>
                                                            <td class="warning" align="center">
                                                                <input {{in_array('create '.$item[0], $permissions) ? 'checked' : ''}} type="checkbox" class="is_create" name="permission[{{$item[0]}}][create]" value="1">
                                                            </td>
                                                            <td class="success" align="center">
                                                                <input {{in_array('edit '.$item[0], $permissions) ? 'checked' : ''}} type="checkbox" class="is_edit" name="permission[{{$item[0]}}][edit]" value="1">
                                                            </td>
                                                            <td class="danger" align="center">
                                                                <input {{in_array('delete '.$item[0], $permissions) ? 'checked' : ''}} type="checkbox" class="is_delete" name="permission[{{$item[0]}}][delete]" value="1">
                                                            </td>
                                                        </tr>

                                                    @endforeach

                                                    </tbody>
                                                </table>

                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <!-- end content-->
                            </div>

                        </div>
                    </div>
                    <!--  end card  -->

                    <div class="clearfix form-actions">
                        <div class="col-md-offset-4 col-md-8">
                            <button class="btn btn-info" type="submit">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                Lưu lại
                            </button>

                            &nbsp; &nbsp; &nbsp;
                            <button class="btn" type="reset">
                                <i class="ace-icon fa fa-undo bigger-110"></i>
                                Reset
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $('#set_as_superadmin input').click(function() {
            var n = $(this).val();
            if(n == '1') {
                $('#privileges_configuration').hide();
            }else{
                $('#privileges_configuration').show();
            }
        })

        $('#set_as_superadmin input:checked').trigger('click');

        $(function() {
            $("#is_visible").click(function() {
                var is_ch = $(this).prop('checked');
                $(".is_visible").prop("checked",is_ch);
                console.log('Create all');
            })
            $("#is_create").click(function() {
                var is_ch = $(this).prop('checked');
                $(".is_create").prop("checked",is_ch);
            })
            $("#is_read").click(function() {
                var is_ch = $(this).is(':checked');
                $(".is_read").prop("checked",is_ch);
            })
            $("#is_edit").click(function() {
                var is_ch = $(this).is(':checked');
                $(".is_edit").prop("checked",is_ch);
            })
            $("#is_delete").click(function() {
                var is_ch = $(this).is(':checked');
                $(".is_delete").prop("checked",is_ch);
            })
            $(".select_horizontal").click(function() {
                var p = $(this).parents('tr');
                var is_ch = $(this).is(':checked');
                p.find("input[type=checkbox]").prop("checked",is_ch);
            })
        })

    </script>
@endsection
