@extends('.backend.layout.main')
@section('title', 'Chi tiết menu')
@section('content')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="{{route('backend.menu')}}">Danh sách menu</a>
            </li>
            <li class="active">Chi tiết menu</li>
        </ul><!-- /.breadcrumb -->
    </div>
    <div class="page-content">

        <div class="page-header">
            <h1>
                Chi tiết menu
            </h1>
        </div><!-- /.page-header -->

        <div class="row">
        <div class="col-md-6">
            <div class="widget-box">
                <div class="widget-header">
                    <h4>Thứ tự menu</h4>
                </div>
                <div class="widget-main">
                    <small class="form-text text-muted">Kéo thả menu để sắp xếp lại vị trí.</small>
                    @php
                        $items = $menu->menuItems->groupBy('parent');
                    @endphp
                    <ul class="draggable-menu">
                        {!!generateBackendMenu($items) !!}
                    </ul>
                </div>
                <!-- end content-->
            </div>
            <!--  end card  -->
        </div>

        <div class="col-md-6">
            <div class="widget-box">
                <div class="widget-header">
                    <h4>Thêm mới menu</h4>
                </div>
                <div class="widget-main">
                    <form method="POST" action="{{route('backend.menuItem.store')}}" id="add-menu-item">
                        @csrf
                        <input type="hidden" name="menu_id" value="{{$menuId}}"/>
                        <div class="alert alert-danger d-none mb-5">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="fa fa-close"></i>
                            </button>
                            <div class="error-message"></div>
                        </div>

                        <div class="form-group">
                            <label>Tiêu đề</label>
                            <input type="text" class="form-control" name="title" required>
                        </div>
                        <div class="form-group">
                            <label>Đường dẫn</label>
                            <input type="text" name="url" class="form-control" required>
                        </div>

                        <div class="radio">
                            <label>
                                <input class="ace" checked type="radio" name="target" value="current">
                                <span class="lbl">Mở ở tab hiện tại</span>
                            </label>
                        </div>

                        <div class="radio">
                            <label>
                                <input class="ace" type="radio" name="target" value="new-window">
                                <span class="lbl">Mở sang tab mới</span>
                            </label>
                        </div>

                        <button class="btn btn-info" type="submit">
                            <i class="ace-icon fa fa-check bigger-110"></i>
                            Thêm mới
                        </button>
                    </form>
                </div>
                <!-- end content-->
            </div>
            <!--  end card  -->
        </div>
    </div>
    </div>
@endsection
@section('script')
<script src="{{url('backend/js/menu-detail.js')}}"></script>
@endsection
