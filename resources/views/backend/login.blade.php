<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta charset="utf-8"/>
    <title>Đăng nhập hệ thống quản trị website</title>
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset("frontend/img/favicon-32x32.png")}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset("frontend/img/favicon-96x96.png")}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset("frontend/img/favicon-16x16.png")}}">

    <meta name="description" content="User login page"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="{{url('backend/assets/css/bootstrap.min.css')}}"/>
    <link rel="stylesheet" href="{{url('backend/assets/font-awesome/4.5.0/css/font-awesome.min.css')}}"/>

    <!-- text fonts -->
    <link rel="stylesheet" href="{{url('backend/assets/css/fonts.googleapis.com.css')}}"/>

    <!-- ace styles -->
    <link rel="stylesheet" href="{{url('backend/assets/css/ace.min.css')}}"/>

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="{{url('backend/assets/css/ace-part2.min.css')}}"/>
    <![endif]-->
    <link rel="stylesheet" href="{{url('backend/assets/css/ace-rtl.min.css')}}"/>

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="{{url('backend/assets/css/ace-ie.min.css')}}"/>
    <![endif]-->

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
    <link rel="stylesheet" href="{{url('backend/assets/css/custom.css')}}"/>
    <!--[if lte IE 8]>
    <script src="{{url('backend/assets/js/html5shiv.min.js')}}"></script>
    <script src="{{url('backend/assets/js/respond.min.js')}}"></script>
    <![endif]-->
</head>

<body class="login-layout blur-login">
<div class="main-container">
    <div class="main-content">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="login-container">

                    <div class="space-6"></div>
                    <h3 class="center" style="color: #eee">ĐĂNG NHẬP HỆ THỐNG</h3>
                    <div class="position-relative">
                        <div id="login-box" class="login-box visible widget-box no-border">

                            <div class="widget-body">

                                <div class="widget-main">
                                    <h4 class="header blue lighter" style="font-size: 16px">
                                        <i class="ace-icon fa fa-coffee green"></i>
                                        Vui lòng điền thông tin đăng nhập.
                                    </h4>

                                    <div class="space-6"></div>
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </div>
                                    @endif
                                    <form class="form" method="post" action="{{route('backend.post.login')}}">
                                        @csrf
                                        <fieldset>
                                            <label class="block clearfix">
														<span class="block input-icon input-icon-right">
                                                            <input type="username" value="{{old('username')}}"
                                                                   name="username" class="form-control"
                                                                   placeholder="Tên tài khoản...">
															<i class="ace-icon fa fa-user"></i>
														</span>
                                            </label>

                                            <label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="password" name="password" class="form-control"
                                                                   placeholder="Mật Khẩu...">
															<i class="ace-icon fa fa-lock"></i>
														</span>
                                            </label>

                                            <div class="space"></div>

                                            <div class="clearfix">

                                                <button type="submit"
                                                        class=" pull-right btn btn-sm btn-primary">
                                                    <i class="ace-icon fa fa-key"></i>
                                                    <span class="bigger-110">Đăng nhập ngay!</span>
                                                </button>
                                            </div>

                                            <div class="space-4"></div>
                                        </fieldset>
                                    </form>
                                </div><!-- /.widget-main -->
                            </div><!-- /.widget-body -->
                        </div><!-- /.login-box -->

                    </div><!-- /.position-relative -->
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.main-content -->
</div><!-- /.main-container -->

<!-- basic scripts -->

<!--[if !IE]> -->
<script src="{{url('backend/assets/js/jquery-2.1.4.min.js')}}"></script>
<!-- <![endif]-->
<!--[if IE]>
<script src="{{url('backend/assets/js/jquery-1.11.3.min.js')}}"></script>
<![endif]-->
</body>
</html>
