@extends('.backend.layout.main')
@section('title', 'Chi tiết bình luận')
@section('content')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="{{route('comments.index')}}">Danh sách bình luận</a>
            </li>
            <li class="active">Chi tiết bình luận</li>
        </ul><!-- /.breadcrumb -->
    </div>

    <div class="page-content">

        <div class="page-header">
            <h1>
                Chi tiết bình luận
            </h1>
        </div><!-- /.page-header -->

        <div class="row">
            <div class="col-xs-12">
                <div class="widget-box">
                    <div class="widget-header">
                        <h4>Bình luận {{$comment->commentable_type == 'App\Models\Product' ? 'sản phẩm' : 'bài viết'}}</h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <h5>Người bình luận:</h5>
                            <p><strong>{{$comment->name}}</strong></p>
                            <div class="hr hr-18 dotted"></div>

                            <h5>Email:</h5>
                            <p><strong>{{$comment->email}}</strong></p>
                            <div class="hr hr-18 dotted"></div>

                            <h5>Nội dung: </h5>
                            <p><strong>{{$comment->content}}</strong></p>
                            <div class="hr hr-18 dotted"></div>

                            <a target="_blank" href="{{url($comment->commentable->permalink->slug)}}"><i class="fa fa-arrow-right"></i> Xem chi tiết {{$comment->commentable_type == 'App\Models\Product' ? 'sản phẩm' : 'bài viết'}}</a>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
