@extends('.backend.layout.main')
@section('title', 'Chỉnh sửa danh mục sản phẩm')
@section('content')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="{{route('product-categories.index')}}">Danh mục sản phẩm</a>
            </li>
            <li class="active">Chỉnh sửa danh mục sản phẩm</li>
        </ul><!-- /.breadcrumb -->
    </div>

    <div class="page-content">

        <div class="page-header">
            <h1>
                Chỉnh sửa danh mục sản phẩm
            </h1>
        </div><!-- /.page-header -->

        <div class="row">
            <div class="col-xs-12">
                <form method="POST" action="{{route('product-categories.update', $productCategory->id)}}">
                    @method('PUT')
                    @csrf
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="fa fa-close"></i>
                            </button>
                            <div class="error-message">
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </div>
                        </div>
                    @endif

                    @if(session('success'))
                        <div class="alert alert-success mb-5 alert-with-icon">
                            <i class="fa fa-check" ></i>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="fa fa-close"></i>
                            </button>
                            {{session('success')}}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-md-8">
                            <div class="widget-box">
                                <div class="widget-body">
                                    <div class="widget-main">
                                        <div class="form-group">
                                            <label>Tiêu đề</label>
                                            <input type="text" class="form-control" name="title" required value="{{$productCategory->title}}">
                                        </div>
                                        <div class="form-group">
                                            <label>Slug</label>
                                            <input type="text" name="slug" class="form-control" value="{{$productCategory->permalink->slug}}">
                                            <small class="form-text text-muted">Để trống nếu muốn sinh tự động.</small>
                                        </div>

                                        <div class="form-group">
                                            <label>Danh mục cha</label>
                                            <select required name="parent" class="form-control chosen-select">
                                                <option disabled selected>Chọn danh mục</option>
                                                {!!generateCategoryTree($categories, $productCategory->parent) !!}
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Mô tả ngắn</label>
                                            <textarea class="form-control" name="excerpt">{{$productCategory->excerpt}}</textarea>
                                        </div>

                                        <div class="radio">
                                            <label>
                                                <input name="active" type="radio" class="ace" value="1" {{$productCategory->active == 1 ? 'checked' : null}}>
                                                <span class="lbl">Hoạt động</span>
                                            </label>
                                        </div>

                                        <div class="radio">
                                            <label>
                                                <input name="active" type="radio" class="ace" value="0" {{$productCategory->active == 0 || $productCategory->active != 1 ? 'checked' : null}}>
                                                <span class="lbl">Không hoạt động</span>
                                            </label>
                                        </div>

                                        <x-backend.single-image :currentImage="$productCategory->featured_image"/>

                                    </div>
                                </div>
                                <!-- end content-->
                            </div>

                        </div>

                        <div class="col-md-4">
                            <x-backend.seo :formData="$productCategory->seo" :isEdit="true"/>
                        </div>
                    </div>
                    <!--  end card  -->

                    <div class="clearfix form-actions">
                        <div class="col-md-offset-4 col-md-8">
                            <button class="btn btn-info" type="submit">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                Lưu lại
                            </button>

                            <button class="btn" type="reset">
                                <i class="ace-icon fa fa-undo bigger-110"></i>
                                Reset
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
