@extends('.backend.layout.main')
@section('title', $title)
@section('content')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="#">Home</a>
            </li>
            <li class="active">{{$title}}</li>
        </ul><!-- /.breadcrumb -->
    </div>

    <div class="page-content">

        <div class="page-header">
            <h1>
                {{$title}}
            </h1>
        </div><!-- /.page-header -->

        <div class="row">
            <div class="col-xs-12">
                @if($type == 'cache')
                    <a href="{{route('backend.cache.clear')}}" class="btn btn-danger" type="button"><i class="fa fa-trash-o"></i> Xóa cache</a>
                    <p><small>Xóa toàn bộ cache website.</small></p>
                @endif


                <div class="widget-box">
                    <div class="widget-main">
                        <form method="POST" action="{{route('backend.setting.save')}}">
                            <input type="hidden" name="type" value="{{$type}}"/>
                    @csrf
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="fa fa-close"></i>
                            </button>
                            <div class="error-message">
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </div>
                        </div>
                    @endif

                    @if(session('success'))
                        <div class="alert alert-success mb-5 alert-with-icon">
                            <i class="fa fa-check"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="fa fa-close"></i>
                            </button>
                            {{session('success')}}
                        </div>
                    @endif
                    @foreach($settings as $item)
                        @if($item->type == 'text')
                            <div class="form-group">
                                <label for="{{$item->id}}">{{$item->title}}</label>
                                <input type="text" class="form-control" id="{{$item->id}}" value="{{$item->value}}"
                                       name="setting[{{$item->id}}]">
                            </div>
                        @elseif($item->type == 'textarea')
                            <div class="form-group">
                                <label for="{{$item->id}}">{{$item->title}}</label>
                                <textarea class="form-control" id="{{$item->id}}"
                                          name="setting[{{$item->id}}]">{{$item->value}}</textarea>
                            </div>
                        @elseif($item->type == 'editor')
                            <div class="editor-group">
                                <label for="{{$item->id}}">{{$item->title}}</label>
                                <textarea class="form-control tinymce" id="{{$item->id}}"
                                          name="setting[{{$item->id}}]">{!! $item->value !!}</textarea>
                            </div>
                        @elseif($item->type == 'file')
                            <div class="form-group">
                                <label for="{{$item->id}}">{{$item->title}}</label>
                                @php
                                    $imageEl = 'holder_'.$item->id;
                                    $removeEl = 'remove_holder_'.$item->id;
                                    $name = 'setting['.$item->id.']';
                                    $fileEl = 'file_holder_'.$item->id;
                                @endphp

                                <x-backend.single-image
                                    :currentImage="$item->value"
                                    :imageEl="$imageEl"
                                    :removeEl="$removeEl"
                                    :name="$name"
                                    :fileEl="$fileEl"
                                />

                            </div>
                            @elseif($item->type == 'option')
                                <div class="form-group">
                                    <label for="{{$item->id}}">{{$item->title}}</label>
                                    <div class="form-check">
                                        <label>
                                            <input {{$item->value == 1 ? 'checked' : ''}} name="setting[{{$item->id}}]" value="1" class="ace ace-switch" type="checkbox">
                                            <span class="lbl"></span>
                                        </label>
                                    </div>
                                </div>
                            @endif
                    @endforeach
                    <div class="clearfix form-actions">
                        <button type="submit" class="btn btn-fill btn-info"><i class="ace-icon fa fa-check bigger-110"></i>Lưu lại</button>
                    </div>
                </form>
                    </div>
                </div>
            </div>
            <!-- end content-->
        </div>
        <!--  end card  -->
    </div>
@endsection
