@extends('.backend.layout.main')
@section('title', $title)
@section('content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="#">Home</a>
        </li>
        <li class="active">{{$title}}</li>
    </ul><!-- /.breadcrumb -->
</div>

<div class="page-content">
    <div class="page-header">
        <h1>{{$title}}</h1>
    </div><!-- /.page-header -->

    <div class="row">
        <div class="col-xs-12">
            {!! $dataTable->table() !!}
        </div>
    </div><!-- /.row -->
</div><!-- /.page-content -->
@endsection
@section('script')
    {!! $dataTable->scripts() !!}
    @if(session('success'))
        <script>
            notify('done', "{{session('success')}}", 'success')
        </script>
    @endif
@endsection
