@extends('.backend.layout.main')
@section('title', 'Danh sách trang tĩnh')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">view_list</i>
                        <h4>Danh sách trang tĩnh</h4>
                    </div>
                </div>
                <div class="card-body">
                    <div class="material-datatables">
                        {!! $dataTable->table() !!}
                    </div>
                </div>
                <!-- end content-->
            </div>
            <!--  end card  -->
        </div>
        <!-- end col-md-12 -->
    </div>
@endsection
@section('script')
    {!! $dataTable->scripts() !!}
@endsection
