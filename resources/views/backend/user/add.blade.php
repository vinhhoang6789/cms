@extends('.backend.layout.main')
@section('title', 'Thêm mới trang tĩnh')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <form method="POST" action="{{route('pages.store')}}">
                @csrf
                @if($errors->any())
                    <div class="alert alert-success mb-5 alert-with-icon">
                        <i class="material-icons" data-notify="icon">notifications</i>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                        <div class="error-message">
                            @foreach($errors as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </div>
                    </div>
                @endif

                @if(session('success'))
                    <div class="alert alert-success mb-5 alert-with-icon">
                        <i class="material-icons" data-notify="icon">done</i>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                        {{session('success')}}
                    </div>
                @endif

                <div class="alert alert-danger d-none mb-5">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="material-icons">close</i>
                    </button>
                    <div class="error-message"></div>
                </div>

                <div class="row">
                    <div class="col-md-9">
                        <div class="card">
                            <div class="card-header card-header-success card-header-icon">
                                <div class="card-icon">
                                    <i class="material-icons">info</i>
                                    <h4>Tổng quan trang tĩnh</h4>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="form-group bmd-form-group">
                                    <label class="bmd-label-floating">Tiêu đề</label>
                                    <input type="text" class="form-control" name="title" required>
                                </div>
                                <div class="form-group bmd-form-group">
                                    <label for="articleSlug" class="bmd-label-floating">Slug</label>
                                    <input type="text" name="slug" class="form-control">
                                    <small class="form-text text-muted">Để trống nếu muốn sinh tự động.</small>
                                </div>

                                <div class="editor-group">
                                    <label>Nội dung</label>
                                    <textarea id="editor" name="content" ></textarea>
                                </div>

                            </div>
                            <!-- end content-->
                        </div>

                        <div class="card">
                            <div class="card-header card-header-info">
                                <h4 class="card-title">Cấu hình SEO</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-group bmd-form-group">
                                    <label class="bmd-label-floating">Tiêu đề</label>
                                    <input type="text" class="form-control" name="seo[title]">
                                </div>
                                <div class="form-group bmd-form-group">
                                    <label class="bmd-label-floating">Canonical</label>
                                    <input type="text" name="seo[canonical]" class="form-control">
                                </div>
                                <div class="form-group bmd-form-group">
                                    <label class="bmd-label-floating">Description</label>
                                    <textarea name="seo[description]" class="form-control"></textarea>
                                </div>
                                <div class="form-group bmd-form-group">
                                    <label class="bmd-label-floating">Scheme</label>
                                    <textarea name="seo[scheme]" class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Index</label>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" name="seo[index]" value="1" checked type="radio"/>
                                            Có
                                            <span class="circle">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" value="0" name="seo[index]"/>
                                            Không
                                            <span class="circle">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Follow</label>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" name="seo[follow]" value="1" checked type="radio"/>
                                            Có
                                            <span class="circle">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" value="0" name="seo[follow]"/>
                                            Không
                                            <span class="circle">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="col-md-3">
                        <div class="card">
                            <div class="card-header card-header-info">
                                <h4 class="card-title">Đăng bài</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Trạng thái</label>
                                    <select name="status" class="selectpicker" data-style="btn btn-link">
                                        <option value="publish">Xuất bản</option>
                                        <option value="draft">Bản nháp</option>
                                    </select>
                                </div>

                                <br>
                                <button type="submit" class="btn btn-fill btn-primary">Lưu lại</button>
                            </div>
                        </div>


                    </div>
                </div>
                <!--  end card  -->
            </form>
        </div>
    </div>
@endsection
