@extends('.backend.layout.main')
@section('title', 'Chi tiết menu')
@section('content')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="{{route('backend.menu.detail', $item->menu_id)}}">Menu</a>
            </li>
            <li class="active">Cập nhật menu</li>
        </ul><!-- /.breadcrumb -->
    </div>
    <div class="page-content">

        <div class="page-header">
            <h1>
                Cập nhật menu
            </h1>
        </div><!-- /.page-header -->

        <div class="row">
            <div class="col-xs-12">
                <form method="POST" action="{{route('backend.menuItem.update')}}" id="add-menu-item">
                    @csrf
                    <input type="hidden" name="id" value="{{$item->id}}"/>
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="fa fa-close"></i>
                            </button>
                            <div class="error-message">
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </div>
                        </div>
                    @endif

                    @if(session('success'))
                        <div class="alert alert-success mb-5 alert-with-icon">
                            <i class="fa fa-check" ></i>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="fa fa-close"></i>
                            </button>
                            {{session('success')}}
                        </div>
                    @endif

                    <div class="form-group bmd-form-group">
                        <label for="menuName" class="bmd-label-floating">Tiêu đề</label>
                        <input type="text" value="{{$item->title}}" class="form-control" id="menuName" name="title"
                               required>
                    </div>
                    <div class="form-group bmd-form-group">
                        <label for="menuUrl" class="bmd-label-floating">Đường dẫn</label>
                        <input type="text" name="url" value="{{$item->url}}" class="form-control" id="menuUrl" required>
                    </div>

                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" {{$item->target == 'current' ? 'checked' : ''}} type="radio"
                                   name="target" value="current"> Mở ở tab hiện tại
                            <span class="circle">
                              <span class="check"></span>
                            </span>
                        </label>
                    </div>

                    <div class="form-check">
                        <label class="form-check-label">
                            <input {{$item->target == 'new-window' ? 'checked' : ''}} class="form-check-input"
                                   type="radio" name="target" value="new-window"> Mở sang tab mới
                            <span class="circle">
                              <span class="check"></span>
                            </span>
                        </label>
                    </div>
                    <button type="submit" class="btn btn-fill btn-primary">Lưu lại</button>
                </form>
            </div>

        </div>
    </div>


@endsection
