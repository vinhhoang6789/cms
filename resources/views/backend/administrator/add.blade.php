@extends('.backend.layout.main')
@section('title', 'Thêm quản trị viên')
@section('content')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="{{route('administrators.index')}}">Danh sách quản trị viên</a>
            </li>
            <li class="active">Thêm quản trị viên</li>
        </ul><!-- /.breadcrumb -->
    </div>

    <div class="page-content">

        <div class="page-header">
            <h1>
                Thêm quản trị viên
            </h1>
        </div><!-- /.page-header -->

        <div class="row">
            <div class="col-xs-12">
                <form method="POST" action="{{route('administrators.store')}}">
                    @csrf
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="fa fa-close"></i>
                            </button>
                            <div class="error-message">
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </div>
                        </div>
                    @endif

                    @if(session('success'))
                        <div class="alert alert-success mb-5 alert-with-icon">
                            <i class="fa fa-check" ></i>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="fa fa-close"></i>
                            </button>
                            {{session('success')}}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-md-12">
                            <div class="widget-box">
                                <div class="widget-body">
                                    <div class="widget-main">
                                        <div class="form-group">
                                            <label>Tên người dùng</label>
                                            <input type="text" class="form-control" name="name" required value="{{old('name')}}">
                                        </div>

                                        <div class="form-group">
                                            <label>Tên đăng nhập</label>
                                            <input type="text" class="form-control" name="username" required value="{{old('username')}}">
                                        </div>

                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" class="form-control" name="email" required value="{{old('email')}}">
                                        </div>

                                        <div class="form-group">
                                            <label>Mật khẩu</label>
                                            <input minlength="6" type="password" class="form-control" name="password" required value="{{old('password')}}">
                                        </div>

                                        <div class="form-group">
                                            <label>Phân quyền</label>
                                            <select class="form-control" name="role_id" required>
                                                <option disabled selected value="">Chọn quyền cho người dùng</option>
                                                @foreach($roles as $role)
                                                    <option {{old('role_id') == $role->id ? 'selected' : ''}} value="{{$role->id}}">{{$role->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            @php
                                                $fileName = 'avatar';
                                            @endphp
                                            <label>Chọn avatar</label>
                                            <x-backend.single-image :currentImage="old('avatar')" :name="$fileName"/>
                                        </div>

                                    </div>
                                </div>
                                <!-- end content-->
                            </div>
                        </div>

                    </div>
                    <!--  end card  -->

                    <div class="clearfix form-actions">
                        <div class="col-md-offset-4 col-md-8">
                            <button class="btn btn-info" type="submit">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                Thêm mới
                            </button>

                            &nbsp; &nbsp; &nbsp;
                            <button class="btn" type="reset">
                                <i class="ace-icon fa fa-undo bigger-110"></i>
                                Reset
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
