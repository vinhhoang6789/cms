@extends('.backend.layout.main')
@section('title', 'Thêm mới block')
@section('content')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="{{route('blocks.index')}}">Danh sách block</a>
            </li>
            <li class="active">Thêm block</li>
        </ul><!-- /.breadcrumb -->
    </div>

    <div class="page-content">

        <div class="page-header">
            <h1>
                Thêm mới block
            </h1>
        </div><!-- /.page-header -->

        <div class="row">
            <div class="col-xs-12">

                <form method="POST" action="{{route('blocks.store')}}">
                    @csrf
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="fa fa-close"></i>
                            </button>
                            <div class="error-message">
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </div>
                        </div>
                    @endif

                    @if(session('success'))
                        <div class="alert alert-success mb-5 alert-with-icon">
                            <i class="material-icons" data-notify="icon">done</i>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="material-icons">close</i>
                            </button>
                            {{session('success')}}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-md-8">
                            <div class="widget-box">
                                <div class="card-body">
                                    <div class="widget-main">
                                        <div class="form-group">
                                            <label>Tiêu đề</label>
                                            <input value="{{old('title')}}" type="text" class="form-control" name="title" required>
                                        </div>

                                        <div class="form-group">
                                            <label>Slug</label>
                                            <input value="{{old('slug')}}" type="text" name="slug" class="form-control">
                                            <small class="form-text text-muted">Để trống nếu muốn sinh tự động.</small>
                                        </div>

                                        <div class="editor-group">
                                            <label>Nội dung</label>
                                            <textarea id="editor" name="content" >{{old('content')}}</textarea>
                                        </div>


                                    </div>

                                </div>
                                <!-- end content-->
                            </div>

                        </div>

                        <div class="col-md-4">
                            <div class="widget-box">
                                <div class="widget-header">
                                    <h4 class="card-title">Nhóm & Liên kết</h4>
                                </div>

                                <div class="widget-main">
                                    <div class="form-group">
                                        <label>Liên kết</label>
                                        <input value="{{old('url')}}" type="text" class="form-control" name="url">
                                    </div>

                                    <div class="form-group">
                                        <label>Nhóm</label>
                                        <select class="form-control" name="group">
                                            <option value="">Chọn nhóm block</option>
                                            <option {{old('group') == 'home' ? 'selected' : ''}} value="home">Trang chủ</option>
                                        </select>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!--  end card  -->

                    <div class="clearfix form-actions">
                        <div class="col-md-offset-4 col-md-8">
                            <button class="btn btn-info" type="submit">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                Thêm mới
                            </button>

                            &nbsp; &nbsp; &nbsp;
                            <button class="btn" type="reset">
                                <i class="ace-icon fa fa-undo bigger-110"></i>
                                Reset
                            </button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
@endsection
