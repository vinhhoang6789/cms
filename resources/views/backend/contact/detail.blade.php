@extends('.backend.layout.main')
@section('title', 'Chi tiết liên hệ')
@section('content')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="{{route('contacts.index')}}">Danh sách liên hệ</a>
            </li>
            <li class="active">Chi tiết liên hệ</li>
        </ul><!-- /.breadcrumb -->
    </div>

    <div class="page-content">

        <div class="page-header">
            <h1>
                Chi tiết liên hệ
            </h1>
        </div><!-- /.page-header -->

        <div class="row">
            <div class="col-xs-12">
                <div class="widget-box">
                    <div class="widget-header">
                        <h4>Chi tiết liên hệ</h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <h5>Người liên hệ:</h5>
                            <p><strong>{{$contact->name}}</strong></p>
                            <div class="hr hr-18 dotted"></div>

                            <h5>Email:</h5>
                            <p><strong>{{$contact->email}}</strong></p>
                            <div class="hr hr-18 dotted"></div>

                            <h5>SĐT:</h5>
                            <p><strong>{{$contact->phone}}</strong></p>
                            <div class="hr hr-18 dotted"></div>

                            <h5>Nội dung: </h5>
                            <p><strong>{{$contact->content}}</strong></p>
                            <div class="hr hr-18 dotted"></div>

                            <h5>File đính kèm: </h5>
                            @foreach($contact->attachments as $attach)
                                <p><a target="_blank" href="{{url('storage/'.$attach->url)}}"><strong>{{$attach->url}}</strong></a></p>
                            @endforeach
                            <div class="hr hr-18 dotted"></div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
