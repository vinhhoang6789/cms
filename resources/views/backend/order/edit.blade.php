@extends('.backend.layout.main')
@section('title', 'Chi tiết menu')
@section('content')
    <div class="row">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('backend.menu.detail', $item->menu_id)}}">Menu</a></li>
                <li class="breadcrumb-item active" aria-current="page">Cập nhật menu</li>
            </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-warning card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">edit</i>
                        <h4>Cập nhật menu</h4>
                    </div>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{route('backend.menuItem.update')}}" id="add-menu-item">
                        @csrf
                        <input type="hidden" name="id" value="{{$item->id}}"/>
                        @if($errors->any())
                            <div class="alert alert-success mb-5 alert-with-icon">
                                <i class="material-icons" data-notify="icon">notifications</i>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <i class="material-icons">close</i>
                                </button>
                                <div class="error-message">
                                    @foreach($errors as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </div>
                            </div>
                        @endif

                        @if(session('success'))
                            <div class="alert alert-success mb-5 alert-with-icon">
                                <i class="material-icons" data-notify="icon">done</i>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <i class="material-icons">close</i>
                                </button>
                                {{session('success')}}
                            </div>
                        @endif

                        <div class="form-group bmd-form-group">
                            <label for="menuName" class="bmd-label-floating">Tiêu đề</label>
                            <input type="text" value="{{$item->title}}" class="form-control" id="menuName" name="title" required>
                        </div>
                        <div class="form-group bmd-form-group">
                            <label for="menuUrl" class="bmd-label-floating">Đường dẫn</label>
                            <input type="text" name="url" value="{{$item->url}}" class="form-control" id="menuUrl" required>
                        </div>

                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" {{$item->target == 'current' ? 'checked' : ''}} type="radio" name="target" value="current"> Mở ở tab hiện tại
                                <span class="circle">
                              <span class="check"></span>
                            </span>
                            </label>
                        </div>

                        <div class="form-check">
                            <label class="form-check-label">
                                <input {{$item->target == 'new-window' ? 'checked' : ''}} class="form-check-input" type="radio" name="target" value="new-window"> Mở sang tab mới
                                <span class="circle">
                              <span class="check"></span>
                            </span>
                            </label>
                        </div>
                        <button type="submit" class="btn btn-fill btn-rose">Lưu lại</button>
                    </form>
                </div>
                <!-- end content-->
            </div>
            <!--  end card  -->
        </div>
    </div>
@endsection
