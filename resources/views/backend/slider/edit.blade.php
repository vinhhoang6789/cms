@extends('.backend.layout.main')
@section('title', 'Chỉnh sửa slider')
@section('content')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="{{route('sliders.index')}}">Danh sách sliders</a>
            </li>
            <li class="active">Chỉnh sửa slider</li>
        </ul><!-- /.breadcrumb -->
    </div>

    <div class="page-content">

        <div class="page-header">
            <h1>
                Chỉnh sửa slider
            </h1>
        </div><!-- /.page-header -->

        <div class="row">
            <div class="col-xs-12">
                <form method="POST" action="{{route('sliders.update', $slider->id)}}">
                    @method('PUT')
                    @csrf
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="fa fa-close"></i>
                            </button>
                            <div class="error-message">
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </div>
                        </div>
                    @endif

                    @if(session('success'))
                        <div class="alert alert-success mb-5 alert-with-icon">
                            <i class="fa fa-check" ></i>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="fa fa-close"></i>
                            </button>
                            {{session('success')}}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-md-8">
                            <div class="widget-box">
                                <div class="widget-body">
                                    <div class="widget-main">
                                        <div class="form-group">
                                            <label>Tiêu đề</label>
                                            <input type="text" class="form-control" name="name" required value="{{$slider->name}}">
                                        </div>

                                        <div class="form-group">
                                            <label>Liên kết</label>
                                            <input type="text" class="form-control" name="url" value="{{$slider->url}}">
                                        </div>

                                        <div class="form-group">
                                            <label>Mô tả</label>
                                            <textarea name="description" class="form-control">{{$slider->description}}</textarea>
                                        </div>

                                        <div class="form-group">
                                            <label>Nhóm</label>
                                            <select class="form-control" name="group">
                                                <option value="">Chọn nhóm slide</option>
                                                <option {{$slider->group == 'home' ? 'selected' : ''}} value="home">Trang chủ</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <!-- end content-->
                            </div>

                        </div>

                        <div class="col-md-4">
                            <div class="widget-box">
                                <div class="widget-header">
                                    <h4 class="card-title">Trạng thái & Hình ảnh</h4>
                                </div>
                                <div class="widget-main">
                                    <div class="form-group">
                                        <div class="radio">
                                            <label>
                                                <input name="active" type="radio" class="ace" value="1" {{$slider->active == 1 || $slider->active !== 0 ? 'checked' : null}}>
                                                <span class="lbl">Hoạt động</span>
                                            </label>
                                        </div>

                                        <div class="radio">
                                            <label>
                                                <input name="active" type="radio" class="ace" value="0" {{$slider->active === 0  ? 'checked' : null}}>
                                                <span class="lbl">Không hoạt động</span>
                                            </label>
                                        </div>
                                    </div>
                                    @php
                                        $name = 'image'
                                    @endphp
                                    <x-backend.single-image :currentImage="$slider->image" :name="$name"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  end card  -->

                    <div class="clearfix form-actions">
                        <div class="col-md-offset-4 col-md-8">
                            <button class="btn btn-info" type="submit">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                Lưu lại
                            </button>
                            &nbsp; &nbsp; &nbsp;
                            <button class="btn" type="reset">
                                <i class="ace-icon fa fa-undo bigger-110"></i>
                                Reset
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
