@extends('layouts.app')

@section('content')
    <div class="full-with-slider">
        @foreach($sliders as $slider)
        <div style="background-image: url('{{asset($slider->image)}}')">
            <div class="container d-flex align-items-center">
                <div class="row">
                    <div class="col-lg-7 animated d-lg-none">
                        <h2>{{$slider->name}}</h2>
                        <p>{{$slider->description}}</p>
                        <a class="btn-primary btn animated delay-1s bounceIn" href="{{$slider->url}}">Xem thêm <ion-icon name="chevron-forward-outline"></ion-icon></a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>

    <section class="page-section our-service bg-secondary py-5 animated fadeInLeft">
        <div class="text-center pb-2">
            <h3 class="under-title">Dịch vụ của HADAVA</h3>
        </div>
        <div class="container">
            <div class="row">
                @if(!empty($blocks['thiet-ke-website']))
                    @php
                        $block = $blocks['thiet-ke-website']
                    @endphp
                    <div class="col-md-4 mb-3 text-center text-lg-left">
                        <img class="mb-3" src="{{asset("frontend/img/web-design.png")}}" alt="{{$block->title}}"/>
                        <h4>{{$block->title}}</h4>
                        {!! $block->content !!}
                        <a href="{{$block->url}}" class="btn btn-primary">Xem thêm <ion-icon name="chevron-forward-outline"></ion-icon></a>
                    </div>
                @endif

                @if(!empty($blocks['ung-dung-website']))
                    @php
                        $block = $blocks['ung-dung-website']
                    @endphp
                    <div class="col-md-4 mb-3 text-center text-lg-left">
                        <img class="mb-3" src="{{asset("frontend/img/web-app.png")}}" alt="{{$block->title}}"/>
                        <h4>{{$block->title}}</h4>
                        {!! $block->content !!}
                        <a href="{{$block->url}}" class="btn btn-primary">Xem thêm <ion-icon name="chevron-forward-outline"></ion-icon></a>
                    </div>
                @endif

                @if(!empty($blocks['ung-dung-di-dong']))
                    @php
                        $block = $blocks['ung-dung-di-dong']
                    @endphp
                    <div class="col-md-4 mb-3 text-center text-lg-left">
                        <img class="mb-3" src="{{asset("frontend/img/mobile-app.png")}}" alt="{{$block->title}}"/>
                        <h4>{{$block->title}}</h4>
                        {!! $block->content !!}
                        <a href="{{$block->url}}" class="btn btn-primary">Xem thêm <ion-icon name="chevron-forward-outline"></ion-icon></a>
                    </div>
                @endif
            </div>
        </div>
    </section>

    <section class="quality-section page-section">
        <div class="text-center pb-2">
            <h3 class="under-title">Uy tín và chất lượng</h3>
        </div>
        <div class="container">
            <h4 class="text-center">HADAVA luôn đặt trách nhiệm vào từng dự án. Mỗi dự án được hoàn thành là niềm tự hào của chúng tôi. Tất cả những dự án đều được kiểm tra chất lượng nghiêm ngặt trước khi bàn giao cho quy khách.</h4>
            <br>
            <div class="d-flex justify-content-between item">
                <div class="mb-3">
                    <h2>300+</h2>
                    <p class="note">Hơn 300 dự án đã được hoàn thành.</p>
                </div>
                <div class="mb-3">
                    <h2>1000+</h2>
                    <p class="note">Hơn 1000 mẫu thiết kế website.</p>
                </div>
            </div>
        </div>

    </section>

    <section class="page-section workflow-section bg-secondary m-0 py-5">
        <div class="text-center pb-2">
            <h3 class="under-title">Quy trình làm việc</h3>
        </div>
        <div class="container">
            <div class="d-none align-items-center item animated faster">
                <img src="{{asset("frontend/img/interview.png")}}" alt="Gặp gỡ">
                <div class="ml-2">
                    <p class="mb-1"><strong>Gặp gỡ</strong></p>
                    <p class="mb-0">Cùng gặp nhau để nói rõ về dự án của bạn.</p>
                </div>
            </div>

            <div class="d-none align-items-center item animated delay-2s faster">
                <img src="{{asset("frontend/img/coding.png")}}" alt="Thiết kế & lập trình">
                <div class="ml-2">
                    <p class="mb-1"><strong>Thiết kế & lập trình</strong></p>
                    <p class="mb-0">Dựa trên những yêu cầu của website.</p>
                </div>
            </div>

            <div class="d-none align-items-center item delay-1s animated faster">
                <div class="mr-2 text-right">
                    <p class="mb-1"><strong>Lên giải pháp</strong></p>
                    <p class="mb-0">Phân tích những tính năng của website để lựa chọn những công nghệ phù hợp.</p>
                </div>
                <img src="{{asset("frontend/img/startup.png")}}" alt="Lên giải pháp">
            </div>

            <div class="d-none align-items-center item delay-3s animated faster">
                <div class="mr-2 text-right">
                    <p class="mb-1"><strong>Kiểm thử & Cập nhật nội dung</strong></p>
                    <p class="mb-0">Kiểm tra tất cả các tính năng để chắc chắn website chạy mượt mà.</p>
                </div>
                <img src="{{asset("frontend/img/checklist.png")}}" alt="Kiểm thử & Cập nhật nội dung">
            </div>

            <div class="d-none align-items-center item delay-4s animated">
                <div class="mr-2 text-right">
                    <p class="mb-1"><strong>Cấu hình & Bàn giao</strong></p>
                    <p class="mb-0">Triển khai và hỗ trợ khách hàng quản trị website.</p>
                </div>
                <img src="{{asset("frontend/img/rocket.png")}}" alt="Cấu hình & Bàn giao">
            </div>
        </div>
    </section>

    <section class="testimonial-section page-section">
        <div class="text-center">
            <h3 class="under-title">Cảm nhận khách hàng</h3>
        </div>
        <div class="testimonial-slider container">
            <div class="d-flex flex-column align-items-center justify-content-center">
                <img class="avatar" alt="Sái Hoài Nam" src="{{asset('frontend/img/avatar-1.jpeg')}}"/>
                <p>“Tôi đánh giá cao công ty HADAVA! Hiện tôi đã làm rất nhiều trang web với HADAVA, tôi rất hài lòng về cách phục vụ và khả năng lập trình của đội ngũ nhân viên.”</p>
                <h3>Sái Hoài Nam</h3>
                <p>TP. Hà Nội</p>
            </div>

            <div class="d-flex flex-column align-items-center justify-content-center">
                <img class="avatar" alt="Kim Thành An" src="{{asset('frontend/img/avatar-2.jpeg')}}"/>
                <p>“Nhân viên công ty HADAVA tư vấn rất nhiệt tình giúp công ty tôi nâng cấp lại web rất chuẩn SEO. Tôi sẽ còn gắn bó với HADAVA Việt Nam trong thời gian dài.”</p>
                <h3>Kim Thành An</h3>
                <p>TP. Hà Nội</p>
            </div>

            <div class="d-flex flex-column align-items-center justify-content-center">
                <img class="avatar" alt="Vũ Nam Nhật" src="{{asset('frontend/img/avatar-3.jpeg')}}"/>
                <p>“HADAVA có cách làm việc của công ty khá chuyên nghiệp vì vậy, tôi luôn luôn tin tưởng đặt hàng những dự án web hay phần mềm tại công ty, tôi rất yên tâm.”</p>
                <h3>Vũ Nam Nhật</h3>
                <p>TP. Đà Nẵng</p>
            </div>
        </div>
    </section>

    <section class="tech-section page-section">
        <div class="text-center pb-2">
            <h3 class="under-title">Công nghệ ứng dụng</h3>
        </div>
        <div class="partner-slider container">
            <img alt="Angular" src="{{asset("frontend/img/icon-angular.png")}}"/>
            <img alt="Bootstrap" src="{{asset("frontend/img/icon-bootstrap.png")}}"/>
            <img alt="Laravel" src="{{asset("frontend/img/icon-laravel.png")}}"/>
            <img alt="Nodejs" src="{{asset("frontend/img/icon-nodejs.png")}}"/>
            <img alt="React" src="{{asset("frontend/img/icon-react.png")}}"/>
            <img alt="React Native" src="{{asset("frontend/img/icon-react-native.png")}}"/>
            <img alt="Sketch" src="{{asset("frontend/img/icon-sketch.png")}}"/>
            <img alt="Vuejs" src="{{asset("frontend/img/icon-vuejs.png")}}"/>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{asset('frontend/js/home.js')}}"></script>
@endsection
