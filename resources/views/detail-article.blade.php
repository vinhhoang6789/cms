@extends('layouts.app')
@section('content')
    <div class="single-article container my-4">
        <div class="row">
            <div class="col-md-8 mb-3">
                <nav aria-lable="breadcrumb" class="mb-3">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{\Config::get('app.url')}}">Trang chủ</a>
                            <ion-icon name="chevron-forward-outline"></ion-icon>
                        </li>
                        @if($breadcrumbCategories)
                            @foreach($breadcrumbCategories as $cat)
                                <li class="breadcrumb-item">
                                    <a href="{{$cat->url()}}">
                                        {{$cat->title}}
                                    </a>
                                    <ion-icon name="chevron-forward-outline"></ion-icon>
                                </li>
                            @endforeach
                        @endif
                        <li class="breadcrumb-item active" aria-current="page">{{$article->title}}</li>
                    </ol>
                </nav>

                <h1>{{$article->title}}</h1>

                <div class="d-flex align-items-center">
                    <span class="mr-2">Chia sẻ:</span>
                    {!! $share !!}
                </div>

                <div class="content py-3 mb-3 text-justify">
                    {!! $article->content !!}
                </div>

                @if(!$relatedPosts->isEmpty())
                    <div class="related-news">
                        <h3>Tin liên quan</h3>
                        <div class="row">
                            @foreach($relatedPosts as $post)
                                <div class="col-md-4 col-lg-3 mb-3">
                                    <a href="{{$post->url()}}" title="{{$post->title}}" class="d-flex d-md-block">
                                        <img src="{{photoThumb($post->featured_image)}}" alt="{{$post->title}}"
                                             class="mr-2 mr-md-0"/>
                                        <p class="mt-2 mb-0 limit-3">{{$post->title}}</p>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                        <a class="text-center d-block" title="Tin tức" style="text-decoration: underline"
                           href="{{url('tin-tuc')}}">Xem thêm</a>
                    </div>
                @endif
            </div>

            <div class="col-md-4 mb-4">
                @if(!$popularPosts->isEmpty())
                    <div class="right-box mb-3">
                        <h3 class="text-center">Bài viết nổi bật</h3>
                        @foreach($popularPosts as $post)
                            <a href="{{$post->url()}}" title="{{$post->title}}"
                               class="p-2 item d-flex align-items-center bg-secondary mb-2">
                                <img src="{{photoThumb($post->featured_image)}}" alt="{{$post->title}}"/>
                                <p class="ml-2 mb-0 limit-2">{{$post->title}}</p>
                            </a>
                        @endforeach
                    </div>
                @endif

                <div class="right-box mb-4 why-choose p-2">
                    <h3 class="text-center">Lý do chọn Hadava</h3>
                    <p class="note my-3 text-center">Dưới đây là những lý do bạn nên chọn chúng tôi.</p>

                    <div class="d-flex mb-2">
                        <div class="icon mr-1">
                            <ion-icon name="checkmark-circle-outline"></ion-icon>
                        </div>
                        <div>
                            <h5>Đội ngũ chuyên nghiệp</h5>
                            <p>Nhân viên thiết kế và lập trình đã có nhiều năm kinh nghiệm tạo ra những website hấp dẫn,
                                chuẩn xác đến từng pixel. </p>
                        </div>
                    </div>

                    <div class="d-flex mb-2">
                        <div class="icon mr-1">
                            <ion-icon name="checkmark-circle-outline"></ion-icon>
                        </div>
                        <div>
                            <h5>Hàng trăm khách hàng tin tưởng</h5>
                            <p>Havada đã giúp hơn 300 doanh nghiệp thành công với chiến lược marketing online.</p>
                        </div>
                    </div>

                    <div class="d-flex mb-2">
                        <div class="icon mr-1">
                            <ion-icon name="checkmark-circle-outline"></ion-icon>
                        </div>
                        <div>
                            <h5>Tùy chỉnh theo nhu cầu của bạn</h5>
                            <p>Hadava sẽ giúp bạn tư vấn và phát triển những chức năng riêng để phù hợp với chiến lược
                                kinh
                                doanh của bạn.</p>
                        </div>
                    </div>

                    <div class="d-flex mb-2">
                        <div class="icon mr-1">
                            <ion-icon name="checkmark-circle-outline"></ion-icon>
                        </div>
                        <div>
                            <h5>Công nghệ tiên tiến</h5>
                            <p>Áp dụng những công nghệ mới nhất hiện nay. Giúp ứng dụng hoạt động mượt mà.</p>
                        </div>
                    </div>

                </div>

                <div class="right-box mb-3 free-advise p-2">
                    <h3 class="text-center">Đăng ký tư vấn</h3>
                    <p class="note text-center">Dự án của bạn là gì? Hãy gửi cho chúng tôi yêu cầu của bạn, nhân viên tư
                        vấn của chúng tôi sẽ liên hệ với bạn ngay lập tức</p>

                    <form method="post" id="contact-form" action="{{route('frontend.contact.store')}}">
                        <input name="type" value="smart" type="hidden"/>

                        <input class="form-control mb-3" placeholder="Tên của bạn *" name="name" type="text"/>

                        <input class="form-control mb-3" placeholder="Email *" name="email" type="email"/>

                        <input class="form-control mb-3" placeholder="Số điện thoại *" type="text" name="phone"/>

                        <textarea class="form-control mb-3" name="content" placeholder="Lời nhắn *"></textarea>

                        <button class="btn btn-primary m-auto d-block px-4 text-uppercase">Gửi đi</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
