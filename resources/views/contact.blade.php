@extends('layouts.app')
@section('content')
    <section class="banner-page d-flex align-items-center text-center">
        <div class="container">
            <h1>Liên hệ</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb justify-content-center align-items-center">
                    <li class="breadcrumb-item"><a href="{{config('app.url')}}">Trang chủ</a><ion-icon name="chevron-forward-outline"></ion-icon></li>
                    <li class="breadcrumb-item active" aria-current="page">Liên hệ</li>
                </ol>
            </nav>
        </div>
    </section>

    <section class="bg-secondary py-5 contact-page">
        <div class="container">
            <h6 class="text-center font-weight-light mb-3">Dự án của bạn là gì? Hãy gửi cho chúng tôi yêu cầu của bạn, nhân viên tư vấn của chúng tôi sẽ liên hệ với bạn ngay lập tức.</h6>
            <h5>Upload file yêu cầu dự án</h5>
            <div id="contactDropzone" class="custom-dropzone mb-4" action="{{route('frontend.attachment.upload')}}">
                <div class="dz-message" data-dz-message>
                    <p class="m-0"><ion-icon name="cloud-upload-outline" class="note"></ion-icon></p>
                    <p class="m-0 note">Kéo file vào đây để tải lên.</p>
                </div>
            </div>

            <form method="post" id="contact-form" action="{{route('frontend.contact.store')}}">
                <h5>Thông tin của bạn</h5>
                <div class="row">
                    <div class="col-lg-6 mb-3">
                        <input class="form-control" placeholder="Tên của bạn" name="name" type="text"/>
                    </div>
                    <div class="col-lg-6 mb-3">
                        <input class="form-control" placeholder="Email" name="email" type="email"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 mb-3">
                        <input class="form-control" placeholder="Số điện thoại" type="text" name="phone"/>
                    </div>
                    <div class="col-lg-6 mb-3">
                        <input class="form-control" placeholder="Công ty" type="text" name="company">
                    </div>
                </div>
                <h5>Mô tả dự án</h5>
                <div class="form-group">
                    <textarea class="form-control" name="content" placeholder="Chi tiết dự án"></textarea>
                </div>
                <br>
                <button class="btn btn-primary m-auto d-block px-4 text-uppercase">Gửi đi</button>
            </form>
        </div>

    </section>
@endsection
@section('script')
<script src="{{asset('frontend/js/dropzone.min.js')}}"></script>
<script src="{{asset('frontend/js/contact.js')}}"></script>
@endsection
