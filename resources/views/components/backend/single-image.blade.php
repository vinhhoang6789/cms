<div class="lfm-selector">
    <div class="img-container">
        <i id="{{$removeEl}}" class="fa fa-remove lfm-remove" {{$currentImage ? 'style=display:block' : ''}}></i>
        <img id="{{$imageEl}}" {{$currentImage ? 'src='.$currentImage : ''}}>
    </div>

    <button type="button" data-remove="{{$removeEl}}" data-input="{{$fileEl}}" data-preview="{{$imageEl}}" class="btn btn-info lfm-image">
        Chọn hình ảnh
    </button>

    <input id="{{$fileEl}}" class="d-none form-control" type="text" name="{{$name}}" value="{{$currentImage}}">
</div>
