<div class="widget-box " >
    <div class="widget-header">
        <h4 class="widget-title">Chọn nhiều hình ảnh</h4>

        <div class="widget-toolbar">
            <a href="javascript:void(0)" class="green" id="add-image" data-name="{{$name}}">
                <i class="ace-icon fa fa-plus-circle"></i> Thêm ảnh
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div class="col-md-12">
            <small>Kéo thả để sắp xếp lại vị trí hình ảnh.</small>
        </div>
        <br>

        <div class="widget-main row">
            <ul id="image-container">
                @if($images)
                    @foreach($images as $image)
                        <li class="image-item col-md-2">
                            <div class="image-action">
                                <input type="hidden" name="{{$name}}[]" value="{{$image->url}}"/>
                                <img src="{{$image->url}}"/>
                                <i class="remove fa fa-remove"></i>
                            </div>
                        </li>
                    @endforeach
                @else
                    <li class="image-item col-md-2">
                        <button type="button" class="btn btn-info choose-image"><i class="fa fa-image"></i> Chọn ảnh</button>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</div>
