<div class="widget-box">
    <div class="widget-header">
        <h4 class="card-title">Cấu hình SEO</h4>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <div class="form-group">
                <label>Tiêu đề</label>
                <input value="{{$formData->title}}" type="text" class="form-control" name="seo[title]">
            </div>
            <div class="form-group">
                <label>Canonical</label>
                <input value="{{$formData->canonical}}" type="text" name="seo[canonical]" class="form-control">
            </div>
            <div class="form-group">
                <label>Description</label>
                <textarea name="seo[description]" class="form-control">{{$formData->description}}</textarea>
            </div>
            <div class="form-group">
                <label>Scheme</label>
                <textarea name="seo[scheme]" class="form-control">{{$formData->scheme}}</textarea>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Index</label>
                        <div class="form-check">
                            <label>
                                @if(old('title') || $isEdit)
                                    <input value="1" {{$formData->index ? 'checked' : ''}} name="seo[index]" class="ace ace-switch" type="checkbox">
                                @else
                                    <input checked name="seo[index]" value="1" class="ace ace-switch" type="checkbox">
                                @endif
                                <span class="lbl"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Follow</label>
                        <div class="form-check">
                            <label>
                                @if(old('title') || $isEdit)
                                    <input value="1" {{$formData->follow ? 'checked' : ''}} name="seo[follow]" class="ace ace-switch" type="checkbox">
                                @else
                                    <input value="1" checked {{$formData->follow == 1 ? 'checked' : ''}} name="seo[follow]" class="ace ace-switch" type="checkbox">
                                @endif
                                <span class="lbl"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
