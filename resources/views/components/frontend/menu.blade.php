<div class="collapse navbar-collapse justify-content-end" id="primary-menu">
    <ul class="navbar-nav">
        {!! $menu !!}
    </ul>
</div>
