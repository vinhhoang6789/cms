@extends('errors::minimal')

@section('title', __('Forbidden'))
@section('code', '403')

@if($exception instanceof \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException)
    @section('message', __("Bạn không có quyền thực hiện tác vụ này." ?: 'Forbidden'))
@else
    @section('message', __($exception->getMessage() ?: 'Forbidden'))
@endif

