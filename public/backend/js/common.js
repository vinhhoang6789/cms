function errorToHtml(response)
{
    let html = '';

    if(response.errors){
        _.forEach(response.errors, function(value){
            value.forEach(function(error){
                html += '<li>'+error+'</li>'
            })
        })
    }else{
        html += '<li>'+response.message+'</li>'
    }

    return html

}

function notify(icon, msg, type)
{
    $.notify({
        message: msg
    }, {
        type: type,
        timer: 3000,
        placement: {
            from: 'top',
            align: 'left'
        }
    });
}

function goToPage(url){
    window.location.assign(url)
}
