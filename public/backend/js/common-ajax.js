$(document).ready(function(){
    let body = $('body')

    body.delegate('a.delete-post', 'click', function(e){
        e.preventDefault();
        let url = $(this).attr('href')

        Swal.fire({
            title: 'Bạn có chắc chắn muốn xóa bản ghi này không?',
            confirmButtonText: 'Đồng ý, xóa luôn!',
            cancelButtonText: 'Không, dữ lại',
            icon: 'question',
            showCancelButton: true,
            buttonsStyling: false,
            confirmButtonClass: "btn btn-success",
            cancelButtonClass: "btn btn-danger"

        }).then(function(result){
            if(result.value){
                loader.show()

                $.ajax({
                    url: url,
                    method: 'DELETE',

                    success: function(){
                        Swal.fire(
                            'Đã xóa!',
                            '',
                            'success',
                        ).then(function(){
                            window.location.reload()
                        })
                    },
                    error: function(res){
                        notify('notifications', res.responseJSON.message, 'danger')
                    },
                    complete: function(){
                        loader.hide()
                    }
                })
            }
        })
    })

    body.delegate('a.delete-permanently-post', 'click', function(e){
        e.preventDefault();
        let url = $(this).attr('href')

        Swal.fire({
            title: 'Bạn có chắc chắn muốn xóa bản ghi này không?',
            text: 'Bản ghi sẽ xóa khỏi dữ liệu vĩnh viễn không thể phục hồi.',
            confirmButtonText: 'Đồng ý, xóa luôn!',
            cancelButtonText: 'Không, dữ lại',
            icon: 'question',
            showCancelButton: true,
            buttonsStyling: false,
            confirmButtonClass: "btn btn-success",
            cancelButtonClass: "btn btn-danger"

        }).then(function(result){
            if(result.value){
                loader.show()

                $.ajax({
                    url: url,
                    method: 'POST',

                    success: function(){
                        Swal.fire(
                            'Đã xóa!',
                            '',
                            'success',
                        ).then(function(){
                            window.location.reload()
                        })
                    },
                    error: function(res){
                        notify('notifications', res.responseJSON.message, 'danger')
                    },
                    complete: function(){
                        loader.hide()
                    }
                })
            }
        })
    })

    body.delegate('a.duplicate-post', 'click', function(e){
        e.preventDefault();
        let url = $(this).attr('href')

        loader.show()

        $.ajax({
            url: url,
            method: 'POST',
            success: function(){
                Swal.fire(
                    'Đã nhân bản!',
                    'Bản ghi đã được nhân bản và lưu với trạng thái là một bản nháp.',
                    'success',
                ).then(function(){
                    window.location.reload()
                })
            },
            error: function(res){
                notify('notifications', res.responseJSON.message, 'danger')
            },
            complete: function(){
                loader.hide()
            }
        })
    })

    body.delegate('a.restore-post', 'click', function(e){
        e.preventDefault();
        let url = $(this).attr('href')

        loader.show()

        $.ajax({
            url: url,
            method: 'POST',
            success: function(){
                Swal.fire(
                    'Khôi phục thành công!',
                    '',
                    'success',
                ).then(function(){
                    window.location.reload()
                })
            },
            error: function(res){
                notify('notifications', res.responseJSON.message, 'danger')
            },
            complete: function(){
                loader.hide()
            }
        })
    })

    body.delegate('input.check-all','change', function(){
        let checkItem = $('input.check-item')
        let checkAll = $(this)

        if(checkAll.is(':checked')){
            checkItem.prop('checked', true)
        }else{
            checkItem.prop('checked', false)
        }
    })

    body.delegate('.multiple-delete', 'click', function(){
        let ids = []
        let url = $(this).find('i').data('url')

        $('input.check-item:checked').each(function(i, e){
            ids.push(e.value)
        })

        if(ids.length <= 0){
            Swal.fire('Chọn những bản ghi mà bạn muốn xóa','','warning')

        }else{
            Swal.fire({
                title: 'Bạn có chắc chắn muốn xóa những bản ghi này không?',
                confirmButtonText: 'Đồng ý, xóa luôn!',
                cancelButtonText: 'Không, dữ lại',
                icon: 'question',
                showCancelButton: true,
                buttonsStyling: false,
                confirmButtonClass: "btn btn-success",
                cancelButtonClass: "btn btn-danger"

            }).then(function(result){

                if(result.value){
                    loader.show()

                    $.ajax({
                        url: url,
                        method: 'POST',
                        data: {ids: ids},

                        success: function(){
                            Swal.fire(
                                'Đã xóa!',
                                '',
                                'success',
                            ).then(function(){
                                window.location.reload()
                            })
                        },
                        error: function(res){
                            notify('notifications', res.responseJSON.message, 'danger')
                        },
                        complete: function(){
                            loader.hide()
                        }
                    })
                }
            })
        }
    })

    body.delegate('.multiple-delete-trash', 'click', function(){
        let ids = []
        let url = $(this).find('i').data('url')

        $('input.check-item:checked').each(function(i, e){
            ids.push(e.value)
        })

        if(ids.length <= 0){
            Swal.fire('Chọn những bản ghi mà bạn muốn xóa','','warning')

        }else{
            Swal.fire({
                title: 'Bạn có chắc chắn muốn xóa những bản ghi này không?',
                confirmButtonText: 'Đồng ý, xóa luôn!',
                cancelButtonText: 'Không, dữ lại',
                text: 'Bản ghi sẽ xóa khỏi dữ liệu vĩnh viễn không thể phục hồi.',
                icon: 'question',
                showCancelButton: true,
                buttonsStyling: false,
                confirmButtonClass: "btn btn-success",
                cancelButtonClass: "btn btn-danger"

            }).then(function(result){

                if(result.value){
                    loader.show()

                    $.ajax({
                        url: url,
                        method: 'POST',
                        data: {ids: ids},

                        success: function(){
                            Swal.fire(
                                'Đã xóa!',
                                '',
                                'success',
                            ).then(function(){
                                window.location.reload()
                            })
                        },
                        error: function(res){
                            notify('notifications', res.responseJSON.message, 'danger')
                        },
                        complete: function(){
                            loader.hide()
                        }
                    })
                }
            })
        }
    })

    body.delegate('.multiple-restore', 'click', function(){
        let ids = []
        let url = $(this).find('i').data('url')

        $('input.check-item:checked').each(function(i, e){
            ids.push(e.value)
        })

        if(ids.length <= 0){
            Swal.fire('Chọn những bản ghi mà bạn muốn khôi phục','','warning')

        }else{
            loader.show()

            $.ajax({
                url: url,
                method: 'POST',
                data:{ids: ids},
                success: function(){
                    Swal.fire(
                        'Khôi phục thành công!',
                        '',
                        'success',
                    ).then(function(){
                        window.location.reload()
                    })
                },
                error: function(res){
                    notify('notifications', res.responseJSON.message, 'danger')
                },
                complete: function(){
                    loader.hide()
                }
            })
        }
    })

    body.delegate('input.update-field','change', function(){
        let url = $(this).data('url')

        let data = {}
        data[$(this).attr('name')] = $(this).val()

        $.ajax({
            url: url,
            method: 'POST',
            data: data,
            error: function(res){
                notify('notifications', res.responseJSON.message, 'danger')
            },
        })
    })

})
