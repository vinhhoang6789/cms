$(document).ready(function(){
    let add = $('#add-image')
    let imagesContainer = $('#image-container')
    let body = $('body')
    let name = add.data('name') ? add.data('name') : 'images'

    imagesContainer.sortable();

    add.click(function(){
        imagesContainer.append(
        ` <li class="image-item col-md-2">
            <button type="button" class="btn btn-info choose-image"><i class="fa fa-image"></i> Chọn ảnh</button>
         </li>`
        )
        imagesContainer.sortable();
    })

    body.delegate('#image-container .choose-image','click', function(){
        let route_prefix = '/admin/filemanager';
        window.open(route_prefix + '?type=image', 'FileManager', 'width=900,height=600');

        let _this = $(this)

        window.SetUrl = function (url, file_path) {
            _this.parent().append(`<input type="hidden" name="${name}[]" value="${file_path}"/>`)

            _this.parent().append(`
                <div class="image-action">
                    <img src="${url}"/>
                    <i class="remove fa fa-remove"></i>
                </div>
            `)

            _this.hide();
        }

        return false
    })

    body.delegate('#image-container .image-item .remove', 'click', function(){
        $(this).parent().parent().remove()
    })

})
