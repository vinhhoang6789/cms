
$.extend( true, $.fn.dataTable.defaults, {
    responsive: true,
    language: {
        url: siteUrl + '/backend/datatablelang.json'
    }
} );

$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr("content")
        }
    })


    var options = {
        filebrowserImageBrowseUrl: '/admin/filemanager?type=Images',
        filebrowserImageUploadUrl: '/admin/filemanager/upload?type=Images&_token=',
        filebrowserBrowseUrl: '/admin/filemanager?type=Files',
        filebrowserUploadUrl: '/admin/filemanager/upload?type=Files&_token='
    };

    CKEDITOR.replace('editor', options);

    $('.lfm-image').filemanager('image');

    $('.lfm-remove').click(function(){
        $(this).hide()
        $(this).next().hide()
        $(this).parent().siblings('input').val('')
    })

    $().ready(function () {
        $('body').tooltip({selector: '[data-toggle="tooltip"]'})
        $('.chosen-select').chosen({search_contains:true, no_results_text: "Không tìm thấy bất kỳ kết quả nào"});
    });

    $('.scrollable').each(function () {
        var $this = $(this);
        $(this).ace_scroll({
            size: $this.attr('data-size') || 100,
            //styleClass: 'scroll-left scroll-margin scroll-thin scroll-dark scroll-light no-track scroll-visible'
        });
    });
});
