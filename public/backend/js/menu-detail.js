$(document).ready(function(){
    let form = $('#add-menu-item')
    let errorBox = form.find('.alert-danger')
    let errorMessage = form.find('.error-message')
    let url = form.attr('action')
    let draggableMenu = $('.draggable-menu')

    $('body').delegate('.delete-menu', 'click', function(){
        let id = $(this).data('id')

        $.ajax({
            url: siteUrl + '/admin/menu-item/delete/' + id,
            method: 'POST',
            success: function(){
                notify('done', "Xóa menu thành công.", 'success')
                $('li.draggable-menu-item[data-id="'+id+'"]').remove()
            },
        })
    })

    draggableMenu.sortable({
        group: 'nested',
        delay: 200,
        onDrop: function($item, container, _super, event){
            let data = draggableMenu.sortable("serialize").get();

            $.ajax({
                url: siteUrl + '/admin/menu-item/update-position',
                method: 'POST',
                data: {menu_data: data},
                success: function(){
                    notify('done', "Vị trí của menu đã được lưu lại.", 'success')
                }
            })
            _super($item, container);
        }
    })

    form.submit(function(e){
        e.preventDefault();
        loader.show();

        $.ajax({
            url: url,
            data: form.serialize(),
            method: 'POST',
            complete: function(){
                loader.hide()
            },
            success: function(res){
                let html = `<li data-id="${res.id}" class="draggable-menu-item">
                        <div class="title-menu">
                            ${res.title}
                            <span class="action pull-right">
                                <i class="fa fa-pencil green"></i>
                                <i class="fa fa-remove delete-menu red" data-id="${res.id}"></i>
                            </span>
                        </div>
                    <ol></ol>
                </li>`
                draggableMenu.append(html)
                errorBox.addClass('d-none').removeClass('d-block')
                form.trigger('reset')
            },
            error: function(res){
                let errors = errorToHtml(res.responseJSON)

                errorMessage.empty()
                errorBox.addClass('d-block')
                errorMessage.append(errors)
            }
        })
    })
})
