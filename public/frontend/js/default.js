function handleFormError(response, formId, showMsg = true) {
    if (response.errors) {
        $.each(response.errors, function (index, values) {
            $.each(values, function (i, value) {
                let input = $('#' + formId).find('.form-control[name="' + index + '"]')

                input.addClass('is-invalid')

                if (showMsg) {
                    input.parent().append('<span class="invalid-feedback">' + value + '</span>')
                }

            })
        })
    }
}

$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr("content")
        }
    })

    $('[data-toggle="tooltip"]').tooltip()

    $('#subscribe-form').submit(function (e) {
        e.preventDefault()
        $(this).find('.form-control')
            .removeClass('is-invalid')
            .parent()
            .find('.invalid-feedback').remove()

        $.ajax({
            method: 'POST',
            url: $(this).attr('action'),
            data: $(this).serialize(),
            success: function () {
                Swal.fire(
                    'Đăng ký nhận bản tin thành công!',
                    '',
                    'success',
                )
                $('#subscribe-form').trigger('reset')
            },
            error: function (res) {
                handleFormError(res.responseJSON, "subscribe-form", false)
            },
        })
    })

    if ($(window).width() >= 992) {
        $('.header-menu-box').on('sticky-end', function () {
            $('.primary-sticky').css('height', '81px')
        })
    }

    $('.header-menu-box').sticky({wrapperClassName: "primary-sticky", zIndex: 9})

    $('.free-advise').sticky({zIndex: 8, topSpacing: 60})

    $(window).on('scroll', function () {
        if ($(window).scrollTop() >= $('footer').offset().top - 600) {
            $('.free-advise').unstick()

        } else {
            $('.free-advise').sticky({zIndex: 8, topSpacing: 60})
        }
    })

    $('#contact-form').submit(function(e){
        e.preventDefault()

        $(this).find('.form-control')
            .removeClass('is-invalid')
            .parent()
            .find('.invalid-feedback').remove()

        loader.show()

        $.ajax({
            method: 'POST',
            url: $(this).attr('action'),
            data: $(this).serialize(),
            success: function(){
                Swal.fire(
                    'Cảm ơn bạn đã gửi yêu cầu cho chúng tôi!',
                    'Hadava sẽ liên lạc với bạn ngay khi nhận được thông tin này.',
                    'success',
                )
                $('#contact-form').trigger('reset')
            },
            error: function(res){
                handleFormError(res.responseJSON, "contact-form", false)
            },
            complete: function(){
                loader.hide()
            }
        })
    })


    $(document).on('click', '.social-button', function (e) {
        let popupSize = {
            width: 780,
            height: 550
        };

        let verticalPos = Math.floor(($(window).width() - popupSize.width) / 2),
            horisontalPos = Math.floor(($(window).height() - popupSize.height) / 2);

        let popup = window.open($(this).prop('href'), 'social',
            'width=' + popupSize.width + ',height=' + popupSize.height +
            ',left=' + verticalPos + ',top=' + horisontalPos +
            ',location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1');

        if (popup) {
            popup.focus();
            e.preventDefault();
        }

    })

})


