$(document).ready(function(){
    $(document).on('scroll', function () {
        if ($(this).scrollTop() >= ($('.workflow-section').offset().top - 500)) {
            $('.workflow-section .item').addClass('zoomIn d-flex')
        }
    })

    let windowW = $(window).width()
    if (windowW >= 992) {
        $('.full-with-slider').on('init', function (slick, currentSlide) {
            $(currentSlide.$slides.get(0)).find('div.animated')
                .addClass('d-lg-block slideInLeft')
                .removeClass('slideOutRight')

            $(currentSlide.$slides.get(0)).find('a.animated').removeClass('bounceOut')

        }).on("afterChange", function (slick, currentSlide) {
            let currentSlick = $(currentSlide.$slides.get(currentSlide.slickCurrentSlide()))
            let prvSlick = $(currentSlide.$slides.get(currentSlide.slickCurrentSlide() - 1))

            currentSlick.find('div.animated')
                .addClass('d-lg-block slideInLeft')
                .removeClass('slideOutRight')

            currentSlick.find('a.animated').removeClass('bounceOut')

            prvSlick.find('div.animated').addClass('slideOutRight')
            prvSlick.find('a.animated').addClass('bounceOut')

        })
    }

    $('.full-with-slider').slick({
        arrows: false,
        dots: true,
        autoplay: true,
        autoplaySpeed: 5000,
    })

    $('.testimonial-slider').slick({
        slidesToShow: 2,
        autoplay: true,
        autoplaySpeed: 4000,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    })

    $('.partner-slider').slick({
        slidesToShow: 5,
        autoplay: true,
        autoplaySpeed: 1000,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    })
})
