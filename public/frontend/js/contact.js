$(document).ready(function(){
    $('#contact-form').submit(function(e){
        e.preventDefault()

        $(this).find('.form-control')
            .removeClass('is-invalid')
            .parent()
            .find('.invalid-feedback').remove()

        loader.show()

        $.ajax({
            method: 'POST',
            url: $(this).attr('action'),
            data: $(this).serialize(),
            success: function(){
                Swal.fire(
                    'Cảm ơn bạn đã gửi yêu cầu cho chúng tôi!',
                    'Hadava sẽ liên lạc với bạn ngay khi nhận được thông tin này.',
                    'success',
                )
                $('#contact-form').trigger('reset')
            },
            error: function(res){
                handleFormError(res.responseJSON, "contact-form")
            },
            complete: function(){
                loader.hide()
            }
        })
    })

    $('#contactDropzone').dropzone({
        url:  $(this).attr('action'),
        params: {_token: $('meta[name="csrf-token"]').attr("content")},
        acceptedFiles: "image/*, .pdf, .docx",
        maxFilesize: 3,
        maxFiles: 4,
        dictFallbackMessage: "Trình duyệt của bạn không hỗ trợ tính năng này.",
        dictInvalidFileType: "File không đúng định dạng cho phép.",
        dictMaxFilesExceeded: "Bạn đã vượt quá số lượng file cho phép upload.",
        success: function(file){
            let res = $.parseJSON(file.xhr.responseText)
            $('#contact-form').append(`<input type="hidden" name="file[]" value="${res.path}"/>`)
        }
    })
})
