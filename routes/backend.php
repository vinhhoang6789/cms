<?php
use Illuminate\Support\Facades\Route;

Route::namespace('Backend')
    ->prefix('admin')->group( function(){
    Route::middleware('administrator.check')
        ->get('/', 'AuthenticationController@showLoginForm')
        ->name('backend.login');

    Route::post('/login', 'AuthenticationController@login')->name('backend.post.login');
    Route::get('/logout', 'AuthenticationController@loggedOut')->name('backend.logout');

    Route::group(['guard' => 'administrator', 'middleware' => 'administrator'],function () {
        Route::group(['prefix' => 'filemanager'], function () {
            Route::get('/', '\UniSharp\LaravelFilemanager\Controllers\LfmController@show')
                ->middleware('can:visible image');
            Route::post('/upload', '\UniSharp\LaravelFilemanager\Controllers\UploadController@upload')
                ->middleware('can:create image');;
        });

        Route::get('/dashboard', 'DashboardController@index')->name('backend.dashboard');
        Route::get('/cache/clear', 'SettingController@clearCache')->name('backend.cache.clear');

        Route::get('/profile', 'ProfileController@index')->name('backend.profile');
        Route::post('/profile/edit', 'ProfileController@edit')->name('backend.profile.edit');

        Route::get('/menu', 'MenuController@index')->name('backend.menu');
        Route::get('/menu/{id}', 'MenuController@detail')->name('backend.menu.detail');
        Route::get('/menu-item/{id}', 'MenuController@detailItem')->name('backend.menuItem.detail');
        Route::post('menu-item/store', 'MenuController@addMenuItem')->name('backend.menuItem.store');
        Route::post('menu-item/update', 'MenuController@updateMenuItem')->name('backend.menuItem.update');
        Route::post('menu-item/update-position', 'MenuController@updateMenuItemPosition')->name('backend.menuItem.updatePosition');
        Route::post('menu-item/delete/{id}', 'MenuController@deleteMenuItem')->name('backend.menuItem.delete');

        Route::get('/setting/general', 'SettingController@index')->name('backend.setting.general');
        Route::get('/setting/cache', 'SettingController@cache')->name('backend.setting.cache');
        Route::post('/setting', 'SettingController@save')->name('backend.setting.save');

        Route::get('media', 'MediaController@index')->name('backend.media');

        Route::resource('comments', 'CommentController');
        Route::post('comments/multiple-destroy', 'CommentController@multipleDestroy')->name('comments.multipleDestroy');

        Route::get('contacts', 'ContactController@index')->name('contacts.index');
        Route::get('contacts/{id}', 'ContactController@show')->name('contacts.show');

        Route::resource('blocks', 'BlockController');
        Route::post('blocks/{id}/duplicate', 'BlockController@duplicate')->name('blocks.duplicate');
        Route::post('blocks/multiple-destroy', 'BlockController@multipleDestroy')->name('blocks.multipleDestroy');

        Route::resource('articles', 'ArticleController');
        Route::post('articles/{id}/duplicate', 'ArticleController@duplicate')->name('articles.duplicate');
        Route::post('articles/multiple-destroy', 'ArticleController@multipleDestroy')->name('articles.multipleDestroy');
        Route::post('articles/update-field/{id}', 'ArticleController@updateField')->name('articles.updateField');

        Route::get('trashed-articles', 'ArticleController@trash')->name('articles.trash');
        Route::post('articles/{id}/restore', 'ArticleController@restore')->name('articles.restore');
        Route::post('articles/multiple-restore-trash', 'ArticleController@multipleRestoreTrash')->name('articles.multipleRestore');
        Route::post('articles/multiple-destroy-trash', 'ArticleController@multipleDestroyTrash')->name('articles.multipleDestroyTrash');
        Route::post('articles/delete-permanently/{id}', 'ArticleController@destroyPermanently')->name('articles.destroyPermanently');

        Route::resource('article-categories', 'ArticleCategoryController');
        Route::post('article-categories/multiple-destroy', 'ArticleCategoryController@multipleDestroy')
            ->name('article-categories.multipleDestroy');

        Route::resource('article-tags', 'ArticleTagController');
        Route::post('article-tags/multiple-destroy', 'ArticleTagController@multipleDestroy')
            ->name('article-tags.multipleDestroy');

        Route::resource('products', 'ProductController');
        Route::post('products/{id}/duplicate', 'ProductController@duplicate')->name('products.duplicate');
        Route::post('products/multiple-destroy', 'ProductController@multipleDestroy')->name('products.multipleDestroy');
        Route::post('products/update-field/{id}', 'ProductController@updateField')->name('products.updateField');

        Route::get('trashed-products', 'ProductController@trash')->name('products.trash');
        Route::post('products/{id}/restore', 'ProductController@restore')->name('products.restore');
        Route::post('products/multiple-restore-trash', 'ProductController@multipleRestoreTrash')->name('products.multipleRestore');
        Route::post('products/multiple-destroy-trash', 'ProductController@multipleDestroyTrash')->name('products.multipleDestroyTrash');
        Route::post('products/delete-permanently/{id}', 'ProductController@destroyPermanently')->name('products.destroyPermanently');

        Route::resource('product-categories', 'ProductCategoryController');
        Route::post('product-categories/multiple-destroy', 'ProductCategoryController@multipleDestroy')
            ->name('product-categories.multipleDestroy');

        Route::resource('pages', 'PageController');
        Route::post('pages/{id}/duplicate', 'PageController@duplicate')->name('pages.duplicate');
        Route::post('pages/multiple-destroy', 'PageController@multipleDestroy')->name('pages.multipleDestroy');

        Route::get('trashed-pages', 'PageController@trash')->name('pages.trash');
        Route::post('pages/{id}/restore', 'PageController@restore')->name('pages.restore');
        Route::post('pages/multiple-restore-trash', 'PageController@multipleRestoreTrash')->name('pages.multipleRestore');
        Route::post('pages/multiple-destroy-trash', 'PageController@multipleDestroyTrash')->name('pages.multipleDestroyTrash');
        Route::post('pages/delete-permanently/{id}', 'PageController@destroyPermanently')->name('pages.destroyPermanently');

        Route::resource('orders', 'OrderController');
        Route::resource('sliders', 'SliderController');
        Route::post('sliders/update-field/{id}', 'SliderController@updateField')->name('sliders.updateField');
        Route::resource('administrators', 'AdministratorController');
        Route::resource('roles', 'RoleController');
    });

});
