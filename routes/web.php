<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::name('frontend.')->group(function(){
    Route::middleware('cacheResponse')->group(function() {
        Route::get('/', 'HomeController@index')->name('home');
        Route::get('tag/{slug}', 'TagController@detail')->name('tag.detail');
        Route::get('lien-he', 'ContactController@index')->name('contact');
        Route::get('tin-tuc', 'ArticleController@index')->name('news');

    });

    Route::post('subscribe', 'HomeController@subscribe')->name('subscribe');
    Route::post('lien-he/store', 'ContactController@store')->name('contact.store');
    Route::post('attachment/upload', 'AttachmentController@upload')->name('attachment.upload');
});

require_once 'backend.php';

