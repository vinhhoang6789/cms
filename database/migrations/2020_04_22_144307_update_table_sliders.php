<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableSliders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sliders', function (Blueprint $table) {
            $table->string('group')->index()->nullable()->after('id');
            $table->string('image')->after('id');
            $table->string('url')->nullable()->after('id');
            $table->string('name')->nullable()->change();
            $table->string('description')->nullable()->after('id');
            $table->integer('position')->default(0)->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sliders', function (Blueprint $table) {
            $table->dropColumn('group');
            $table->dropColumn('image');
            $table->dropColumn('url');
            $table->dropColumn('name');
            $table->dropColumn('description');
            $table->dropColumn('position');
        });
    }
}
