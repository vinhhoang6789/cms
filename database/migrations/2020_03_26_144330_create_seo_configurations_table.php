<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeoConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seo_configurations', function (Blueprint $table) {
            $table->id();
            $table->integer('seo_configurable_id');
            $table->string('seo_configurable_type');
            $table->string('title')->nullable();
            $table->double('index')->default(1);
            $table->double('follow')->default(1);
            $table->string('canonical')->nullable();
            $table->string('description')->nullable();
            $table->string('scheme')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seo_configurations');
    }
}
