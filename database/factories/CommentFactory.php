<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Comment;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    $type = [
        0 => 'App\Models\Product',
        1 => 'App\Models\Article',
    ];

    return [
        'name' => $faker->name(),
        'email' => $faker->email(),
        'content' => $faker->text(200),
        'commentable_id' => rand(1, 1000),
        'commentable_type' => $type[rand(0,1)],
    ];
});
