<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ArticleCategory;
use Faker\Generator as Faker;

$factory->define(ArticleCategory::class, function (Faker $faker) {
    $title = $faker->realText(20);
    return [
        'title' => $title,
    ];
});





