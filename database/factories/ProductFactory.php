<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    $title = $faker->realText(100);
    return [
        'title' => $title,
        'price' => rand(100000, 1000000000),
        'status' => 'publish',
        'product_category_id' => rand(1,100),
    ];
});
