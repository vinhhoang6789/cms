<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Article;
use Faker\Generator as Faker;

$factory->define(Article::class, function (Faker $faker) {
    $title = $faker->realText(150);

    return [
        'title' => $title,
        'excerpt' => $faker->text(100),
        'content' => $faker->text(5000),
        'article_category_id' => rand(1,100),
        'administrator_id' => 1,
        'featured_image' => $faker->imageUrl(),
        'status' => 'publish',
    ];
});
