<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Administrator;
use Faker\Generator as Faker;

$factory->define(Administrator::class, function (Faker $faker) {
    return [
        'username' => 'admin'.$faker->randomNumber(5, true),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
    ];
});
