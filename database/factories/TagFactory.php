<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Tag;
use Faker\Generator as Faker;

$factory->define(Tag::class, function (Faker $faker) {
    $title = $faker->realText(10);
    return [
        'title' => $title,
        'slug' => str_slug($title),
        'type' => 'article',
    ];
});
