<?php

use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $time = date('Y-m-d H:i:s');
        \DB::table('settings')->insert([
            [
                'group' => 'general',
                'title' => 'Site Name',
                'type' => 'text',
                'created_at' => $time,
                'updated_at' => $time,
            ],
            [
                'group' => 'general',
                'title' => 'Tiêu đề',
                'type' => 'text',
                'created_at' => $time,
                'updated_at' => $time,
            ],
            [
                'group' => 'general',
                'title' => 'Meta Description',
                'type' => 'text',
                'created_at' => $time,
                'updated_at' => $time,
            ],
            [
                'group' => 'general',
                'title' => 'Logo',
                'type' => 'file',
                'created_at' => $time,
                'updated_at' => $time,
            ],
            [
                'group' => 'general',
                'title' => 'Facebook',
                'type' => 'text',
                'created_at' => $time,
                'updated_at' => $time,
            ],
            [
                'group' => 'general',
                'title' => 'Google',
                'type' => 'text',
                'created_at' => $time,
                'updated_at' => $time,
            ],
            [
                'group' => 'general',
                'title' => 'Youtube',
                'type' => 'text',
                'created_at' => $time,
                'updated_at' => $time,
            ],
            [
                'group' => 'general',
                'title' => 'Hotline',
                'type' => 'text',
                'created_at' => $time,
                'updated_at' => $time,
            ],
            [
                'group' => 'general',
                'title' => 'Zalo',
                'type' => 'text',
                'created_at' => $time,
                'updated_at' => $time,
            ],

            [
                'group' => 'cache',
                'title' => 'Chế độ cache',
                'type' => 'option',
                'created_at' => $time,
                'updated_at' => $time,
            ],

        ]);
    }
}
