<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdministratorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $time = date('Y-m-d H:i:s');

        DB::table('administrators')->insert(
            [
                'name' => 'Admin',
                'username' => 'admin',
                'role_id' => 1,
                'email' => 'admin@domain.com',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password,
                'created_at' => $time,
                'updated_at' => $time,
            ]
        );
    }
}
