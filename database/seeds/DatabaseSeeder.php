<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionSeeder::class);
        $this->call(AdministratorSeeder::class);
        $this->call(MenuTableSeeder::class);
        $this->call(SettingTableSeeder::class);
        $this->call(PageTableSeeder::class);
        $this->call(RoleSeeder::class);
    }
}
