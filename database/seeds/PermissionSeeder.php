<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $time = date('Y-m-d H:i:s');
        $manageableList = \App\Models\Administrator::manageableList();

        $data = [];
        $roles = ['visible', 'create', 'edit', 'delete'];

        foreach ($manageableList as $item){
            $itemExploder = explode('|', $item);

            foreach($roles as $role){
                $data[] = [
                    'name' => $role.' '.$itemExploder[0],
                    'guard_name' => 'administrator',
                    'created_at' => $time,
                    'updated_at' => $time,
                ];
            }
        }

        \DB::table('permissions')->insert($data);
    }
}
