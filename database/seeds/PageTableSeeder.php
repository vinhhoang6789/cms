<?php

use Illuminate\Database\Seeder;

class PageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $page = new \App\Models\Page([
            'title' => 'Giới thiệu công ty',
            'status' => 'publish',
        ]);
        $page->save();

        $page->permalink()->create([
            'type' => \App\Models\Page::PERMALINK_TYPE,
            'action' => \App\Models\Page::PERMALINK_ACTION
        ]);

        $page->seo()->create();

        $page = new \App\Models\Page([
            'title' => 'Câu hỏi thường gặp',
            'status' => 'publish',
        ]);
        $page->save();

        $page->permalink()->create([
            'type' => \App\Models\Page::PERMALINK_TYPE,
            'action' => \App\Models\Page::PERMALINK_ACTION
        ]);

        $page->seo()->create();
    }
}
