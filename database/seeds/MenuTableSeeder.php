<?php

use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $time = date('Y-m-d H:i:s');
        \DB::table('menus')->insert([
            'name' => 'Menu chính',
            'created_at' => $time,
            'updated_at' => $time,
        ]);
    }
}
